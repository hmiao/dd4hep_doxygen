var modules =
[
    [ "dd4hep classes and objects", "de/d88/group__DD4HEP.html", "de/d88/group__DD4HEP" ],
    [ "dd4hep::XML classes and objects", "d0/d2c/group__DD4HEP__XML.html", "d0/d2c/group__DD4HEP__XML" ],
    [ "dd4hep::detail components, classes and objects", "d0/d92/group__DD4HEP__CORE.html", "d0/d92/group__DD4HEP__CORE" ],
    [ "dd4hep::sim components, classes and objects", "dc/d78/group__DD4HEP__SIMULATION.html", "dc/d78/group__DD4HEP__SIMULATION" ],
    [ "DDAlign: dd4hep Detector geometry alignment components.", "d8/d94/group__DD4HEP__ALIGN.html", "d8/d94/group__DD4HEP__ALIGN" ],
    [ "DDCond: dd4hep Detector Conditions components, classes and functions", "dd/d8d/group__DD4HEP__CONDITIONS.html", "dd/d8d/group__DD4HEP__CONDITIONS" ],
    [ "DDEve: dd4hep event display components, classes and objects", "de/df8/group__DD4HEP__EVE.html", "de/df8/group__DD4HEP__EVE" ],
    [ "LCIO components, classes and objects", "d5/d05/group__LCIO.html", "d5/d05/group__LCIO" ],
    [ "Plugins", "dd/dd3/group__Plugins.html", "dd/dd3/group__Plugins" ],
    [ "Classes and namespaces from the ROOT project. See http://root.cern.ch", "d3/d0b/group__ROOT.html", "d3/d0b/group__ROOT" ]
];
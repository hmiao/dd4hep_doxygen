var structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo =
[
    [ "segInfo", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#a1d35ac39c98928366fb9b4ccb4413b90", null ],
    [ "megaTileOffsetX", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#a2659c89260ae881707a27033f97169a3", null ],
    [ "megaTileOffsetY", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#aafc0cfdbccb88ffcaa15a51237936621", null ],
    [ "megaTileSizeX", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#a7ca45b8ee9171b15f0d69bbb2847724a", null ],
    [ "megaTileSizeY", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#a516c21dd08367939eb6f6eadd647718c", null ],
    [ "nCellsX", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#a75827544cae757a4bf43963945cb6bd9", null ],
    [ "nCellsY", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html#acdd7c00f6acee5699b020447a6bf83d6", null ]
];
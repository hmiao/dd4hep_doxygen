var classdd4hep_1_1Author =
[
    [ "Author", "d7/d6a/classdd4hep_1_1Author.html#a65b332a87aced018f14f82528caec217", null ],
    [ "Author", "d7/d6a/classdd4hep_1_1Author.html#ac2ab434bde47eef1f0ad193b2f617d5d", null ],
    [ "Author", "d7/d6a/classdd4hep_1_1Author.html#a72f7f15f57dca967cb4e61e2163a74d9", null ],
    [ "Author", "d7/d6a/classdd4hep_1_1Author.html#a6008255ed0e712b6d880d5338f106b8c", null ],
    [ "authorEmail", "d7/d6a/classdd4hep_1_1Author.html#a283b6a4d3a85d1b2d51a26ab1155c37a", null ],
    [ "authorName", "d7/d6a/classdd4hep_1_1Author.html#a3fe7a9f2c93f188dda6c53c0a753e763", null ],
    [ "operator=", "d7/d6a/classdd4hep_1_1Author.html#a4edb4659ee0fb6fb23f218207729a405", null ],
    [ "setAuthorEmail", "d7/d6a/classdd4hep_1_1Author.html#a0e51de9a1fef12422800b96164296ae4", null ],
    [ "setAuthorName", "d7/d6a/classdd4hep_1_1Author.html#aca1a9559e7b890d4b817b1a5286a034a", null ]
];
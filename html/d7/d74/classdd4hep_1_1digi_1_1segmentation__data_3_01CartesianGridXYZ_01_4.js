var classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4 =
[
    [ "segmentation_xyz", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#aeaef2e56fb5644d1d8e1b5c8ff455d49", null ],
    [ "x_f_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a648cc7902e84fad6b1ab1d4375a40439", null ],
    [ "x_grid_size", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#aa731f31c4a9b2c813141c43bb4edd3d3", null ],
    [ "x_mask", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a6c649033637cca8571ba8afd2c212673", null ],
    [ "x_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a129bace1c1b164bbd035b14dfd8fe6ed", null ],
    [ "y_f_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#aab52d816d6c9e4ad1ed3e968213a9fb1", null ],
    [ "y_grid_size", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a45564ac481e309d09a3cf764eaca2183", null ],
    [ "y_mask", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#af6abff60a08291e422d99c79495b0795", null ],
    [ "y_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#ae0e74d30709781ca1ae9b79836d99840", null ],
    [ "z_f_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a5f50978afb57cb046ee5dc39235a5c34", null ],
    [ "z_grid_size", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a00bd6fd5f671cbef5a8f4223fb1337b7", null ],
    [ "z_mask", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a005a1f8493e0cc54e362781a4925dec3", null ],
    [ "z_offset", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html#a07d572074b9fb3bb9de6b8d7a12a99f8", null ]
];
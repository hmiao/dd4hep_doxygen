var hierarchy =
[
    [ "dd4hep::detail::LCDDConverter::GeometryInfo::_checks", "d8/d8c/structdd4hep_1_1detail_1_1LCDDConverter_1_1GeometryInfo_1_1__checks.html", null ],
    [ "dd4hep::cms::ParsingContext::_debug", "d9/d5f/structdd4hep_1_1cms_1_1ParsingContext_1_1__debug.html", null ],
    [ "dd4hep_base._Levels", "d1/d39/classdd4hep__base_1_1__Levels.html", null ],
    [ "testDDPython.a_class", "dd/d9d/classtestDDPython_1_1a__class.html", null ],
    [ "dd4hep::cond::AbstractMap", "d3/d13/classdd4hep_1_1cond_1_1AbstractMap.html", null ],
    [ "dd4hep::digi::DigiAction::Actors< T >", "d5/d59/classdd4hep_1_1digi_1_1DigiAction_1_1Actors.html", null ],
    [ "dd4hep::sim::Geant4Action::Actors< T >", "d9/d6b/classdd4hep_1_1sim_1_1Geant4Action_1_1Actors.html", null ],
    [ "dd4hep::cms::AlgoArguments", "d1/dff/classdd4hep_1_1cms_1_1AlgoArguments.html", null ],
    [ "dd4hep::AlignmentExamples::AlignmentCreator", "d3/d65/classdd4hep_1_1AlignmentExamples_1_1AlignmentCreator.html", null ],
    [ "dd4hep::AlignmentData", "df/d43/classdd4hep_1_1AlignmentData.html", null ],
    [ "dd4hep::AlignmentExamples::AlignmentDataAccess", "d1/d5c/classdd4hep_1_1AlignmentExamples_1_1AlignmentDataAccess.html", null ],
    [ "dd4hep::align::AlignmentsCalculator", "df/dd1/classdd4hep_1_1align_1_1AlignmentsCalculator.html", null ],
    [ "dd4hep::align::AlignmentsCalib", "d2/de5/classdd4hep_1_1align_1_1AlignmentsCalib.html", null ],
    [ "dd4hep::align::AlignmentsCollector< T >", "d3/d7b/classdd4hep_1_1align_1_1AlignmentsCollector.html", null ],
    [ "dd4hep::align::AlignmentsPrinter", "dd/dcc/classdd4hep_1_1align_1_1AlignmentsPrinter.html", [
      [ "dd4hep::align::AlignedVolumePrinter", "d6/d09/classdd4hep_1_1align_1_1AlignedVolumePrinter.html", null ]
    ] ],
    [ "dd4hep::DDPython::AllowThreads", "d7/d27/structdd4hep_1_1DDPython_1_1AllowThreads.html", null ],
    [ "dd4hep::detail::Apply1rst< M, FCN >", "df/d84/classdd4hep_1_1detail_1_1Apply1rst.html", null ],
    [ "dd4hep::detail::Apply2nd< M, FCN >", "d5/d08/classdd4hep_1_1detail_1_1Apply2nd.html", null ],
    [ "dd4hep::detail::ApplyMemFunc< R, T >", "dd/d2a/structdd4hep_1_1detail_1_1ApplyMemFunc.html", null ],
    [ "dd4hep::detail::ApplyMemFunc1< R, T, A1 >", "d0/d50/structdd4hep_1_1detail_1_1ApplyMemFunc1.html", null ],
    [ "dd4hep::detail::ApplyMemFunc2< R, T, A1, A2 >", "db/d36/structdd4hep_1_1detail_1_1ApplyMemFunc2.html", null ],
    [ "dd4hep::detail::ApplyMemFuncConst< R, T >", "db/da9/structdd4hep_1_1detail_1_1ApplyMemFuncConst.html", null ],
    [ "dd4hep::detail::ApplyMemFuncConst1< R, T, A1 >", "d8/d8c/structdd4hep_1_1detail_1_1ApplyMemFuncConst1.html", null ],
    [ "dd4hep::detail::ApplyMemFuncConst2< R, T, A1, A2 >", "de/d30/structdd4hep_1_1detail_1_1ApplyMemFuncConst2.html", null ],
    [ "BarrelDetector", null, [
      [ "dd4hep::detail::SectorBarrelCalorimeter", "de/d25/classdd4hep_1_1detail_1_1SectorBarrelCalorimeter.html", null ]
    ] ],
    [ "BASE", null, [
      [ "dd4hep::cond::ConditionsLinearPool< MAPPING, BASE >", "d2/d51/classdd4hep_1_1cond_1_1ConditionsLinearPool.html", [
        [ "dd4hep::cond::ConditionsLinearUpdatePool< MAPPING, BASE >", "d4/d52/classdd4hep_1_1cond_1_1ConditionsLinearUpdatePool.html", null ]
      ] ],
      [ "dd4hep::cond::ConditionsMappedPool< MAPPING, BASE >", "dd/ddb/classdd4hep_1_1cond_1_1ConditionsMappedPool.html", [
        [ "dd4hep::cond::ConditionsMappedUpdatePool< MAPPING, BASE >", "d0/d15/classdd4hep_1_1cond_1_1ConditionsMappedUpdatePool.html", null ]
      ] ]
    ] ],
    [ "dd4hep::BasicGrammar", "d4/d47/classdd4hep_1_1BasicGrammar.html", [
      [ "dd4hep::Grammar< TYPE >", "d3/d84/classdd4hep_1_1Grammar.html", null ]
    ] ],
    [ "dd4hep::DDSegmentation::BitField64", "d5/dfe/classdd4hep_1_1DDSegmentation_1_1BitField64.html", null ],
    [ "dd4hep::DDSegmentation::BitFieldCoder", "d5/d10/classdd4hep_1_1DDSegmentation_1_1BitFieldCoder.html", null ],
    [ "dd4hep::DDSegmentation::BitFieldElement", "d5/d57/classdd4hep_1_1DDSegmentation_1_1BitFieldElement.html", null ],
    [ "dd4hep::DDSegmentation::BitFieldValue", "db/dd1/classdd4hep_1_1DDSegmentation_1_1BitFieldValue.html", null ],
    [ "dd4hep::sim::Geant4HitCollection::CollectionFlags::BitItems", "de/da8/structdd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags_1_1BitItems.html", null ],
    [ "dd4hep::DDPython::BlockThreads", "df/d8e/structdd4hep_1_1DDPython_1_1BlockThreads.html", null ],
    [ "dd4hep::ByName< T >", "d9/d06/classdd4hep_1_1ByName.html", null ],
    [ "dd4hep::ByNameAttr< T >", "d1/d55/classdd4hep_1_1ByNameAttr.html", null ],
    [ "gaudi::DetService::CacheEntry", "dd/dd3/classgaudi_1_1DetService_1_1CacheEntry.html", null ],
    [ "dd4hep::Callback", "db/d89/classdd4hep_1_1Callback.html", null ],
    [ "dd4hep::CallbackSequence", "df/dd0/structdd4hep_1_1CallbackSequence.html", null ],
    [ "dd4hep::Display::CalodataContext", "db/db9/structdd4hep_1_1Display_1_1CalodataContext.html", null ],
    [ "dd4hep::rec::Vector3D::Cartesian", "d7/d40/structdd4hep_1_1rec_1_1Vector3D_1_1Cartesian.html", null ],
    [ "dd4hep::Cast", "d8/d71/classdd4hep_1_1Cast.html", null ],
    [ "dd4hep::digi::cell_data< SEGMENTATION >", "d4/d94/classdd4hep_1_1digi_1_1cell__data.html", null ],
    [ "dd4hep::rec::CellIDPositionConverter", "d0/d77/classdd4hep_1_1rec_1_1CellIDPositionConverter.html", null ],
    [ "cgaudi_factory_t", "db/d10/structcgaudi__factory__t.html", null ],
    [ "cgaudi_pluginsvc_t", "d6/d63/structcgaudi__pluginsvc__t.html", null ],
    [ "cgaudi_property_t", "d5/de5/structcgaudi__property__t.html", null ],
    [ "dd4hep::detail::ClearOnReturn< C >", "da/d18/structdd4hep_1_1detail_1_1ClearOnReturn.html", null ],
    [ "CLICSid.CLICSid", "d3/ddf/classCLICSid_1_1CLICSid.html", null ],
    [ "dd4hep::cond::ClientData", "d0/d10/structdd4hep_1_1cond_1_1ClientData.html", [
      [ "dd4hep::DDDB::DDDBNamed", "d3/d9a/classdd4hep_1_1DDDB_1_1DDDBNamed.html", [
        [ "dd4hep::DDDB::DDDBAuthor", "da/d9d/classdd4hep_1_1DDDB_1_1DDDBAuthor.html", null ],
        [ "dd4hep::DDDB::DDDBCatalog", "d3/db3/classdd4hep_1_1DDDB_1_1DDDBCatalog.html", null ],
        [ "dd4hep::DDDB::DDDBDocument", "db/d35/classdd4hep_1_1DDDB_1_1DDDBDocument.html", null ],
        [ "dd4hep::DDDB::DDDBElement", "df/dec/classdd4hep_1_1DDDB_1_1DDDBElement.html", null ],
        [ "dd4hep::DDDB::DDDBIsotope", "dc/d6d/classdd4hep_1_1DDDB_1_1DDDBIsotope.html", null ],
        [ "dd4hep::DDDB::DDDBLogVol", "d7/de1/classdd4hep_1_1DDDB_1_1DDDBLogVol.html", null ],
        [ "dd4hep::DDDB::DDDBMaterial", "d3/df3/classdd4hep_1_1DDDB_1_1DDDBMaterial.html", null ],
        [ "dd4hep::DDDB::DDDBPhysVol", "d3/d0d/classdd4hep_1_1DDDB_1_1DDDBPhysVol.html", [
          [ "dd4hep::DDDB::DDDBParamPhysVol", "df/d43/classdd4hep_1_1DDDB_1_1DDDBParamPhysVol.html", [
            [ "dd4hep::DDDB::DDDBParamPhysVol2D", "de/d5c/classdd4hep_1_1DDDB_1_1DDDBParamPhysVol2D.html", [
              [ "dd4hep::DDDB::DDDBParamPhysVol3D", "d5/dc7/classdd4hep_1_1DDDB_1_1DDDBParamPhysVol3D.html", null ]
            ] ]
          ] ]
        ] ],
        [ "dd4hep::DDDB::DDDBShape", "da/de3/classdd4hep_1_1DDDB_1_1DDDBShape.html", null ],
        [ "dd4hep::DDDB::DDDBTabProperty", "d3/d23/classdd4hep_1_1DDDB_1_1DDDBTabProperty.html", null ],
        [ "dd4hep::DDDB::DDDBVersion", "df/d97/classdd4hep_1_1DDDB_1_1DDDBVersion.html", null ]
      ] ]
    ] ],
    [ "boost::iostreams::closable_tag", null, [
      [ "dd4hep::dd4hep_file< T >::category", "d5/d2d/structdd4hep_1_1dd4hep__file_1_1category.html", null ],
      [ "dd4hep::dd4hep_file_sink< T >::category", "d9/df3/structdd4hep_1_1dd4hep__file__sink_1_1category.html", null ],
      [ "dd4hep::dd4hep_file_source< T >::category", "d8/d7a/structdd4hep_1_1dd4hep__file__source_1_1category.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4HitCollection::CollectionFlags", "d8/dc6/uniondd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags.html", null ],
    [ "dd4hep::sim::Geant4HitCollection::Compare", "d5/dc6/classdd4hep_1_1sim_1_1Geant4HitCollection_1_1Compare.html", [
      [ "dd4hep::sim::CellIDCompare< TYPE >", "da/dfc/classdd4hep_1_1sim_1_1CellIDCompare.html", null ],
      [ "dd4hep::sim::PositionCompare< TYPE, POS >", "d4/d99/classdd4hep_1_1sim_1_1PositionCompare.html", null ]
    ] ],
    [ "dd4hep::ComponentCast", "da/d99/classdd4hep_1_1ComponentCast.html", null ],
    [ "dd4hepFactories.ComponentDumper", "dd/d87/classdd4hepFactories_1_1ComponentDumper.html", null ],
    [ "dd4hep::cond::Operators::Cond__Oper", "dc/da8/classdd4hep_1_1cond_1_1Operators_1_1Cond____Oper.html", [
      [ "dd4hep::cond::Operators::ActiveSelect< T >", "d0/de6/structdd4hep_1_1cond_1_1Operators_1_1ActiveSelect.html", null ],
      [ "dd4hep::cond::Operators::ConditionsOperation< OPER >", "d6/dee/classdd4hep_1_1cond_1_1Operators_1_1ConditionsOperation.html", null ],
      [ "dd4hep::cond::Operators::KeyFind", "d6/df8/classdd4hep_1_1cond_1_1Operators_1_1KeyFind.html", null ],
      [ "dd4hep::cond::Operators::KeyedSelect< collection_type >", "d2/d1e/classdd4hep_1_1cond_1_1Operators_1_1KeyedSelect.html", null ],
      [ "dd4hep::cond::Operators::MapSelect< T >", "d9/d24/structdd4hep_1_1cond_1_1Operators_1_1MapSelect.html", null ],
      [ "dd4hep::cond::Operators::OperatorWrapper< OPER >", "d6/db5/classdd4hep_1_1cond_1_1Operators_1_1OperatorWrapper.html", null ],
      [ "dd4hep::cond::Operators::PoolRemove< T >", "d1/dd6/structdd4hep_1_1cond_1_1Operators_1_1PoolRemove.html", null ],
      [ "dd4hep::cond::Operators::PoolSelect< T >", "d4/df8/structdd4hep_1_1cond_1_1Operators_1_1PoolSelect.html", null ],
      [ "dd4hep::cond::Operators::SequenceSelect< T >", "d5/d54/structdd4hep_1_1cond_1_1Operators_1_1SequenceSelect.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionDependency", "dc/d35/classdd4hep_1_1cond_1_1ConditionDependency.html", null ],
    [ "dd4hep::ConditionKey", "db/de1/classdd4hep_1_1ConditionKey.html", null ],
    [ "dd4hep::detail::ConditionObject", "d1/dac/classdd4hep_1_1detail_1_1ConditionObject.html", [
      [ "dd4hep::detail::AlignmentObject", "d7/d8b/classdd4hep_1_1detail_1_1AlignmentObject.html", null ],
      [ "gaudi::detail::DeIOVObject", "d7/d0b/classgaudi_1_1detail_1_1DeIOVObject.html", [
        [ "gaudi::detail::DeVPGenericObject", "de/d8a/classgaudi_1_1detail_1_1DeVPGenericObject.html", [
          [ "gaudi::detail::DeVPObject", "db/d17/classgaudi_1_1detail_1_1DeVPObject.html", null ]
        ] ],
        [ "gaudi::detail::DeVPSensorObject", "d9/d2e/classgaudi_1_1detail_1_1DeVPSensorObject.html", null ],
        [ "gaudi::detail::DeVeloGenericObject", "d5/da9/classgaudi_1_1detail_1_1DeVeloGenericObject.html", null ],
        [ "gaudi::detail::DeVeloObject", "d8/dd5/classgaudi_1_1detail_1_1DeVeloObject.html", null ],
        [ "gaudi::detail::DeVeloSensorObject", "d9/d9e/classgaudi_1_1detail_1_1DeVeloSensorObject.html", null ]
      ] ],
      [ "gaudi::detail::DeObject< STATIC, IOV >", "da/d82/classgaudi_1_1detail_1_1DeObject.html", null ],
      [ "gaudi::detail::DeStaticObject", "dc/d79/classgaudi_1_1detail_1_1DeStaticObject.html", [
        [ "gaudi::detail::DeVPGenericStaticObject", "df/d55/classgaudi_1_1detail_1_1DeVPGenericStaticObject.html", [
          [ "gaudi::detail::DeVPStaticObject", "da/dc3/classgaudi_1_1detail_1_1DeVPStaticObject.html", null ]
        ] ],
        [ "gaudi::detail::DeVPSensorStaticObject", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html", null ],
        [ "gaudi::detail::DeVeloGenericStaticObject", "d2/d31/classgaudi_1_1detail_1_1DeVeloGenericStaticObject.html", null ],
        [ "gaudi::detail::DeVeloSensorStaticObject", "d6/d8f/classgaudi_1_1detail_1_1DeVeloSensorStaticObject.html", null ],
        [ "gaudi::detail::DeVeloStaticObject", "dc/dde/classgaudi_1_1detail_1_1DeVeloStaticObject.html", null ]
      ] ]
    ] ],
    [ "dd4hep::cond::ConditionResolver", "d7/d79/classdd4hep_1_1cond_1_1ConditionResolver.html", [
      [ "dd4hep::cond::ConditionsDependencyHandler", "d7/d2a/classdd4hep_1_1cond_1_1ConditionsDependencyHandler.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsCleanup", "d4/d1a/classdd4hep_1_1cond_1_1ConditionsCleanup.html", [
      [ "dd4hep::cond::ConditionsFullCleanup", "dc/d0a/classdd4hep_1_1cond_1_1ConditionsFullCleanup.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsCollector< T >", "da/d3b/classdd4hep_1_1cond_1_1ConditionsCollector.html", null ],
    [ "dd4hep::cond::ConditionsContent", "dc/d58/classdd4hep_1_1cond_1_1ConditionsContent.html", null ],
    [ "dd4hep::cond::ConditionsIOVPool", "d0/d66/classdd4hep_1_1cond_1_1ConditionsIOVPool.html", null ],
    [ "dd4hep::cond::ConditionsListener", "d1/d82/classdd4hep_1_1cond_1_1ConditionsListener.html", [
      [ "dd4hep::DDDB::DDDBConditionsLoader::KeyCollector", "d9/d4b/classdd4hep_1_1DDDB_1_1DDDBConditionsLoader_1_1KeyCollector.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsLoadInfo", "d3/d44/classdd4hep_1_1cond_1_1ConditionsLoadInfo.html", [
      [ "dd4hep::cond::ConditionsContent::LoadInfo< T >", "d4/d11/classdd4hep_1_1cond_1_1ConditionsContent_1_1LoadInfo.html", null ]
    ] ],
    [ "dd4hep::ConditionsMap", "d6/dbd/classdd4hep_1_1ConditionsMap.html", [
      [ "dd4hep::AlignmentsNominalMap", "da/df2/classdd4hep_1_1AlignmentsNominalMap.html", null ],
      [ "dd4hep::ConditionsMapping< T >", "df/dfd/classdd4hep_1_1ConditionsMapping.html", null ],
      [ "dd4hep::cond::ConditionsSlice", "d4/d0e/classdd4hep_1_1cond_1_1ConditionsSlice.html", null ],
      [ "dd4hep::cond::UserPool", "de/de8/classdd4hep_1_1cond_1_1UserPool.html", [
        [ "dd4hep::cond::ConditionsMappedUserPool< MAPPING >", "df/d3f/classdd4hep_1_1cond_1_1ConditionsMappedUserPool.html", null ]
      ] ]
    ] ],
    [ "dd4hep::cond::ConditionsPrinter", "da/db2/classdd4hep_1_1cond_1_1ConditionsPrinter.html", null ],
    [ "dd4hep::cond::ConditionsRepository", "d6/d89/classdd4hep_1_1cond_1_1ConditionsRepository.html", null ],
    [ "dd4hep::ConditionsSelect", "d3/de7/classdd4hep_1_1ConditionsSelect.html", [
      [ "dd4hep::ConditionsSelectWrapper< OBJECT >", "d3/d2d/classdd4hep_1_1ConditionsSelectWrapper.html", null ],
      [ "dd4hep::cond::Operators::MapConditionsSelect< T >", "d2/d7c/structdd4hep_1_1cond_1_1Operators_1_1MapConditionsSelect.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsTag", "db/d68/classdd4hep_1_1cond_1_1ConditionsTag.html", null ],
    [ "dd4hep::cond::ConditionsTextRepository", "d4/d0f/classdd4hep_1_1cond_1_1ConditionsTextRepository.html", null ],
    [ "dd4hep::cond::ConditionsXMLRepositoryWriter", "de/d0c/classdd4hep_1_1cond_1_1ConditionsXMLRepositoryWriter.html", null ],
    [ "dd4hep::cond::ConditionUpdateCall", "dc/d3e/classdd4hep_1_1cond_1_1ConditionUpdateCall.html", [
      [ "dd4hep::ConditionExamples::ConditionNonDefaultCtorUpdate1", "de/d8c/classdd4hep_1_1ConditionExamples_1_1ConditionNonDefaultCtorUpdate1.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate1", "db/d4b/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate1.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate2", "df/d82/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate2.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate3", "d5/d36/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate3.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate4", "dc/da6/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate4.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate5", "d8/dce/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate5.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate6", "dd/d16/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate6.html", null ],
      [ "gaudi::DeAlignmentCall", "d2/dbb/classgaudi_1_1DeAlignmentCall.html", null ],
      [ "gaudi::DeVPIOVConditionCall", "da/dbc/classgaudi_1_1DeVPIOVConditionCall.html", [
        [ "gaudi::DeVPConditionCall", "db/d1e/classgaudi_1_1DeVPConditionCall.html", null ]
      ] ],
      [ "gaudi::DeVPStaticConditionCall", "df/d18/classgaudi_1_1DeVPStaticConditionCall.html", null ],
      [ "gaudi::DeVeloIOVConditionCall", "d6/ddb/classgaudi_1_1DeVeloIOVConditionCall.html", [
        [ "gaudi::DeVeloConditionCall", "d5/dc2/classgaudi_1_1DeVeloConditionCall.html", null ]
      ] ],
      [ "gaudi::DeVeloStaticConditionCall", "d8/d41/classgaudi_1_1DeVeloStaticConditionCall.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionUpdateContext", "de/d2d/classdd4hep_1_1cond_1_1ConditionUpdateContext.html", null ],
    [ "dd4hep::cond::ConditionUpdateUserContext", "d5/df1/classdd4hep_1_1cond_1_1ConditionUpdateUserContext.html", [
      [ "gaudi::VPUpdateContext", "da/d47/classgaudi_1_1VPUpdateContext.html", null ],
      [ "gaudi::VeloUpdateContext", "dd/d96/classgaudi_1_1VeloUpdateContext.html", null ]
    ] ],
    [ "dd4hep::DisplayConfiguration::Config", "d5/dbc/classdd4hep_1_1DisplayConfiguration_1_1Config.html", [
      [ "dd4hep::DisplayConfiguration::ViewConfig", "db/d97/classdd4hep_1_1DisplayConfiguration_1_1ViewConfig.html", null ]
    ] ],
    [ "dd4hep::rec::ConicalSupportStruct", "da/dd3/structdd4hep_1_1rec_1_1ConicalSupportStruct.html", null ],
    [ "dd4hep::ConstructionFactory< T >", "d1/db2/classdd4hep_1_1ConstructionFactory.html", null ],
    [ "dd4hep::digi::DigiSubdetectorSequence::Context", "da/d90/classdd4hep_1_1digi_1_1DigiSubdetectorSequence_1_1Context.html", null ],
    [ "dd4hep::ContextMenu", "d6/d97/classdd4hep_1_1ContextMenu.html", null ],
    [ "dd4hep::sim::Geant4Action::ContextSwap", "d0/d2a/classdd4hep_1_1sim_1_1Geant4Action_1_1ContextSwap.html", null ],
    [ "dd4hep::ConversionArg", "d7/dde/structdd4hep_1_1ConversionArg.html", null ],
    [ "dd4hep::Converter< T, ARG >", "d0/d8f/structdd4hep_1_1Converter.html", null ],
    [ "dd4hep::DDCMSDetElementCreator::Count", "d2/da9/structdd4hep_1_1DDCMSDetElementCreator_1_1Count.html", null ],
    [ "dd4hep::DetElementCreator::Count", "d0/d42/structdd4hep_1_1DetElementCreator_1_1Count.html", null ],
    [ "dd4hep::InstanceCount::Counter", "d6/d34/classdd4hep_1_1InstanceCount_1_1Counter.html", null ],
    [ "dd4hep::rec::Vector3D::Cylindrical", "d3/d4c/structdd4hep_1_1rec_1_1Vector3D_1_1Cylindrical.html", null ],
    [ "dd4hep::DDCMSDetElementCreator::Data", "d1/d63/structdd4hep_1_1DDCMSDetElementCreator_1_1Data.html", null ],
    [ "dd4hep::DetElementCreator::Data", "d4/d41/structdd4hep_1_1DetElementCreator_1_1Data.html", null ],
    [ "dd4hep::sim::DataExtension", "d2/d8c/classdd4hep_1_1sim_1_1DataExtension.html", null ],
    [ "dd4hep::dd4hep_file< T >", "d4/ddf/classdd4hep_1_1dd4hep__file.html", [
      [ "dd4hep::dd4hep_file_sink< T >", "d2/d60/classdd4hep_1_1dd4hep__file__sink.html", null ]
    ] ],
    [ "dd4hep::dd4hep_file< int >", "d4/ddf/classdd4hep_1_1dd4hep__file.html", [
      [ "dd4hep::dd4hep_file_source< T >", "df/de2/classdd4hep_1_1dd4hep__file__source.html", null ]
    ] ],
    [ "dd4hep::dd4hep_lock_t", "d0/d0d/structdd4hep_1_1dd4hep__lock__t.html", null ],
    [ "dd4hep::dd4hep_mutex_t", "de/dc1/structdd4hep_1_1dd4hep__mutex__t.html", null ],
    [ "DD4hepRootCheck", "d4/d82/classDD4hepRootCheck.html", null ],
    [ "dd4hep::detail::DD4hepUI", "d2/da6/classdd4hep_1_1detail_1_1DD4hepUI.html", null ],
    [ "dd4hep::DDDB::dddb", "d1/dee/classdd4hep_1_1DDDB_1_1dddb.html", null ],
    [ "dd4hep::DDDB::dddb_conditions", "d3/ddf/classdd4hep_1_1DDDB_1_1dddb__conditions.html", null ],
    [ "dd4hep::DDDB::DDDBAssembly", "d8/d83/classdd4hep_1_1DDDB_1_1DDDBAssembly.html", null ],
    [ "dd4hep::DDDB::DDDBAtom", "df/d42/classdd4hep_1_1DDDB_1_1DDDBAtom.html", null ],
    [ "dd4hep::DDDB::DDDBBooleanOperation", "d4/d34/classdd4hep_1_1DDDB_1_1DDDBBooleanOperation.html", null ],
    [ "dd4hep::DDDB::DDDBBooleanShape", "d9/da2/classdd4hep_1_1DDDB_1_1DDDBBooleanShape.html", [
      [ "dd4hep::DDDB::DDDBBooleanIntersection", "da/d05/classdd4hep_1_1DDDB_1_1DDDBBooleanIntersection.html", null ],
      [ "dd4hep::DDDB::DDDBBooleanSubtraction", "db/d99/classdd4hep_1_1DDDB_1_1DDDBBooleanSubtraction.html", null ],
      [ "dd4hep::DDDB::DDDBBooleanUnion", "d8/d3d/classdd4hep_1_1DDDB_1_1DDDBBooleanUnion.html", null ]
    ] ],
    [ "dd4hep::DDDB::DDDBBox", "d8/d63/classdd4hep_1_1DDDB_1_1DDDBBox.html", null ],
    [ "dd4hep::DDDB::DDDBConeSegment", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html", null ],
    [ "dd4hep::DDDB::DDDBCons", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html", null ],
    [ "dd4hep::DDDB::DDDBEllipsoid", "d1/de0/classdd4hep_1_1DDDB_1_1DDDBEllipsoid.html", null ],
    [ "dd4hep::DDDB::DDDBEllipticalTube", "d7/d7a/classdd4hep_1_1DDDB_1_1DDDBEllipticalTube.html", null ],
    [ "dd4hep::DDDB::DDDBHyperboloid", "d8/ded/classdd4hep_1_1DDDB_1_1DDDBHyperboloid.html", null ],
    [ "dd4hep::DDDB::DDDBMaterialComponent", "d4/d7c/classdd4hep_1_1DDDB_1_1DDDBMaterialComponent.html", null ],
    [ "dd4hep::DDDB::DDDBParaboloid", "dc/dcb/classdd4hep_1_1DDDB_1_1DDDBParaboloid.html", null ],
    [ "dd4hep::DDDB::DDDBPolycone", "d0/d81/classdd4hep_1_1DDDB_1_1DDDBPolycone.html", null ],
    [ "dd4hep::DDDB::DDDBPolygon", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html", null ],
    [ "dd4hep::DDDB::DDDBSphere", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html", null ],
    [ "dd4hep::DDDB::DDDBTorus", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html", null ],
    [ "dd4hep::DDDB::DDDBTrap", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html", null ],
    [ "dd4hep::DDDB::DDDBTRD", "de/df6/classdd4hep_1_1DDDB_1_1DDDBTRD.html", null ],
    [ "dd4hep::DDDB::DDDBTubs", "dd/d9f/classdd4hep_1_1DDDB_1_1DDDBTubs.html", null ],
    [ "dd4hep::DDDB::DDDBZPlane", "d3/d65/classdd4hep_1_1DDDB_1_1DDDBZPlane.html", null ],
    [ "dd4hep::DDEve", "d3/d99/structdd4hep_1_1DDEve.html", null ],
    [ "dd4hep::DDEveHit", "d3/d94/classdd4hep_1_1DDEveHit.html", null ],
    [ "dd4hep::DDEveHitActor", "d7/d3a/structdd4hep_1_1DDEveHitActor.html", [
      [ "dd4hep::BoxsetCreator", "d7/ddd/structdd4hep_1_1BoxsetCreator.html", [
        [ "dd4hep::TowersetCreator", "df/df2/structdd4hep_1_1TowersetCreator.html", null ]
      ] ],
      [ "dd4hep::EtaPhiHistogramActor", "d8/dbe/structdd4hep_1_1EtaPhiHistogramActor.html", null ],
      [ "dd4hep::PointsetCreator", "dd/dba/structdd4hep_1_1PointsetCreator.html", null ]
    ] ],
    [ "dd4hep::DDEveParticle", "d9/d96/classdd4hep_1_1DDEveParticle.html", null ],
    [ "dd4hep::DDEveParticleActor", "d5/d3e/structdd4hep_1_1DDEveParticleActor.html", [
      [ "dd4hep::MCParticleCreator", "df/d3e/structdd4hep_1_1MCParticleCreator.html", null ],
      [ "dd4hep::StartVertexCreator", "d8/d02/structdd4hep_1_1StartVertexCreator.html", null ]
    ] ],
    [ "dd4hep::DDPython", "d8/dd7/classdd4hep_1_1DDPython.html", null ],
    [ "dd4hep::DDTest", "d7/d48/classdd4hep_1_1DDTest.html", null ],
    [ "Gaudi::PluginService::v2::DeclareFactory< T, F >", "dc/d01/structGaudi_1_1PluginService_1_1v2_1_1DeclareFactory.html", null ],
    [ "gaudi::DeConditionCallDefs", "dc/d90/classgaudi_1_1DeConditionCallDefs.html", [
      [ "gaudi::DeVPIOVConditionCall", "da/dbc/classgaudi_1_1DeVPIOVConditionCall.html", null ],
      [ "gaudi::DeVPStaticConditionCall", "df/d18/classgaudi_1_1DeVPStaticConditionCall.html", null ],
      [ "gaudi::DeVeloIOVConditionCall", "d6/ddb/classgaudi_1_1DeVeloIOVConditionCall.html", null ],
      [ "gaudi::DeVeloStaticConditionCall", "d8/d41/classgaudi_1_1DeVeloStaticConditionCall.html", null ]
    ] ],
    [ "dd4hep::DisplayConfiguration::Defaults", "d1/d45/structdd4hep_1_1DisplayConfiguration_1_1Defaults.html", [
      [ "dd4hep::DisplayConfiguration::Calo3D", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html", null ],
      [ "dd4hep::DisplayConfiguration::Calodata", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html", null ],
      [ "dd4hep::DisplayConfiguration::Hits", "d5/d16/structdd4hep_1_1DisplayConfiguration_1_1Hits.html", null ],
      [ "dd4hep::DisplayConfiguration::Panel", "d6/dea/structdd4hep_1_1DisplayConfiguration_1_1Panel.html", null ]
    ] ],
    [ "gaudi::DeHelpers", "d8/d0e/structgaudi_1_1DeHelpers.html", null ],
    [ "dd4hep::Delta", "dd/d2d/classdd4hep_1_1Delta.html", null ],
    [ "dd4hep::align::DeltaCollector< T >", "df/d18/classdd4hep_1_1align_1_1DeltaCollector.html", null ],
    [ "dd4hep::cond::DependencyBuilder", "d7/d14/classdd4hep_1_1cond_1_1DependencyBuilder.html", null ],
    [ "dd4hep::detail::DestroyFirst< M >", "dd/d07/classdd4hep_1_1detail_1_1DestroyFirst.html", null ],
    [ "dd4hep::detail::DestroyHandle< T >", "da/de5/classdd4hep_1_1detail_1_1DestroyHandle.html", null ],
    [ "dd4hep::detail::DestroyHandles< M >", "d1/d7a/classdd4hep_1_1detail_1_1DestroyHandles.html", null ],
    [ "dd4hep::detail::DestroyObject< T >", "d1/d01/classdd4hep_1_1detail_1_1DestroyObject.html", null ],
    [ "dd4hep::detail::DestroyObjects< M >", "dc/df9/classdd4hep_1_1detail_1_1DestroyObjects.html", null ],
    [ "dd4hep::Path::detail", "dc/d79/classdd4hep_1_1Path_1_1detail.html", null ],
    [ "dd4hep::Detector", "df/d70/classdd4hep_1_1Detector.html", [
      [ "dd4hep::DetectorImp", "d5/dc6/classdd4hep_1_1DetectorImp.html", null ]
    ] ],
    [ "dd4hep::DetectorData", "d7/d96/classdd4hep_1_1DetectorData.html", [
      [ "dd4hep::DetectorImp", "d5/dc6/classdd4hep_1_1DetectorImp.html", null ]
    ] ],
    [ "dd4hep::DetectorLoad", "d5/d54/classdd4hep_1_1DetectorLoad.html", [
      [ "dd4hep::DetectorImp", "d5/dc6/classdd4hep_1_1DetectorImp.html", null ]
    ] ],
    [ "dd4hep::DetectorProcessor", "de/dec/classdd4hep_1_1DetectorProcessor.html", [
      [ "dd4hep::DetElementProcessor< T >", "d8/dc6/classdd4hep_1_1DetElementProcessor.html", null ],
      [ "dd4hep::DetectorProcessorShared< T >", "d1/d74/classdd4hep_1_1DetectorProcessorShared.html", null ]
    ] ],
    [ "dd4hep::DetectorScanner", "d9/d25/classdd4hep_1_1DetectorScanner.html", null ],
    [ "dd4hep::DetectorSelector", "d3/d6e/classdd4hep_1_1DetectorSelector.html", null ],
    [ "dd4hep::DetElementsCollector< T >", "d1/d85/classdd4hep_1_1DetElementsCollector.html", null ],
    [ "dd4hep::DetNominalCreator", "d8/df8/classdd4hep_1_1DetNominalCreator.html", null ],
    [ "dd4hep::DetType", "dc/db5/classdd4hep_1_1DetType.html", null ],
    [ "boost::iostreams::device_tag", null, [
      [ "dd4hep::dd4hep_file_sink< T >::category", "d9/df3/structdd4hep_1_1dd4hep__file__sink_1_1category.html", null ],
      [ "dd4hep::dd4hep_file_source< T >::category", "d8/d7a/structdd4hep_1_1dd4hep__file__source_1_1category.html", null ]
    ] ],
    [ "dd4hep::digi::DigiAction", "de/d3e/classdd4hep_1_1digi_1_1DigiAction.html", [
      [ "dd4hep::digi::DigiEventAction", "de/d2b/classdd4hep_1_1digi_1_1DigiEventAction.html", [
        [ "dd4hep::digi::DigiInputAction", "d3/dd9/classdd4hep_1_1digi_1_1DigiInputAction.html", [
          [ "dd4hep::digi::DigiDDG4Input", "df/df5/classdd4hep_1_1digi_1_1DigiDDG4Input.html", null ]
        ] ],
        [ "dd4hep::digi::DigiLockedAction", "df/d64/classdd4hep_1_1digi_1_1DigiLockedAction.html", null ],
        [ "dd4hep::digi::DigiRandomNoise", "d7/dad/classdd4hep_1_1digi_1_1DigiRandomNoise.html", null ],
        [ "dd4hep::digi::DigiSynchronize", "dd/d75/classdd4hep_1_1digi_1_1DigiSynchronize.html", [
          [ "dd4hep::digi::DigiActionSequence", "dc/d3b/classdd4hep_1_1digi_1_1DigiActionSequence.html", [
            [ "dd4hep::digi::DigiSubdetectorSequence", "dd/d9b/classdd4hep_1_1digi_1_1DigiSubdetectorSequence.html", null ]
          ] ]
        ] ],
        [ "dd4hep::digi::DigiTestAction", "d2/deb/classdd4hep_1_1digi_1_1DigiTestAction.html", null ]
      ] ],
      [ "dd4hep::digi::DigiSignalProcessor", "d0/d3f/classdd4hep_1_1digi_1_1DigiSignalProcessor.html", [
        [ "dd4hep::digi::DigiAttenuator", "d8/d0d/classdd4hep_1_1digi_1_1DigiAttenuator.html", null ],
        [ "dd4hep::digi::DigiExponentialNoise", "de/d52/classdd4hep_1_1digi_1_1DigiExponentialNoise.html", null ],
        [ "dd4hep::digi::DigiGaussianNoise", "d1/d03/classdd4hep_1_1digi_1_1DigiGaussianNoise.html", null ],
        [ "dd4hep::digi::DigiLandauNoise", "d1/d8b/classdd4hep_1_1digi_1_1DigiLandauNoise.html", null ],
        [ "dd4hep::digi::DigiPoissonNoise", "dd/d85/classdd4hep_1_1digi_1_1DigiPoissonNoise.html", null ],
        [ "dd4hep::digi::DigiRandomNoise", "d7/dad/classdd4hep_1_1digi_1_1DigiRandomNoise.html", null ],
        [ "dd4hep::digi::DigiSignalProcessorSequence", "d2/d0e/classdd4hep_1_1digi_1_1DigiSignalProcessorSequence.html", null ],
        [ "dd4hep::digi::DigiTestSignalProcessor", "da/d51/classdd4hep_1_1digi_1_1DigiTestSignalProcessor.html", null ],
        [ "dd4hep::digi::DigiUniformNoise", "d1/d12/classdd4hep_1_1digi_1_1DigiUniformNoise.html", null ]
      ] ]
    ] ],
    [ "dd4hep::digi::DigiCellContext", "d7/d40/classdd4hep_1_1digi_1_1DigiCellContext.html", null ],
    [ "dd4hep::digi::DigiCellData", "d5/d1d/classdd4hep_1_1digi_1_1DigiCellData.html", [
      [ "dd4hep::digi::cell_data< CartesianGridXY >", "db/d62/classdd4hep_1_1digi_1_1cell__data_3_01CartesianGridXY_01_4.html", null ],
      [ "dd4hep::digi::cell_data< CartesianGridXYZ >", "d5/ddb/classdd4hep_1_1digi_1_1cell__data_3_01CartesianGridXYZ_01_4.html", null ]
    ] ],
    [ "dd4hep::digi::DigiCellScanner", "d0/d62/classdd4hep_1_1digi_1_1DigiCellScanner.html", [
      [ "dd4hep::digi::CellScanner< SEGMENTATION, SOLID >", "d8/dc1/classdd4hep_1_1digi_1_1CellScanner.html", null ]
    ] ],
    [ "dd4hep::digi::DigiContext", "d1/d9a/classdd4hep_1_1digi_1_1DigiContext.html", null ],
    [ "dd4hep::digi::DigiCount", "d2/dab/classdd4hep_1_1digi_1_1DigiCount.html", null ],
    [ "dd4hep::digi::DigiHandle< TYPE >", "d3/d3d/classdd4hep_1_1digi_1_1DigiHandle.html", null ],
    [ "dd4hep::digi::DigiInputs", "db/d08/classdd4hep_1_1digi_1_1DigiInputs.html", null ],
    [ "dd4hep::digi::DigiKernel", "d2/d79/classdd4hep_1_1digi_1_1DigiKernel.html", null ],
    [ "dd4hep::digi::DigiOutputs", "d4/d79/classdd4hep_1_1digi_1_1DigiOutputs.html", null ],
    [ "dd4hep::digi::DigiRandomGenerator", "d7/d6d/classdd4hep_1_1digi_1_1DigiRandomGenerator.html", null ],
    [ "dd4hep::digi::DigiStore", "d1/dab/classdd4hep_1_1digi_1_1DigiStore.html", null ],
    [ "DDDigi.Digitize", "da/d57/classDDDigi_1_1Digitize.html", null ],
    [ "xml::Dimension", null, [
      [ "dd4hep::DDDB::dddb_dim_t", "d0/d6f/structdd4hep_1_1DDDB_1_1dddb__dim__t.html", null ]
    ] ],
    [ "dd4hep::DisplayConfiguration", "d1/d89/classdd4hep_1_1DisplayConfiguration.html", null ],
    [ "dd4hep::json::Document", "d6/ded/classdd4hep_1_1json_1_1Document.html", [
      [ "dd4hep::json::DocumentHolder", "dc/d1d/classdd4hep_1_1json_1_1DocumentHolder.html", null ]
    ] ],
    [ "dd4hep::xml::Document", "dc/d7a/classdd4hep_1_1xml_1_1Document.html", [
      [ "dd4hep::xml::DocumentHolder", "d3/df3/classdd4hep_1_1xml_1_1DocumentHolder.html", null ]
    ] ],
    [ "dd4hep::json::DocumentHandler", "d4/d04/classdd4hep_1_1json_1_1DocumentHandler.html", null ],
    [ "dd4hep::xml::DocumentHandler", "d3/d25/classdd4hep_1_1xml_1_1DocumentHandler.html", null ],
    [ "DOMErrorHandler", "d4/dad/classDOMErrorHandler.html", [
      [ "dd4hep::xml::DocumentErrorHandler", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html", null ]
    ] ],
    [ "SomeExperiment::Dump", "d6/dc7/classSomeExperiment_1_1Dump.html", null ],
    [ "dd4hep::json::Element", "de/d78/classdd4hep_1_1json_1_1Element.html", null ],
    [ "dd4hep::xml::Element", "d3/dc6/classdd4hep_1_1xml_1_1Element.html", [
      [ "dd4hep::xml::LayeringCnv", "d7/dc1/classdd4hep_1_1xml_1_1LayeringCnv.html", null ],
      [ "dd4hep::xml::RefElement", "dc/d57/classdd4hep_1_1xml_1_1RefElement.html", null ]
    ] ],
    [ "Element", null, [
      [ "dd4hep::DD4HEP_DIMENSION_NS::ChildValue", "d5/d32/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1ChildValue.html", null ],
      [ "dd4hep::DD4HEP_DIMENSION_NS::Dimension", "d3/d5d/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1Dimension.html", [
        [ "dd4hep::DD4HEP_DIMENSION_NS::Component", "d3/d2f/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1Component.html", null ],
        [ "dd4hep::DD4HEP_DIMENSION_NS::DetElement", "d5/dfe/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1DetElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::digi::EnergyDeposit", "d4/d19/classdd4hep_1_1digi_1_1EnergyDeposit.html", [
      [ "dd4hep::digi::CaloDeposit", "db/d0c/classdd4hep_1_1digi_1_1CaloDeposit.html", null ],
      [ "dd4hep::digi::TrackerDeposit", "d2/ddc/classdd4hep_1_1digi_1_1TrackerDeposit.html", null ]
    ] ],
    [ "TiXmlBase::Entity", "d8/d33/structTiXmlBase_1_1Entity.html", null ],
    [ "align::AlignmentsCalib::Entry", "d4/d42/classAlignmentsCalib_1_1Entry.html", null ],
    [ "dd4hep::cond::ConditionsRepository::Entry", "d8/d2d/classdd4hep_1_1cond_1_1ConditionsRepository_1_1Entry.html", null ],
    [ "dd4hep::cond::ConditionsTextRepository::Entry", "d7/d58/classdd4hep_1_1cond_1_1ConditionsTextRepository_1_1Entry.html", null ],
    [ "dd4hep::DDSegmentation::MultiSegmentation::Entry", "de/d0d/structdd4hep_1_1DDSegmentation_1_1MultiSegmentation_1_1Entry.html", null ],
    [ "dd4hep::PluginTester::Entry", "d9/d5c/structdd4hep_1_1PluginTester_1_1Entry.html", null ],
    [ "ErrorHandler", "d8/d53/classErrorHandler.html", [
      [ "dd4hep::xml::DocumentErrorHandler", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html", null ]
    ] ],
    [ "dd4hep::detail::eval", "db/dd4/structdd4hep_1_1detail_1_1eval.html", null ],
    [ "dd4hep::tools::Evaluator::Object::EvalStatus", "d6/d59/structdd4hep_1_1tools_1_1Evaluator_1_1Object_1_1EvalStatus.html", null ],
    [ "dd4hep::tools::Evaluator", "d4/d72/classdd4hep_1_1tools_1_1Evaluator.html", null ],
    [ "dd4hep::EventConsumer", "d3/d02/classdd4hep_1_1EventConsumer.html", [
      [ "dd4hep::Display", "d3/dcb/classdd4hep_1_1Display.html", null ],
      [ "dd4hep::EventControl", "d1/d6f/classdd4hep_1_1EventControl.html", null ]
    ] ],
    [ "dd4hep::EventHandler", "d2/dd2/classdd4hep_1_1EventHandler.html", [
      [ "dd4hep::DDG4EventHandler", "dd/d5b/classdd4hep_1_1DDG4EventHandler.html", null ],
      [ "dd4hep::GenericEventHandler", "d5/d97/classdd4hep_1_1GenericEventHandler.html", null ],
      [ "dd4hep::LCIOEventHandler", "d5/def/classdd4hep_1_1LCIOEventHandler.html", null ]
    ] ],
    [ "dd4hep::sim::HepMC::EventHeader", "d7/d11/classdd4hep_1_1sim_1_1HepMC_1_1EventHeader.html", null ],
    [ "dd4hep::sim::EventParameters", "d9/d1c/classdd4hep_1_1sim_1_1EventParameters.html", null ],
    [ "dd4hep::sim::HepMC::EventStream", "d8/d92/classdd4hep_1_1sim_1_1HepMC_1_1EventStream.html", null ],
    [ "dd4hep::EveUserContextMenu", "d5/d87/classdd4hep_1_1EveUserContextMenu.html", [
      [ "dd4hep::ElementListContextMenu", "d4/dc0/classdd4hep_1_1ElementListContextMenu.html", null ],
      [ "dd4hep::EvePgonSetProjectedContextMenu", "d1/db2/classdd4hep_1_1EvePgonSetProjectedContextMenu.html", null ],
      [ "dd4hep::EveShapeContextMenu", "d2/ded/classdd4hep_1_1EveShapeContextMenu.html", null ]
    ] ],
    [ "std::exception", null, [
      [ "DD4hep_End_Of_File", "d5/de4/classDD4hep__End__Of__File.html", null ],
      [ "Gaudi::PluginService::v1::Exception", "d2/dc3/classGaudi_1_1PluginService_1_1v1_1_1Exception.html", null ]
    ] ],
    [ "dd4hep::ExtensionEntry", "d4/dd5/classdd4hep_1_1ExtensionEntry.html", [
      [ "dd4hep::DetElement::DetElementExtension< Q, T >", "d3/de5/classdd4hep_1_1DetElement_1_1DetElementExtension.html", null ],
      [ "dd4hep::detail::CopyDeleteExtension< Q, T >", "d5/d89/classdd4hep_1_1detail_1_1CopyDeleteExtension.html", null ],
      [ "dd4hep::detail::DeleteExtension< Q, T >", "d2/d26/classdd4hep_1_1detail_1_1DeleteExtension.html", null ],
      [ "dd4hep::detail::SimpleExtension< Q, T >", "de/d4d/classdd4hep_1_1detail_1_1SimpleExtension.html", null ]
    ] ],
    [ "Gaudi::PluginService::v1::Details::Factory< T >", "d4/db2/classGaudi_1_1PluginService_1_1v1_1_1Details_1_1Factory.html", null ],
    [ "Gaudi::PluginService::v1::Factory< R, Args >", "dc/d09/classGaudi_1_1PluginService_1_1v1_1_1Factory.html", null ],
    [ "Gaudi::PluginService::v2::Factory< R(Args...)>", "d2/d54/structGaudi_1_1PluginService_1_1v2_1_1Factory_3_01R_07Args_8_8_8_08_4.html", null ],
    [ "Gaudi::PluginService::v1::Details::Registry::FactoryInfo", "d2/deb/structGaudi_1_1PluginService_1_1v1_1_1Details_1_1Registry_1_1FactoryInfo.html", null ],
    [ "dd4hep::detail::FalphaNoise", "d2/ddf/classdd4hep_1_1detail_1_1FalphaNoise.html", null ],
    [ "dd4hep::Filter", "d1/dd9/structdd4hep_1_1Filter.html", null ],
    [ "Filter", "d6/d81/classFilter.html", null ],
    [ "dd4hep::digi::DigiAction::FindByName", "de/d6c/structdd4hep_1_1digi_1_1DigiAction_1_1FindByName.html", null ],
    [ "dd4hep::sim::Geant4Action::FindByName", "dd/dd3/structdd4hep_1_1sim_1_1Geant4Action_1_1FindByName.html", null ],
    [ "FindString", "d0/d31/structFindString.html", null ],
    [ "dd4hep::Parsers::KeyValueGrammar< Iterator, Skipper >::first", "df/d04/structdd4hep_1_1Parsers_1_1KeyValueGrammar_1_1first.html", null ],
    [ "dd4hep::Parsers::PairGrammar< Iterator, PairT, Skipper >::first", "d1/db1/structdd4hep_1_1Parsers_1_1PairGrammar_1_1first.html", null ],
    [ "dd4hep::rec::FixedPadSizeTPCStruct", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html", null ],
    [ "dd4hep::digi::EnergyDeposit::FunctionTable", "d1/dd1/classdd4hep_1_1digi_1_1EnergyDeposit_1_1FunctionTable.html", [
      [ "dd4hep::digi::CaloDeposit::FunctionTable", "dd/d91/classdd4hep_1_1digi_1_1CaloDeposit_1_1FunctionTable.html", null ],
      [ "dd4hep::digi::TrackerDeposit::FunctionTable", "d3/d92/classdd4hep_1_1digi_1_1TrackerDeposit_1_1FunctionTable.html", null ]
    ] ],
    [ "dd4hep::Callback::Wrapper< T >::Functor", "d0/d15/uniondd4hep_1_1Callback_1_1Wrapper_1_1Functor.html", null ],
    [ "G4AssemblyVolume", "d8/d10/classG4AssemblyVolume.html", null ],
    [ "G4MagneticField", "d8/d9d/classG4MagneticField.html", [
      [ "dd4hep::sim::Geant4Field", "dc/de9/classdd4hep_1_1sim_1_1Geant4Field.html", null ]
    ] ],
    [ "G4UImessenger", "d7/d0e/classG4UImessenger.html", [
      [ "dd4hep::sim::Geant4UIMessenger", "d9/d1d/classdd4hep_1_1sim_1_1Geant4UIMessenger.html", null ]
    ] ],
    [ "G4UserEventAction", "de/d75/classG4UserEventAction.html", [
      [ "dd4hep::sim::Geant4UserEventAction", "df/d1d/classdd4hep_1_1sim_1_1Geant4UserEventAction.html", null ]
    ] ],
    [ "G4UserLimits", "d9/d11/classG4UserLimits.html", [
      [ "dd4hep::sim::Geant4UserLimits", "dc/d7c/classdd4hep_1_1sim_1_1Geant4UserLimits.html", null ]
    ] ],
    [ "G4UserRunAction", "dd/d05/classG4UserRunAction.html", [
      [ "dd4hep::sim::Geant4UserRunAction", "de/ded/classdd4hep_1_1sim_1_1Geant4UserRunAction.html", null ]
    ] ],
    [ "G4UserStackingAction", "d9/d8c/classG4UserStackingAction.html", [
      [ "dd4hep::sim::Geant4UserStackingAction", "d5/d63/classdd4hep_1_1sim_1_1Geant4UserStackingAction.html", null ]
    ] ],
    [ "G4UserSteppingAction", "dd/d17/classG4UserSteppingAction.html", [
      [ "dd4hep::sim::Geant4UserSteppingAction", "d6/d48/classdd4hep_1_1sim_1_1Geant4UserSteppingAction.html", null ]
    ] ],
    [ "G4UserTrackingAction", "d4/d9f/classG4UserTrackingAction.html", [
      [ "dd4hep::sim::Geant4UserTrackingAction", "df/de2/classdd4hep_1_1sim_1_1Geant4UserTrackingAction.html", null ]
    ] ],
    [ "G4VHit", "d9/da3/classG4VHit.html", [
      [ "dd4hep::sim::Geant4Hit", "d9/dcc/classdd4hep_1_1sim_1_1Geant4Hit.html", [
        [ "dd4hep::sim::Geant4CalorimeterHit", "d2/d27/classdd4hep_1_1sim_1_1Geant4CalorimeterHit.html", null ],
        [ "dd4hep::sim::Geant4TrackerHit", "d3/d65/classdd4hep_1_1sim_1_1Geant4TrackerHit.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4HitWrapper", "d7/d8b/classdd4hep_1_1sim_1_1Geant4HitWrapper.html", null ]
    ] ],
    [ "G4VHitsCollection", "dc/d91/classG4VHitsCollection.html", [
      [ "dd4hep::sim::Geant4HitCollection", "dd/d1a/classdd4hep_1_1sim_1_1Geant4HitCollection.html", null ]
    ] ],
    [ "G4VPhysicsConstructor", "d1/d2f/classG4VPhysicsConstructor.html", [
      [ "dd4hep::sim::Geant4PhysicsConstructor::Constructor", "df/d27/classdd4hep_1_1sim_1_1Geant4PhysicsConstructor_1_1Constructor.html", null ]
    ] ],
    [ "G4VSDFilter", "d5/d2d/classG4VSDFilter.html", [
      [ "dd4hep::sim::Geant4SensDet", "df/d9f/classdd4hep_1_1sim_1_1Geant4SensDet.html", null ]
    ] ],
    [ "G4VSensitiveDetector", "df/dbc/classG4VSensitiveDetector.html", [
      [ "dd4hep::sim::Geant4SensDet", "df/d9f/classdd4hep_1_1sim_1_1Geant4SensDet.html", null ]
    ] ],
    [ "G4VUserActionInitialization", "d0/d77/classG4VUserActionInitialization.html", [
      [ "dd4hep::sim::Geant4UserActionInitialization", "d2/d77/classdd4hep_1_1sim_1_1Geant4UserActionInitialization.html", null ]
    ] ],
    [ "G4VUserDetectorConstruction", "d6/df8/classG4VUserDetectorConstruction.html", [
      [ "dd4hep::sim::Geant4GDMLDetector", "d4/d79/classdd4hep_1_1sim_1_1Geant4GDMLDetector.html", null ],
      [ "dd4hep::sim::Geant4UserDetectorConstruction", "d2/da1/classdd4hep_1_1sim_1_1Geant4UserDetectorConstruction.html", null ]
    ] ],
    [ "G4VUserPrimaryGeneratorAction", "d4/dbc/classG4VUserPrimaryGeneratorAction.html", [
      [ "dd4hep::sim::Geant4UserGeneratorAction", "d7/d26/classdd4hep_1_1sim_1_1Geant4UserGeneratorAction.html", null ]
    ] ],
    [ "G4VUserTrackInformation", "df/de2/classG4VUserTrackInformation.html", [
      [ "dd4hep::sim::Geant4TrackInformation", "d4/d08/classdd4hep_1_1sim_1_1Geant4TrackInformation.html", null ]
    ] ],
    [ "DDG4.Geant4", "d6/d59/classDDG4_1_1Geant4.html", null ],
    [ "dd4hep::sim::Geant4Action", "dd/d7c/classdd4hep_1_1sim_1_1Geant4Action.html", [
      [ "dd4hep::sim::Geant4ActionPhase", "d2/d3e/classdd4hep_1_1sim_1_1Geant4ActionPhase.html", null ],
      [ "dd4hep::sim::Geant4ActionSD", "d0/d9a/classdd4hep_1_1sim_1_1Geant4ActionSD.html", [
        [ "dd4hep::sim::Geant4SensDet", "df/d9f/classdd4hep_1_1sim_1_1Geant4SensDet.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4DetectorConstruction", "da/d5e/classdd4hep_1_1sim_1_1Geant4DetectorConstruction.html", [
        [ "dd4hep::sim::Geant4DetectorGeometryConstruction", "d4/d1e/classdd4hep_1_1sim_1_1Geant4DetectorGeometryConstruction.html", null ],
        [ "dd4hep::sim::Geant4DetectorSensitivesConstruction", "da/d8e/classdd4hep_1_1sim_1_1Geant4DetectorSensitivesConstruction.html", null ],
        [ "dd4hep::sim::Geant4FieldTrackingConstruction", "d7/d6b/classdd4hep_1_1sim_1_1Geant4FieldTrackingConstruction.html", null ],
        [ "dd4hep::sim::Geant4PythonDetectorConstruction", "df/d46/classdd4hep_1_1sim_1_1Geant4PythonDetectorConstruction.html", null ],
        [ "dd4hep::sim::Geant4PythonDetectorConstructionLast", "d2/d2f/classdd4hep_1_1sim_1_1Geant4PythonDetectorConstructionLast.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4DetectorConstructionSequence", "da/d04/classdd4hep_1_1sim_1_1Geant4DetectorConstructionSequence.html", null ],
      [ "dd4hep::sim::Geant4DummyTruthHandler", "d8/d4d/classdd4hep_1_1sim_1_1Geant4DummyTruthHandler.html", null ],
      [ "dd4hep::sim::Geant4EventAction", "d2/d4b/classdd4hep_1_1sim_1_1Geant4EventAction.html", [
        [ "dd4hep::sim::Geant4HitDumpAction", "d2/da8/classdd4hep_1_1sim_1_1Geant4HitDumpAction.html", null ],
        [ "dd4hep::sim::Geant4HitTruthHandler", "d0/d24/classdd4hep_1_1sim_1_1Geant4HitTruthHandler.html", null ],
        [ "dd4hep::sim::Geant4OutputAction", "d3/d38/classdd4hep_1_1sim_1_1Geant4OutputAction.html", [
          [ "dd4hep::sim::Geant4Output2EDM4hep", "d0/d31/classdd4hep_1_1sim_1_1Geant4Output2EDM4hep.html", null ],
          [ "dd4hep::sim::Geant4Output2LCIO", "d5/de7/classdd4hep_1_1sim_1_1Geant4Output2LCIO.html", null ],
          [ "dd4hep::sim::Geant4Output2ROOT", "dc/d14/classdd4hep_1_1sim_1_1Geant4Output2ROOT.html", null ]
        ] ],
        [ "dd4hep::sim::Geant4ParticleDumpAction", "da/d0b/classdd4hep_1_1sim_1_1Geant4ParticleDumpAction.html", null ],
        [ "dd4hep::sim::Geant4ParticlePrint", "d0/d98/classdd4hep_1_1sim_1_1Geant4ParticlePrint.html", null ],
        [ "dd4hep::sim::Geant4SharedEventAction", "db/d30/classdd4hep_1_1sim_1_1Geant4SharedEventAction.html", null ],
        [ "dd4hep::sim::Geant4SurfaceTest", "da/dcb/classdd4hep_1_1sim_1_1Geant4SurfaceTest.html", null ],
        [ "dd4hep::sim::Test::Geant4TestEventAction", "d3/d58/classdd4hep_1_1sim_1_1Test_1_1Geant4TestEventAction.html", null ],
        [ "myanalysis::HitTupleAction", "dd/d4d/classmyanalysis_1_1HitTupleAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4EventActionSequence", "da/d6e/classdd4hep_1_1sim_1_1Geant4EventActionSequence.html", null ],
      [ "dd4hep::sim::Geant4Filter", "dd/d6e/classdd4hep_1_1sim_1_1Geant4Filter.html", [
        [ "dd4hep::sim::EnergyDepositMinimumCut", "d9/df3/structdd4hep_1_1sim_1_1EnergyDepositMinimumCut.html", null ],
        [ "dd4hep::sim::Geant4ReadoutVolumeFilter", "dc/d6e/classdd4hep_1_1sim_1_1Geant4ReadoutVolumeFilter.html", null ],
        [ "dd4hep::sim::ParticleFilter", "d9/dd7/structdd4hep_1_1sim_1_1ParticleFilter.html", [
          [ "dd4hep::sim::GeantinoRejectFilter", "de/d38/structdd4hep_1_1sim_1_1GeantinoRejectFilter.html", null ],
          [ "dd4hep::sim::ParticleRejectFilter", "dd/d53/structdd4hep_1_1sim_1_1ParticleRejectFilter.html", null ],
          [ "dd4hep::sim::ParticleSelectFilter", "de/df7/structdd4hep_1_1sim_1_1ParticleSelectFilter.html", null ]
        ] ]
      ] ],
      [ "dd4hep::sim::Geant4GDMLWriteAction", "da/d36/classdd4hep_1_1sim_1_1Geant4GDMLWriteAction.html", null ],
      [ "dd4hep::sim::Geant4GeneratorAction", "df/dcc/classdd4hep_1_1sim_1_1Geant4GeneratorAction.html", [
        [ "dd4hep::sim::Geant4GeneratorActionInit", "dd/d7c/classdd4hep_1_1sim_1_1Geant4GeneratorActionInit.html", null ],
        [ "dd4hep::sim::Geant4GeneratorWrapper", "d3/dae/classdd4hep_1_1sim_1_1Geant4GeneratorWrapper.html", null ],
        [ "dd4hep::sim::Geant4InputAction", "df/d3b/classdd4hep_1_1sim_1_1Geant4InputAction.html", null ],
        [ "dd4hep::sim::Geant4InteractionMerger", "dc/d73/classdd4hep_1_1sim_1_1Geant4InteractionMerger.html", null ],
        [ "dd4hep::sim::Geant4InteractionVertexBoost", "da/d75/classdd4hep_1_1sim_1_1Geant4InteractionVertexBoost.html", null ],
        [ "dd4hep::sim::Geant4InteractionVertexSmear", "d7/d83/classdd4hep_1_1sim_1_1Geant4InteractionVertexSmear.html", null ],
        [ "dd4hep::sim::Geant4ParticleGenerator", "d0/d2f/classdd4hep_1_1sim_1_1Geant4ParticleGenerator.html", [
          [ "dd4hep::sim::Geant4IsotropeGenerator", "db/dd0/classdd4hep_1_1sim_1_1Geant4IsotropeGenerator.html", [
            [ "dd4hep::sim::Geant4ParticleGun", "d8/d95/classdd4hep_1_1sim_1_1Geant4ParticleGun.html", null ]
          ] ]
        ] ],
        [ "dd4hep::sim::Geant4ParticleHandler", "d0/dac/classdd4hep_1_1sim_1_1Geant4ParticleHandler.html", null ],
        [ "dd4hep::sim::Geant4PrimaryHandler", "da/d52/classdd4hep_1_1sim_1_1Geant4PrimaryHandler.html", null ],
        [ "dd4hep::sim::Geant4SharedGeneratorAction", "dc/d32/classdd4hep_1_1sim_1_1Geant4SharedGeneratorAction.html", null ],
        [ "dd4hep::sim::Test::Geant4TestGeneratorAction", "d5/de1/classdd4hep_1_1sim_1_1Test_1_1Geant4TestGeneratorAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4GeneratorActionSequence", "d3/d2a/classdd4hep_1_1sim_1_1Geant4GeneratorActionSequence.html", null ],
      [ "dd4hep::sim::Geant4PhaseAction", "d7/db1/classdd4hep_1_1sim_1_1Geant4PhaseAction.html", [
        [ "dd4hep::sim::Geant4FieldTrackingSetupAction", "d0/de3/classdd4hep_1_1sim_1_1Geant4FieldTrackingSetupAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4PhysicsList", "dd/da8/classdd4hep_1_1sim_1_1Geant4PhysicsList.html", [
        [ "dd4hep::sim::Geant4CerenkovPhysics", "db/de1/classdd4hep_1_1sim_1_1Geant4CerenkovPhysics.html", null ],
        [ "dd4hep::sim::Geant4DefaultRangeCut", "d6/d67/classdd4hep_1_1sim_1_1Geant4DefaultRangeCut.html", null ],
        [ "dd4hep::sim::Geant4OpticalPhotonPhysics", "de/d6c/classdd4hep_1_1sim_1_1Geant4OpticalPhotonPhysics.html", null ],
        [ "dd4hep::sim::Geant4PhysicsConstructor", "dd/d33/classdd4hep_1_1sim_1_1Geant4PhysicsConstructor.html", [
          [ "dd4hep::sim::Geant4ExtraParticles", "dd/d15/classdd4hep_1_1sim_1_1Geant4ExtraParticles.html", null ]
        ] ],
        [ "dd4hep::sim::Geant4ScintillationPhysics", "d9/d6f/classdd4hep_1_1sim_1_1Geant4ScintillationPhysics.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4PhysicsListActionSequence", "d8/de4/classdd4hep_1_1sim_1_1Geant4PhysicsListActionSequence.html", null ],
      [ "dd4hep::sim::Geant4PythonAction", "d7/d8a/classdd4hep_1_1sim_1_1Geant4PythonAction.html", null ],
      [ "dd4hep::sim::Geant4Random", "d1/d8f/classdd4hep_1_1sim_1_1Geant4Random.html", null ],
      [ "dd4hep::sim::Geant4RunAction", "d9/de9/classdd4hep_1_1sim_1_1Geant4RunAction.html", [
        [ "dd4hep::sim::Geant4EventSeed", "d3/d63/classdd4hep_1_1sim_1_1Geant4EventSeed.html", null ],
        [ "dd4hep::sim::Geant4SharedRunAction", "d7/d27/classdd4hep_1_1sim_1_1Geant4SharedRunAction.html", null ],
        [ "dd4hep::sim::Test::Geant4TestRunAction", "d4/d3f/classdd4hep_1_1sim_1_1Test_1_1Geant4TestRunAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4RunActionSequence", "db/dc4/classdd4hep_1_1sim_1_1Geant4RunActionSequence.html", null ],
      [ "dd4hep::sim::Geant4RunManager< RUNMANAGER >", "d9/d7d/classdd4hep_1_1sim_1_1Geant4RunManager.html", null ],
      [ "dd4hep::sim::Geant4SensDetActionSequence", "d3/d3d/classdd4hep_1_1sim_1_1Geant4SensDetActionSequence.html", null ],
      [ "dd4hep::sim::Geant4Sensitive", "da/d70/classdd4hep_1_1sim_1_1Geant4Sensitive.html", [
        [ "Tests::Geant4SensitiveAction< T >", "de/d3d/classTests_1_1Geant4SensitiveAction.html", null ],
        [ "dd4hep::sim::Geant4EscapeCounter", "db/d31/classdd4hep_1_1sim_1_1Geant4EscapeCounter.html", null ],
        [ "dd4hep::sim::Geant4SensitiveAction< T >", "d3/d98/classdd4hep_1_1sim_1_1Geant4SensitiveAction.html", null ],
        [ "dd4hep::sim::Test::Geant4TestSensitive", "da/d89/classdd4hep_1_1sim_1_1Test_1_1Geant4TestSensitive.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4StackingAction", "d1/d82/classdd4hep_1_1sim_1_1Geant4StackingAction.html", [
        [ "dd4hep::sim::Geant4SharedStackingAction", "d3/d4e/classdd4hep_1_1sim_1_1Geant4SharedStackingAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4StackingActionSequence", "d5/df7/classdd4hep_1_1sim_1_1Geant4StackingActionSequence.html", null ],
      [ "dd4hep::sim::Geant4SteppingAction", "d1/d8c/classdd4hep_1_1sim_1_1Geant4SteppingAction.html", [
        [ "dd4hep::sim::Geant4GeometryScanner", "d3/d23/classdd4hep_1_1sim_1_1Geant4GeometryScanner.html", null ],
        [ "dd4hep::sim::Geant4MaterialScanner", "d4/d15/classdd4hep_1_1sim_1_1Geant4MaterialScanner.html", null ],
        [ "dd4hep::sim::Geant4SharedSteppingAction", "d1/df5/classdd4hep_1_1sim_1_1Geant4SharedSteppingAction.html", null ],
        [ "dd4hep::sim::Test::Geant4TestStepAction", "da/d74/classdd4hep_1_1sim_1_1Test_1_1Geant4TestStepAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4SteppingActionSequence", "d8/d13/classdd4hep_1_1sim_1_1Geant4SteppingActionSequence.html", null ],
      [ "dd4hep::sim::Geant4TrackingAction", "d0/d22/classdd4hep_1_1sim_1_1Geant4TrackingAction.html", [
        [ "dd4hep::sim::Geant4SharedTrackingAction", "df/d3c/classdd4hep_1_1sim_1_1Geant4SharedTrackingAction.html", null ],
        [ "dd4hep::sim::Geant4TrackingPostAction", "d8/de8/classdd4hep_1_1sim_1_1Geant4TrackingPostAction.html", null ],
        [ "dd4hep::sim::Geant4TrackingPreAction", "dd/d44/classdd4hep_1_1sim_1_1Geant4TrackingPreAction.html", null ],
        [ "dd4hep::sim::Test::Geant4TestTrackAction", "d7/d1b/classdd4hep_1_1sim_1_1Test_1_1Geant4TestTrackAction.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4TrackingActionSequence", "de/d78/classdd4hep_1_1sim_1_1Geant4TrackingActionSequence.html", null ],
      [ "dd4hep::sim::Geant4UIManager", "dd/d98/classdd4hep_1_1sim_1_1Geant4UIManager.html", null ],
      [ "dd4hep::sim::Geant4UserInitialization", "d4/de2/classdd4hep_1_1sim_1_1Geant4UserInitialization.html", [
        [ "dd4hep::sim::Geant4PythonInitialization", "d5/df8/classdd4hep_1_1sim_1_1Geant4PythonInitialization.html", null ],
        [ "dd4hep::sim::Geant4UserInitializationSequence", "dc/d5e/classdd4hep_1_1sim_1_1Geant4UserInitializationSequence.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4UserParticleHandler", "d5/d24/classdd4hep_1_1sim_1_1Geant4UserParticleHandler.html", [
        [ "dd4hep::sim::Geant4TCUserParticleHandler", "d2/d23/classdd4hep_1_1sim_1_1Geant4TCUserParticleHandler.html", null ]
      ] ]
    ] ],
    [ "dd4hep::sim::Geant4ActionContainer", "d7/d3d/classdd4hep_1_1sim_1_1Geant4ActionContainer.html", [
      [ "dd4hep::sim::Geant4Kernel", "da/d63/classdd4hep_1_1sim_1_1Geant4Kernel.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4AssemblyVolume", "da/daf/classdd4hep_1_1sim_1_1Geant4AssemblyVolume.html", null ],
    [ "dd4hep::sim::Geant4Call", "d9/de2/classdd4hep_1_1sim_1_1Geant4Call.html", [
      [ "dd4hep::sim::Geant4UIManager", "dd/d98/classdd4hep_1_1sim_1_1Geant4UIManager.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4Calorimeter", "d2/d9f/classdd4hep_1_1sim_1_1Geant4Calorimeter.html", null ],
    [ "Geant4Compatibility", "df/d57/classGeant4Compatibility.html", null ],
    [ "dd4hep::sim::Geant4Context", "d6/dd0/classdd4hep_1_1sim_1_1Geant4Context.html", null ],
    [ "dd4hep::sim::Geant4ConversionHelper", "dc/d9b/classdd4hep_1_1sim_1_1Geant4ConversionHelper.html", [
      [ "dd4hep::sim::Geant4Conversion< OUTPUT, ARGS >", "d1/db7/classdd4hep_1_1sim_1_1Geant4Conversion.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4DataDump", "d7/d52/classdd4hep_1_1sim_1_1Geant4DataDump.html", null ],
    [ "dd4hep::sim::Geant4DetectorConstructionContext", "d9/dff/classdd4hep_1_1sim_1_1Geant4DetectorConstructionContext.html", null ],
    [ "dd4hep::sim::Geant4EventReader", "d5/d7f/classdd4hep_1_1sim_1_1Geant4EventReader.html", [
      [ "dd4hep::sim::Geant4EventReaderGuineaPig", "d7/dee/classdd4hep_1_1sim_1_1Geant4EventReaderGuineaPig.html", null ],
      [ "dd4hep::sim::Geant4EventReaderHepEvt", "df/d1c/classdd4hep_1_1sim_1_1Geant4EventReaderHepEvt.html", null ],
      [ "dd4hep::sim::Geant4EventReaderHepMC", "de/dc7/classdd4hep_1_1sim_1_1Geant4EventReaderHepMC.html", null ],
      [ "dd4hep::sim::HEPMC3EventReader", "d3/d03/classdd4hep_1_1sim_1_1HEPMC3EventReader.html", [
        [ "dd4hep::sim::HEPMC3FileReader", "d0/dbb/classdd4hep_1_1sim_1_1HEPMC3FileReader.html", null ]
      ] ],
      [ "dd4hep::sim::LCIOEventReader", "dc/df0/classdd4hep_1_1sim_1_1LCIOEventReader.html", [
        [ "dd4hep::sim::LCIOFileReader", "d5/d81/classdd4hep_1_1sim_1_1LCIOFileReader.html", null ],
        [ "dd4hep::sim::LCIOStdHepReader", "d0/dd2/classdd4hep_1_1sim_1_1LCIOStdHepReader.html", null ]
      ] ]
    ] ],
    [ "dd4hep::sim::Geant4Exec", "d9/dd9/classdd4hep_1_1sim_1_1Geant4Exec.html", null ],
    [ "dd4hep::sim::Geant4FieldTrackingSetup", "d1/d14/structdd4hep_1_1sim_1_1Geant4FieldTrackingSetup.html", [
      [ "dd4hep::sim::Geant4FieldTrackingConstruction", "d7/d6b/classdd4hep_1_1sim_1_1Geant4FieldTrackingConstruction.html", null ],
      [ "dd4hep::sim::Geant4FieldTrackingSetupAction", "d0/de3/classdd4hep_1_1sim_1_1Geant4FieldTrackingSetupAction.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4GenericSD< T >", "de/d07/classdd4hep_1_1sim_1_1Geant4GenericSD.html", null ],
    [ "dd4hep::sim::Geant4GFlashSpotHandler", "db/d65/classdd4hep_1_1sim_1_1Geant4GFlashSpotHandler.html", null ],
    [ "dd4hep::sim::Geant4Handle< TYPE >", "d2/da3/classdd4hep_1_1sim_1_1Geant4Handle.html", null ],
    [ "dd4hep::sim::Geant4HierarchyDump", "d9/d44/classdd4hep_1_1sim_1_1Geant4HierarchyDump.html", null ],
    [ "dd4hep::sim::Geant4HitData", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html", [
      [ "dd4hep::sim::Geant4Calorimeter::Hit", "d1/d37/classdd4hep_1_1sim_1_1Geant4Calorimeter_1_1Hit.html", null ],
      [ "dd4hep::sim::Geant4Tracker::Hit", "df/d20/classdd4hep_1_1sim_1_1Geant4Tracker_1_1Hit.html", [
        [ "SomeExperiment::MyTrackerHit", "d6/d56/classSomeExperiment_1_1MyTrackerHit.html", null ]
      ] ]
    ] ],
    [ "dd4hep::sim::Geant4MonteCarloTruth", "d8/d27/classdd4hep_1_1sim_1_1Geant4MonteCarloTruth.html", [
      [ "dd4hep::sim::Geant4DummyTruthHandler", "d8/d4d/classdd4hep_1_1sim_1_1Geant4DummyTruthHandler.html", null ],
      [ "dd4hep::sim::Geant4ParticleHandler", "d0/dac/classdd4hep_1_1sim_1_1Geant4ParticleHandler.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4OpticalCalorimeter", "d3/d41/structdd4hep_1_1sim_1_1Geant4OpticalCalorimeter.html", null ],
    [ "dd4hep::sim::Geant4Particle", "de/d0b/classdd4hep_1_1sim_1_1Geant4Particle.html", null ],
    [ "dd4hep::sim::Geant4ParticleHandle", "df/df2/classdd4hep_1_1sim_1_1Geant4ParticleHandle.html", null ],
    [ "dd4hep::sim::Geant4ParticleMap", "df/dec/classdd4hep_1_1sim_1_1Geant4ParticleMap.html", null ],
    [ "dd4hep::sim::Geant4PrimaryConfig", "de/d8a/classdd4hep_1_1sim_1_1Geant4PrimaryConfig.html", null ],
    [ "dd4hep::sim::Geant4PrimaryEvent", "dc/d8d/classdd4hep_1_1sim_1_1Geant4PrimaryEvent.html", null ],
    [ "dd4hep::sim::Geant4PrimaryInteraction", "d3/d97/classdd4hep_1_1sim_1_1Geant4PrimaryInteraction.html", null ],
    [ "dd4hep::sim::Geant4PrimaryMap", "dd/ddb/classdd4hep_1_1sim_1_1Geant4PrimaryMap.html", null ],
    [ "dd4hep::sim::Geant4PythonCall", "de/d45/classdd4hep_1_1sim_1_1Geant4PythonCall.html", null ],
    [ "dd4hep::sim::Geant4ScintillatorCalorimeter", "da/da8/structdd4hep_1_1sim_1_1Geant4ScintillatorCalorimeter.html", null ],
    [ "dd4hep::sim::Geant4SensDetSequences", "d9/de4/classdd4hep_1_1sim_1_1Geant4SensDetSequences.html", null ],
    [ "dd4hep::sim::Geant4StepHandler", "d6/d67/classdd4hep_1_1sim_1_1Geant4StepHandler.html", null ],
    [ "dd4hep::sim::Test::Geant4TestBase", "df/d6c/classdd4hep_1_1sim_1_1Test_1_1Geant4TestBase.html", [
      [ "dd4hep::sim::Test::Geant4TestEventAction", "d3/d58/classdd4hep_1_1sim_1_1Test_1_1Geant4TestEventAction.html", null ],
      [ "dd4hep::sim::Test::Geant4TestGeneratorAction", "d5/de1/classdd4hep_1_1sim_1_1Test_1_1Geant4TestGeneratorAction.html", null ],
      [ "dd4hep::sim::Test::Geant4TestRunAction", "d4/d3f/classdd4hep_1_1sim_1_1Test_1_1Geant4TestRunAction.html", null ],
      [ "dd4hep::sim::Test::Geant4TestSensitive", "da/d89/classdd4hep_1_1sim_1_1Test_1_1Geant4TestSensitive.html", null ],
      [ "dd4hep::sim::Test::Geant4TestStepAction", "da/d74/classdd4hep_1_1sim_1_1Test_1_1Geant4TestStepAction.html", null ],
      [ "dd4hep::sim::Test::Geant4TestTrackAction", "d7/d1b/classdd4hep_1_1sim_1_1Test_1_1Geant4TestTrackAction.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4TouchableHandler", "d9/d42/classdd4hep_1_1sim_1_1Geant4TouchableHandler.html", null ],
    [ "dd4hep::sim::Geant4Tracker", "d9/d06/classdd4hep_1_1sim_1_1Geant4Tracker.html", null ],
    [ "dd4hep::sim::Geant4TrackHandler", "da/da2/classdd4hep_1_1sim_1_1Geant4TrackHandler.html", null ],
    [ "dd4hep::sim::Geant4Vertex", "dd/d6b/classdd4hep_1_1sim_1_1Geant4Vertex.html", null ],
    [ "dd4hep::rec::GearHandle", "d4/d47/classdd4hep_1_1rec_1_1GearHandle.html", null ],
    [ "dd4hep::detail::GeoHandlerTypes", "d4/d49/classdd4hep_1_1detail_1_1GeoHandlerTypes.html", [
      [ "dd4hep::detail::GeoHandler", "d0/d4e/classdd4hep_1_1detail_1_1GeoHandler.html", [
        [ "dd4hep::cms::ShapeDump", "d1/da3/classdd4hep_1_1cms_1_1ShapeDump.html", null ],
        [ "dd4hep::detail::GeometryTreeDump", "dd/dc8/classdd4hep_1_1detail_1_1GeometryTreeDump.html", null ],
        [ "dd4hep::detail::LCDDConverter", "d4/ddb/classdd4hep_1_1detail_1_1LCDDConverter.html", null ],
        [ "dd4hep::detail::PandoraConverter", "d5/d18/structdd4hep_1_1detail_1_1PandoraConverter.html", null ],
        [ "dd4hep::sim::Geant4Converter", "df/d7c/classdd4hep_1_1sim_1_1Geant4Converter.html", null ]
      ] ],
      [ "dd4hep::sim::Geant4Mapping", "de/dbb/classdd4hep_1_1sim_1_1Geant4Mapping.html", [
        [ "dd4hep::sim::Geant4Converter", "df/d7c/classdd4hep_1_1sim_1_1Geant4Converter.html", null ]
      ] ]
    ] ],
    [ "dd4hep::detail::GeoHandlerTypes::GeometryInfo", "d8/d64/classdd4hep_1_1detail_1_1GeoHandlerTypes_1_1GeometryInfo.html", [
      [ "dd4hep::detail::LCDDConverter::GeometryInfo", "d1/d8e/classdd4hep_1_1detail_1_1LCDDConverter_1_1GeometryInfo.html", null ],
      [ "dd4hep::detail::PandoraConverter::GeometryInfo", "d0/d4a/structdd4hep_1_1detail_1_1PandoraConverter_1_1GeometryInfo.html", null ],
      [ "dd4hep::sim::Geant4GeometryInfo", "d6/d8e/classdd4hep_1_1sim_1_1Geant4GeometryInfo.html", null ]
    ] ],
    [ "dd4hep::detail::GeoScan", "d5/d9c/classdd4hep_1_1detail_1_1GeoScan.html", null ],
    [ "dd4hep::detail::get_2nd< C >", "d3/d55/structdd4hep_1_1detail_1_1get__2nd.html", null ],
    [ "dd4hep::DDPython::GILState", "d5/d93/structdd4hep_1_1DDPython_1_1GILState.html", null ],
    [ "dd4hep::align::GlobalAlignmentCache", "d1/db2/classdd4hep_1_1align_1_1GlobalAlignmentCache.html", null ],
    [ "dd4hep::align::GlobalAlignmentOperator", "d8/d0c/classdd4hep_1_1align_1_1GlobalAlignmentOperator.html", [
      [ "dd4hep::align::GlobalAlignmentActor< T >", "d3/d8f/classdd4hep_1_1align_1_1GlobalAlignmentActor.html", null ],
      [ "dd4hep::align::GlobalAlignmentSelector", "d7/dd6/classdd4hep_1_1align_1_1GlobalAlignmentSelector.html", null ]
    ] ],
    [ "dd4hep::align::GlobalAlignmentStack", "d7/d97/classdd4hep_1_1align_1_1GlobalAlignmentStack.html", null ],
    [ "dd4hep::align::GlobalAlignmentWriter", "d6/ddc/classdd4hep_1_1align_1_1GlobalAlignmentWriter.html", null ],
    [ "dd4hep::cond::GlobalTag", "dc/dce/classdd4hep_1_1cond_1_1GlobalTag.html", null ],
    [ "sp::qi::grammar", null, [
      [ "dd4hep::Parsers::StringGrammar< Iterator, dd4hep::Parsers::SkipperGrammar >", "dd/dad/structdd4hep_1_1Parsers_1_1StringGrammar.html", null ],
      [ "dd4hep::Parsers::StringGrammar< Iterator, dd4hep::Parsers::SkipperGrammar >", "dd/dad/structdd4hep_1_1Parsers_1_1StringGrammar.html", null ],
      [ "dd4hep::Parsers::BoolGrammar< Iterator, Skipper >", "d7/d51/structdd4hep_1_1Parsers_1_1BoolGrammar.html", null ],
      [ "dd4hep::Parsers::BoolGrammar< Iterator, Skipper >", "d7/d51/structdd4hep_1_1Parsers_1_1BoolGrammar.html", null ],
      [ "dd4hep::Parsers::CharGrammar< Iterator, Skipper >", "dc/ddc/structdd4hep_1_1Parsers_1_1CharGrammar.html", null ],
      [ "dd4hep::Parsers::CharGrammar< Iterator, Skipper >", "dc/ddc/structdd4hep_1_1Parsers_1_1CharGrammar.html", null ],
      [ "dd4hep::Parsers::IntGrammar< Iterator, RT, Skipper >", "d1/d06/structdd4hep_1_1Parsers_1_1IntGrammar.html", null ],
      [ "dd4hep::Parsers::IntGrammar< Iterator, RT, Skipper >", "d1/d06/structdd4hep_1_1Parsers_1_1IntGrammar.html", null ],
      [ "dd4hep::Parsers::KeyValueGrammar< Iterator, Skipper >", "d5/d4e/structdd4hep_1_1Parsers_1_1KeyValueGrammar.html", null ],
      [ "dd4hep::Parsers::KeyValueGrammar< Iterator, Skipper >", "d5/d4e/structdd4hep_1_1Parsers_1_1KeyValueGrammar.html", null ],
      [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >", "d5/def/structdd4hep_1_1Parsers_1_1MapGrammar.html", null ],
      [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >", "d5/def/structdd4hep_1_1Parsers_1_1MapGrammar.html", null ],
      [ "dd4hep::Parsers::PairGrammar< Iterator, PairT, Skipper >", "d4/ded/structdd4hep_1_1Parsers_1_1PairGrammar.html", null ],
      [ "dd4hep::Parsers::PairGrammar< Iterator, PairT, Skipper >", "d4/ded/structdd4hep_1_1Parsers_1_1PairGrammar.html", null ],
      [ "dd4hep::Parsers::Pnt3DGrammar< Iterator, PointT, Skipper >", "d6/d38/structdd4hep_1_1Parsers_1_1Pnt3DGrammar.html", null ],
      [ "dd4hep::Parsers::Pnt3DGrammar< Iterator, PointT, Skipper >", "d6/d38/structdd4hep_1_1Parsers_1_1Pnt3DGrammar.html", null ],
      [ "dd4hep::Parsers::Pnt4DGrammar< Iterator, PointT, Skipper >", "d9/d67/structdd4hep_1_1Parsers_1_1Pnt4DGrammar.html", null ],
      [ "dd4hep::Parsers::Pnt4DGrammar< Iterator, PointT, Skipper >", "d9/d67/structdd4hep_1_1Parsers_1_1Pnt4DGrammar.html", null ],
      [ "dd4hep::Parsers::RealGrammar< Iterator, RT, Skipper >", "d1/dd3/structdd4hep_1_1Parsers_1_1RealGrammar.html", null ],
      [ "dd4hep::Parsers::RealGrammar< Iterator, RT, Skipper >", "d1/dd3/structdd4hep_1_1Parsers_1_1RealGrammar.html", null ],
      [ "dd4hep::Parsers::Rot3DGrammar< Iterator, PointT, Skipper >", "d5/d66/structdd4hep_1_1Parsers_1_1Rot3DGrammar.html", null ],
      [ "dd4hep::Parsers::Rot3DGrammar< Iterator, PointT, Skipper >", "d5/d66/structdd4hep_1_1Parsers_1_1Rot3DGrammar.html", null ],
      [ "dd4hep::Parsers::SkipperGrammar< Iterator >", "d0/d52/structdd4hep_1_1Parsers_1_1SkipperGrammar.html", null ],
      [ "dd4hep::Parsers::SkipperGrammar< Iterator >", "d0/d52/structdd4hep_1_1Parsers_1_1SkipperGrammar.html", null ],
      [ "dd4hep::Parsers::StringGrammar< Iterator, Skipper >", "dd/dad/structdd4hep_1_1Parsers_1_1StringGrammar.html", null ],
      [ "dd4hep::Parsers::StringGrammar< Iterator, Skipper >", "dd/dad/structdd4hep_1_1Parsers_1_1StringGrammar.html", null ],
      [ "dd4hep::Parsers::VectorGrammar< Iterator, VectorT, Skipper >", "d6/d5c/structdd4hep_1_1Parsers_1_1VectorGrammar.html", null ],
      [ "dd4hep::Parsers::VectorGrammar< Iterator, VectorT, Skipper >", "d6/d5c/structdd4hep_1_1Parsers_1_1VectorGrammar.html", null ]
    ] ],
    [ "dd4hep::Parsers::Grammar_< Iterator, T, Skipper, Enable >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, ROOT::Math::DisplacementVector3D< T1, T2 >, Skipper >", "d9/df8/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01ROOT_1_1Math_1_1DisplacementVector3D_3_01b072aaa9f8de6468377027a390b71b8b.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, ROOT::Math::LorentzVector< T1 >, Skipper >", "d4/d8f/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01ROOT_1_1Math_1_1LorentzVector_3_01T1_01_4_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, ROOT::Math::PositionVector3D< T1, T2 >, Skipper >", "d1/d37/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01ROOT_1_1Math_1_1PositionVector3D_3_01T1_00_01T2_01_4_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, ROOT::Math::RotationZYX, Skipper >", "d3/d6c/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01ROOT_1_1Math_1_1RotationZYX_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, Scalar, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, ScalarT, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, std::list< InnerT, AllocatorT >, Skipper >", "d5/d67/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01std_1_1list_3_01InnerT_00_01AllocatorT_01_4_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, std::map< KeyT, ValueT, KeyCompareT, AllocatorT >, Skipper >", "da/d9b/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01std_1_1map_3_01KeyT_00_01ValueT_00_01KeyC20df92580ec075243f4b8abd16c93e7c.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, std::pair< KeyT, ValueT >, Skipper >", "d2/de5/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01std_1_1pair_3_01KeyT_00_01ValueT_01_4_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, std::set< InnerT, CompareT, AllocatorT >, Skipper >", "d4/ded/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01std_1_1set_3_01InnerT_00_01CompareT_00_01ac5cabb27cb03c7a9a83b296ec0c8d36.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, std::vector< InnerT, AllocatorT >, Skipper >", "d2/ddb/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01std_1_1vector_3_01InnerT_00_01AllocatorT_01_4_00_01Skipper_01_4.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, T, Skipper, typename boost::enable_if< boost::is_floating_point< T > >::type >", "d8/d3d/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01T_00_01Skipper_00_01typename_01boost_1_1e9ceab0d117d2d9a83f3cdf660cde78a3.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, T, Skipper, typename boost::enable_if< boost::is_integral< T > >::type >", "d5/d42/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01T_00_01Skipper_00_01typename_01boost_1_1e7f81310a317d75ffa3d56d5f50432e11.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, typenameMapT::key_type, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, typenameMapT::mapped_type, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, typenamePairT::first_type, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, typenamePairT::second_type, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::Parsers::Grammar_< Iterator, typenameVectorT::value_type, dd4hep::Parsers::SkipperGrammar >", "d1/d7c/structdd4hep_1_1Parsers_1_1Grammar__.html", null ],
    [ "dd4hep::GrammarRegistry", "da/d92/classdd4hep_1_1GrammarRegistry.html", null ],
    [ "dd4hep::Handle< T >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::CartesianGridXY", "de/d5c/classdd4hep_1_1CartesianGridXY.html", null ],
      [ "dd4hep::CartesianGridXYZ", "d1/d27/classdd4hep_1_1CartesianGridXYZ.html", null ],
      [ "dd4hep::CartesianGridXZ", "d4/de1/classdd4hep_1_1CartesianGridXZ.html", null ],
      [ "dd4hep::CartesianGridYZ", "d3/dee/classdd4hep_1_1CartesianGridYZ.html", null ],
      [ "dd4hep::CartesianStripX", "d5/dc7/classdd4hep_1_1CartesianStripX.html", null ],
      [ "dd4hep::CartesianStripY", "d7/d8a/classdd4hep_1_1CartesianStripY.html", null ],
      [ "dd4hep::CartesianStripZ", "d7/d6f/classdd4hep_1_1CartesianStripZ.html", null ],
      [ "dd4hep::GridPhiEta", "d1/d24/classdd4hep_1_1GridPhiEta.html", null ],
      [ "dd4hep::GridRPhiEta", "da/d3c/classdd4hep_1_1GridRPhiEta.html", null ],
      [ "dd4hep::MultiSegmentation", "db/d91/classdd4hep_1_1MultiSegmentation.html", null ],
      [ "dd4hep::NoSegmentation", "d7/d5b/classdd4hep_1_1NoSegmentation.html", null ],
      [ "dd4hep::PolarGridRPhi", "d0/d26/classdd4hep_1_1PolarGridRPhi.html", null ],
      [ "dd4hep::PolarGridRPhi2", "d7/d67/classdd4hep_1_1PolarGridRPhi2.html", null ],
      [ "dd4hep::Solid_type< T >", "da/d3f/classdd4hep_1_1Solid__type.html", null ],
      [ "dd4hep::WaferGridXY", "d0/d88/classdd4hep_1_1WaferGridXY.html", null ],
      [ "dd4hep::sim::Geant4VolumeManager", "df/d50/classdd4hep_1_1sim_1_1Geant4VolumeManager.html", null ]
    ] ],
    [ "dd4hep::Handle< ConditionsManagerObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::cond::ConditionsManager", "dd/dbb/classdd4hep_1_1cond_1_1ConditionsManager.html", null ]
    ] ],
    [ "dd4hep::Handle< ConstantObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Constant", "df/d9f/classdd4hep_1_1Constant.html", null ]
    ] ],
    [ "dd4hep::Handle< dd4hep::cond::ConditionsDataLoader >", "d7/de1/classdd4hep_1_1Handle.html", null ],
    [ "dd4hep::Handle< dd4hep::DetElementObject >", "d7/de1/classdd4hep_1_1Handle.html", null ],
    [ "dd4hep::Handle< dd4hep::NamedObject >", "d7/de1/classdd4hep_1_1Handle.html", null ],
    [ "dd4hep::Handle< dd4hep::SensitiveDetectorObject >", "d7/de1/classdd4hep_1_1Handle.html", null ],
    [ "dd4hep::Handle< detail::AlignmentObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Alignment", "d9/dc2/classdd4hep_1_1Alignment.html", null ],
      [ "dd4hep::AlignmentCondition", "d3/d93/classdd4hep_1_1AlignmentCondition.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::ConditionObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Condition", "d8/d7b/classdd4hep_1_1Condition.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeIOVObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeIOVElement", "dd/de8/classgaudi_1_1DeIOVElement.html", [
        [ "gaudi::DetectorElement< DeIOVElement >", "d4/d80/classgaudi_1_1DetectorElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeStaticElement", "d8/dca/classgaudi_1_1DeStaticElement.html", [
        [ "gaudi::DetectorStaticElement< DeStaticElement >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloGenericObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloGenericElement", "d7/d39/classgaudi_1_1DeVeloGenericElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloGenericStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloGenericStaticElement", "d6/d05/classgaudi_1_1DeVeloGenericStaticElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloElement", "d3/d34/classgaudi_1_1DeVeloElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloSensorObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloSensorElement", "d6/d6e/classgaudi_1_1DeVeloSensorElement.html", [
        [ "gaudi::DeVeloPhiSensor", "d9/d56/classgaudi_1_1DeVeloPhiSensor.html", null ],
        [ "gaudi::DeVeloRSensor", "dc/d1b/classgaudi_1_1DeVeloRSensor.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloSensorStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloSensorStaticElement", "df/dae/classgaudi_1_1DeVeloSensorStaticElement.html", [
        [ "gaudi::DetectorStaticElement< DeVeloSensorStaticElement >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeVeloStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVeloStaticElement", "de/d65/classgaudi_1_1DeVeloStaticElement.html", [
        [ "gaudi::DetectorStaticElement< DeVeloStaticElement >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPGenericObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPGenericElement", "de/de8/classgaudi_1_1DeVPGenericElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPGenericStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPGenericStaticElement", "d0/d2b/classgaudi_1_1DeVPGenericStaticElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPElement", "d2/d89/classgaudi_1_1DeVPElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPSensorObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPSensorElement", "d4/d90/classgaudi_1_1DeVPSensorElement.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPSensorStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPSensorStaticElement", "d3/d02/classgaudi_1_1DeVPSensorStaticElement.html", [
        [ "gaudi::DetectorStaticElement< DeVPSensorStaticElement >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::DeVPStaticObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "gaudi::DeVPStaticElement", "d0/d01/classgaudi_1_1DeVPStaticElement.html", [
        [ "gaudi::DetectorStaticElement< DeVPStaticElement >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< detail::OpticalSurfaceManagerObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::OpticalSurfaceManager", "de/d2a/classdd4hep_1_1OpticalSurfaceManager.html", null ]
    ] ],
    [ "dd4hep::Handle< detail::VolumeManagerObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::VolumeManager", "d6/d7f/classdd4hep_1_1VolumeManager.html", null ]
    ] ],
    [ "dd4hep::Handle< Detector >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::DetectorHelper", "d0/d7b/classdd4hep_1_1DetectorHelper.html", null ]
    ] ],
    [ "dd4hep::Handle< DetElementObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::DetElement", "d5/d89/classdd4hep_1_1DetElement.html", [
        [ "dd4hep::align::GlobalDetectorAlignment", "d3/d6c/classdd4hep_1_1align_1_1GlobalDetectorAlignment.html", null ],
        [ "dd4hep::rec::DetectorSurfaces", "d1/d99/classdd4hep_1_1rec_1_1DetectorSurfaces.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< Geant4GeometryInfo >", "d7/de1/classdd4hep_1_1Handle.html", null ],
    [ "dd4hep::Handle< HeaderObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Header", "d8/dcc/classdd4hep_1_1Header.html", null ]
    ] ],
    [ "dd4hep::Handle< IDDescriptorObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::IDDescriptor", "d2/db8/classdd4hep_1_1IDDescriptor.html", null ]
    ] ],
    [ "dd4hep::Handle< LimitSetObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::LimitSet", "df/dcb/classdd4hep_1_1LimitSet.html", null ]
    ] ],
    [ "dd4hep::Handle< NamedObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Author", "d7/d6a/classdd4hep_1_1Author.html", null ],
      [ "dd4hep::CartesianField", "da/da9/classdd4hep_1_1CartesianField.html", null ],
      [ "dd4hep::OverlayedField", "d5/ded/classdd4hep_1_1OverlayedField.html", null ]
    ] ],
    [ "dd4hep::Handle< ReadoutObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Readout", "dd/dae/classdd4hep_1_1Readout.html", null ]
    ] ],
    [ "dd4hep::Handle< RegionObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Region", "d2/df7/classdd4hep_1_1Region.html", null ]
    ] ],
    [ "dd4hep::Handle< SegmentationObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Segmentation", "d0/d6a/classdd4hep_1_1Segmentation.html", null ]
    ] ],
    [ "dd4hep::Handle< SensitiveDetectorObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::SensitiveDetector", "d4/d09/classdd4hep_1_1SensitiveDetector.html", null ]
    ] ],
    [ "dd4hep::Handle< TGDMLMatrix >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::PropertyTable", "d0/d96/classdd4hep_1_1PropertyTable.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoArb8 >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoArb8 >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::EightPointSolid", "d5/dab/classdd4hep_1_1EightPointSolid.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoBBox >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoBBox >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Box", "df/de8/classdd4hep_1_1Box.html", null ],
        [ "dd4hep::cms::DDBox", "d3/d3e/classdd4hep_1_1cms_1_1DDBox.html", null ],
        [ "dd4hep::cms::DDBox", "d3/d3e/classdd4hep_1_1cms_1_1DDBox.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoBorderSurface >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::BorderSurface", "de/db4/classdd4hep_1_1BorderSurface.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoCompositeShape >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoCompositeShape >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::BooleanSolid", "de/d38/classdd4hep_1_1BooleanSolid.html", [
          [ "dd4hep::IntersectionSolid", "dd/d45/classdd4hep_1_1IntersectionSolid.html", null ],
          [ "dd4hep::SubtractionSolid", "d2/d95/classdd4hep_1_1SubtractionSolid.html", null ],
          [ "dd4hep::UnionSolid", "d3/d96/classdd4hep_1_1UnionSolid.html", null ]
        ] ],
        [ "dd4hep::PseudoTrap", "d8/d36/classdd4hep_1_1PseudoTrap.html", null ],
        [ "dd4hep::TruncatedTube", "d9/df6/classdd4hep_1_1TruncatedTube.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoCone >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoCone >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Cone", "db/d37/classdd4hep_1_1Cone.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoConeSeg >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoConeSeg >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::ConeSegment", "d3/d4d/classdd4hep_1_1ConeSegment.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoCtub >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoCtub >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::CutTube", "d9/de6/classdd4hep_1_1CutTube.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoElement >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Atom", "d5/d28/classdd4hep_1_1Atom.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoEltu >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoEltu >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::EllipticalTube", "dc/d2d/classdd4hep_1_1EllipticalTube.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoHalfSpace >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoHalfSpace >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::HalfSpace", "db/d16/classdd4hep_1_1HalfSpace.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoHype >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoHype >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Hyperboloid", "d2/da0/classdd4hep_1_1Hyperboloid.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoMedium >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Material", "d6/dbd/classdd4hep_1_1Material.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoNode >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::PlacedVolume", "d8/d80/classdd4hep_1_1PlacedVolume.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoOpticalSurface >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::OpticalSurface", "d2/d6f/classdd4hep_1_1OpticalSurface.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoParaboloid >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoParaboloid >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Paraboloid", "d8/dd7/classdd4hep_1_1Paraboloid.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoPcon >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoPcon >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Polycone", "dc/d23/classdd4hep_1_1Polycone.html", null ],
        [ "dd4hep::cms::DDPolycone", "d3/d4e/classdd4hep_1_1cms_1_1DDPolycone.html", null ],
        [ "dd4hep::cms::DDPolycone", "d3/d4e/classdd4hep_1_1cms_1_1DDPolycone.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoPgon >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoPgon >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Polyhedra", "db/dbc/classdd4hep_1_1Polyhedra.html", null ],
        [ "dd4hep::PolyhedraRegular", "dd/da2/classdd4hep_1_1PolyhedraRegular.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoPhysicalNode >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::align::GlobalAlignment", "d9/d5a/classdd4hep_1_1align_1_1GlobalAlignment.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoScaledShape >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoScaledShape >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Scale", "d1/db3/classdd4hep_1_1Scale.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoShape >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoShape >", "da/d3f/classdd4hep_1_1Solid__type.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoShapeAssembly >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoShapeAssembly >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::ShapelessSolid", "d7/dc7/classdd4hep_1_1ShapelessSolid.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoSkinSurface >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::SkinSurface", "d6/d0c/classdd4hep_1_1SkinSurface.html", null ]
    ] ],
    [ "dd4hep::Handle< TGeoSphere >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoSphere >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Sphere", "d9/d29/classdd4hep_1_1Sphere.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoTorus >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoTorus >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Torus", "d4/d77/classdd4hep_1_1Torus.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoTrap >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoTrap >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Trap", "de/d16/classdd4hep_1_1Trap.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoTrd1 >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoTrd1 >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Trd1", "d1/d7d/classdd4hep_1_1Trd1.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoTrd2 >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoTrd2 >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Trd2", "df/d4d/classdd4hep_1_1Trd2.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoTubeSeg >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoTubeSeg >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::Tube", "d1/d40/classdd4hep_1_1Tube.html", null ],
        [ "dd4hep::TwistedTube", "dd/deb/classdd4hep_1_1TwistedTube.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoVolume >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Volume", "d2/dca/classdd4hep_1_1Volume.html", [
        [ "dd4hep::Assembly", "d4/df1/classdd4hep_1_1Assembly.html", null ],
        [ "dd4hep::VolumeMulti", "de/dda/classdd4hep_1_1VolumeMulti.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< TGeoXtru >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::Solid_type< TGeoXtru >", "da/d3f/classdd4hep_1_1Solid__type.html", [
        [ "dd4hep::ExtrudedPolygon", "de/d0f/classdd4hep_1_1ExtrudedPolygon.html", null ]
      ] ]
    ] ],
    [ "dd4hep::Handle< VisAttrObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::VisAttr", "d9/d1a/classdd4hep_1_1VisAttr.html", null ]
    ] ],
    [ "dd4hep::Handle< WorldObject >", "d7/de1/classdd4hep_1_1Handle.html", [
      [ "dd4hep::World", "d8/d2f/classdd4hep_1_1World.html", null ]
    ] ],
    [ "dd4hep::json::Handle_t", "d9/dcb/classdd4hep_1_1json_1_1Handle__t.html", [
      [ "dd4hep::json::Collection_t", "d6/d85/classdd4hep_1_1json_1_1Collection__t.html", null ]
    ] ],
    [ "dd4hep::xml::Handle_t", "d8/dc2/classdd4hep_1_1xml_1_1Handle__t.html", [
      [ "dd4hep::xml::Collection_t", "de/d4b/classdd4hep_1_1xml_1_1Collection__t.html", null ]
    ] ],
    [ "Detector::HandleMap", null, [
      [ "dd4hep::DetectorData::ObjectHandleMap", "d1/dc7/classdd4hep_1_1DetectorData_1_1ObjectHandleMap.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4UserLimits::Handler", "d6/db5/structdd4hep_1_1sim_1_1Geant4UserLimits_1_1Handler.html", null ],
    [ "dd4hep::ConditionKey::HashCompare", "d6/d92/structdd4hep_1_1ConditionKey_1_1HashCompare.html", null ],
    [ "dd4hep::HitCollection", "df/d80/classdd4hep_1_1HitCollection.html", null ],
    [ "dd4hep::sim::HitCompare< HIT >", "de/dfa/classdd4hep_1_1sim_1_1HitCompare.html", [
      [ "dd4hep::sim::HitPositionCompare< HIT >", "dc/dbf/structdd4hep_1_1sim_1_1HitPositionCompare.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4HitWrapper::HitManipulator", "d5/dd9/classdd4hep_1_1sim_1_1Geant4HitWrapper_1_1HitManipulator.html", null ],
    [ "dd4hep::rec::ICone", "dd/ddd/classdd4hep_1_1rec_1_1ICone.html", [
      [ "dd4hep::rec::ConeSurface", "dd/df5/classdd4hep_1_1rec_1_1ConeSurface.html", null ]
    ] ],
    [ "dd4hep::rec::ICylinder", "d3/d8f/classdd4hep_1_1rec_1_1ICylinder.html", [
      [ "dd4hep::rec::CylinderSurface", "d0/db0/classdd4hep_1_1rec_1_1CylinderSurface.html", [
        [ "dd4hep::rec::ConeSurface", "dd/df5/classdd4hep_1_1rec_1_1ConeSurface.html", null ]
      ] ]
    ] ],
    [ "gaudi::IDetService", "d3/d02/classgaudi_1_1IDetService.html", [
      [ "gaudi::DetService", "d1/dfa/classgaudi_1_1DetService.html", null ]
    ] ],
    [ "dd4hep::rec::IMaterial", "d8/dc5/classdd4hep_1_1rec_1_1IMaterial.html", [
      [ "dd4hep::rec::MaterialData", "d7/d96/classdd4hep_1_1rec_1_1MaterialData.html", null ]
    ] ],
    [ "dd4hep::DDDB::Increment< T >", "d4/d84/classdd4hep_1_1DDDB_1_1Increment.html", null ],
    [ "dd4hep::Increment< T >", "d5/d79/structdd4hep_1_1Increment.html", null ],
    [ "boost::iostreams::input_seekable", null, [
      [ "dd4hep::dd4hep_file_source< T >::category", "d8/d7a/structdd4hep_1_1dd4hep__file__source_1_1category.html", null ]
    ] ],
    [ "dd4hep::cad::InputReader", "d6/db7/classdd4hep_1_1cad_1_1InputReader.html", [
      [ "dd4hep::cad::ASSIMPReader", "da/d0f/classdd4hep_1_1cad_1_1ASSIMPReader.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsSlice::Inserter", "de/dd0/classdd4hep_1_1cond_1_1ConditionsSlice_1_1Inserter.html", null ],
    [ "dd4hep::InstanceCount", "dc/d4e/structdd4hep_1_1InstanceCount.html", null ],
    [ "dd4hep::digi::DigiKernel::Internals", "d3/d85/classDigiKernel_1_1Internals.html", null ],
    [ "dd4hep::detail::interp", "d7/da7/structdd4hep_1_1detail_1_1interp.html", null ],
    [ "dd4hep::sim::Geant4HitWrapper::InvalidHit", "d2/dba/classdd4hep_1_1sim_1_1Geant4HitWrapper_1_1InvalidHit.html", null ],
    [ "dd4hep::IOV", "d8/da2/classdd4hep_1_1IOV.html", null ],
    [ "dd4hep::IOVType", "da/df8/classdd4hep_1_1IOVType.html", null ],
    [ "dd4hep::rec::ISurface", "d4/d5f/classdd4hep_1_1rec_1_1ISurface.html", [
      [ "dd4hep::rec::Surface", "d6/d86/classdd4hep_1_1rec_1_1Surface.html", [
        [ "dd4hep::rec::CylinderSurface", "d0/db0/classdd4hep_1_1rec_1_1CylinderSurface.html", null ]
      ] ],
      [ "dd4hep::rec::VolSurface", "d4/d6f/classdd4hep_1_1rec_1_1VolSurface.html", [
        [ "SimpleCylinder", "d0/d7d/classSimpleCylinder.html", null ],
        [ "SimpleCylinder", "d0/d7d/classSimpleCylinder.html", null ],
        [ "SimpleCylinder", "d0/d7d/classSimpleCylinder.html", null ],
        [ "dd4hep::rec::VolCone", "d0/d3f/classdd4hep_1_1rec_1_1VolCone.html", null ],
        [ "dd4hep::rec::VolCylinder", "dc/d6e/classdd4hep_1_1rec_1_1VolCylinder.html", null ],
        [ "dd4hep::rec::VolSurfaceHandle< T >", "d7/d55/classdd4hep_1_1rec_1_1VolSurfaceHandle.html", null ]
      ] ],
      [ "dd4hep::rec::VolSurfaceBase", "d2/d8d/classdd4hep_1_1rec_1_1VolSurfaceBase.html", [
        [ "dd4hep::rec::VolConeImpl", "d4/da3/classdd4hep_1_1rec_1_1VolConeImpl.html", null ],
        [ "dd4hep::rec::VolCylinderImpl", "d2/d1b/classdd4hep_1_1rec_1_1VolCylinderImpl.html", [
          [ "SimpleCylinderImpl", "dd/dc7/classSimpleCylinderImpl.html", null ],
          [ "SimpleCylinderImpl", "dd/dc7/classSimpleCylinderImpl.html", null ],
          [ "SimpleCylinderImpl", "dd/dc7/classSimpleCylinderImpl.html", null ]
        ] ],
        [ "dd4hep::rec::VolPlaneImpl", "d2/d92/classdd4hep_1_1rec_1_1VolPlaneImpl.html", null ]
      ] ]
    ] ],
    [ "dd4hep::digi::KernelHandle", "d4/dd5/classdd4hep_1_1digi_1_1KernelHandle.html", null ],
    [ "dd4hep::sim::KernelHandle", "df/dc8/classdd4hep_1_1sim_1_1KernelHandle.html", null ],
    [ "dd4hep::digi::Key", "d6/d4a/uniondd4hep_1_1digi_1_1Key.html", null ],
    [ "dd4hep::ConditionKey::KeyMaker", "da/d51/uniondd4hep_1_1ConditionKey_1_1KeyMaker.html", null ],
    [ "dd4hep::align::Keys", "df/d1e/classdd4hep_1_1align_1_1Keys.html", null ],
    [ "gaudi::Keys", "d3/d5f/structgaudi_1_1Keys.html", null ],
    [ "dd4hep::Layer", "df/d05/classdd4hep_1_1Layer.html", null ],
    [ "dd4hep::rec::LayeredCalorimeterStruct::Layer", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html", null ],
    [ "dd4hep::DDSegmentation::TiledLayerSegmentation::LayerDimensions", "d5/ddd/structdd4hep_1_1DDSegmentation_1_1TiledLayerSegmentation_1_1LayerDimensions.html", null ],
    [ "dd4hep::rec::LayeredCalorimeterStruct", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html", null ],
    [ "dd4hep::Layering", "d3/d5c/classdd4hep_1_1Layering.html", null ],
    [ "dd4hep::rec::ZDiskPetalsStruct::LayerLayout", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html", null ],
    [ "dd4hep::rec::ZPlanarStruct::LayerLayout", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html", null ],
    [ "dd4hep::LayerSlice", "d0/d09/classdd4hep_1_1LayerSlice.html", null ],
    [ "dd4hep::LayerStack", "dd/dd0/classdd4hep_1_1LayerStack.html", null ],
    [ "Tests::LcioTestTracker", "d2/d54/classTests_1_1LcioTestTracker.html", null ],
    [ "LHeD.LHeD", "d5/d59/classLHeD_1_1LHeD.html", null ],
    [ "dd4hep::Limit", "d2/d14/classdd4hep_1_1Limit.html", null ],
    [ "std::list", null, [
      [ "dd4hep::rec::SurfaceList", "d2/d29/classdd4hep_1_1rec_1_1SurfaceList.html", null ],
      [ "dd4hep::rec::VolSurfaceList", "d9/d30/structdd4hep_1_1rec_1_1VolSurfaceList.html", null ]
    ] ],
    [ "dd4hep::detail::ListBinder", "da/d0e/structdd4hep_1_1detail_1_1ListBinder.html", null ],
    [ "Gaudi::PluginService::v1::Details::Logger", "d0/dc4/classGaudi_1_1PluginService_1_1v1_1_1Details_1_1Logger.html", null ],
    [ "dd4hep::detail::MapBinder", "dc/dbe/structdd4hep_1_1detail_1_1MapBinder.html", null ],
    [ "dd4hep::rec::MapStringDoubleStruct", "d3/d78/structdd4hep_1_1rec_1_1MapStringDoubleStruct.html", null ],
    [ "dd4hep::rec::MaterialManager", "d2/d78/classdd4hep_1_1rec_1_1MaterialManager.html", null ],
    [ "dd4hep::rec::MaterialScan", "d2/d21/classdd4hep_1_1rec_1_1MaterialScan.html", null ],
    [ "dd4hep::Callback::mfunc_t", "d5/dbb/structdd4hep_1_1Callback_1_1mfunc__t.html", null ],
    [ "types.ModuleType", null, [
      [ "ddsix.Module_six_moves_urllib", "da/d27/classddsix_1_1Module__six__moves__urllib.html", null ],
      [ "ddsix._LazyModule", "d1/dfe/classddsix_1_1__LazyModule.html", [
        [ "ddsix.Module_six_moves_urllib_error", "da/d1e/classddsix_1_1Module__six__moves__urllib__error.html", null ],
        [ "ddsix.Module_six_moves_urllib_parse", "dd/d81/classddsix_1_1Module__six__moves__urllib__parse.html", null ],
        [ "ddsix.Module_six_moves_urllib_request", "df/dae/classddsix_1_1Module__six__moves__urllib__request.html", null ],
        [ "ddsix.Module_six_moves_urllib_response", "d1/d43/classddsix_1_1Module__six__moves__urllib__response.html", null ],
        [ "ddsix.Module_six_moves_urllib_robotparser", "d9/db9/classddsix_1_1Module__six__moves__urllib__robotparser.html", null ],
        [ "ddsix._MovedItems", "d3/d82/classddsix_1_1__MovedItems.html", null ]
      ] ]
    ] ],
    [ "dd4hep::sim::Geant4Hit::MonteCarloContrib", "dc/da1/structdd4hep_1_1sim_1_1Geant4Hit_1_1MonteCarloContrib.html", null ],
    [ "dd4hep::sim::Geant4HitData::MonteCarloContrib", "d0/db7/classdd4hep_1_1sim_1_1Geant4HitData_1_1MonteCarloContrib.html", null ],
    [ "MultiView", "df/d6a/classMultiView.html", null ],
    [ "SomeExperiment::MyTrackerSD", "d6/d8d/classSomeExperiment_1_1MyTrackerSD.html", null ],
    [ "dd4hep::NamedObject", "da/d74/classdd4hep_1_1NamedObject.html", [
      [ "dd4hep::CartesianField::Object", "d1/d07/classdd4hep_1_1CartesianField_1_1Object.html", [
        [ "dd4hep::ConstantField", "d0/d20/classdd4hep_1_1ConstantField.html", null ],
        [ "dd4hep::DipoleField", "d8/d58/classdd4hep_1_1DipoleField.html", null ],
        [ "dd4hep::MultipoleField", "db/d8c/classdd4hep_1_1MultipoleField.html", null ],
        [ "dd4hep::SolenoidField", "dd/d4f/classdd4hep_1_1SolenoidField.html", null ]
      ] ],
      [ "dd4hep::ConstantObject", "df/d8f/classdd4hep_1_1ConstantObject.html", null ],
      [ "dd4hep::DetElementObject", "db/dde/classdd4hep_1_1DetElementObject.html", [
        [ "dd4hep::WorldObject", "dd/d9f/classdd4hep_1_1WorldObject.html", null ]
      ] ],
      [ "dd4hep::HeaderObject", "db/de3/classdd4hep_1_1HeaderObject.html", null ],
      [ "dd4hep::IDDescriptorObject", "db/d40/classdd4hep_1_1IDDescriptorObject.html", null ],
      [ "dd4hep::LimitSetObject", "dd/dd2/classdd4hep_1_1LimitSetObject.html", null ],
      [ "dd4hep::OverlayedField::Object", "df/de5/classdd4hep_1_1OverlayedField_1_1Object.html", null ],
      [ "dd4hep::ReadoutObject", "dd/dfc/classdd4hep_1_1ReadoutObject.html", null ],
      [ "dd4hep::RegionObject", "db/d02/classdd4hep_1_1RegionObject.html", null ],
      [ "dd4hep::SensitiveDetectorObject", "d6/d96/classdd4hep_1_1SensitiveDetectorObject.html", null ],
      [ "dd4hep::VisAttrObject", "d4/db8/classdd4hep_1_1VisAttrObject.html", null ],
      [ "dd4hep::align::GlobalAlignmentData", "da/d58/classdd4hep_1_1align_1_1GlobalAlignmentData.html", null ],
      [ "dd4hep::cond::ConditionsDataLoader", "d5/d82/classdd4hep_1_1cond_1_1ConditionsDataLoader.html", [
        [ "dd4hep::DDDB::DDDBConditionsLoader", "d4/da7/classdd4hep_1_1DDDB_1_1DDDBConditionsLoader.html", null ],
        [ "dd4hep::cond::ConditionsMultiLoader", "dd/d4c/classdd4hep_1_1cond_1_1ConditionsMultiLoader.html", null ],
        [ "dd4hep::cond::ConditionsSnapshotRootLoader", "dc/d6c/classdd4hep_1_1cond_1_1ConditionsSnapshotRootLoader.html", null ],
        [ "dd4hep::cond::ConditionsXmlLoader", "de/dfa/classdd4hep_1_1cond_1_1ConditionsXmlLoader.html", null ]
      ] ],
      [ "dd4hep::cond::ConditionsManagerObject", "d9/db2/classdd4hep_1_1cond_1_1ConditionsManagerObject.html", [
        [ "dd4hep::cond::Manager_Type1", "d9/da7/classdd4hep_1_1cond_1_1Manager__Type1.html", null ]
      ] ],
      [ "dd4hep::cond::ConditionsPool", "d6/d61/classdd4hep_1_1cond_1_1ConditionsPool.html", [
        [ "dd4hep::cond::UpdatePool", "d3/d2b/classdd4hep_1_1cond_1_1UpdatePool.html", null ]
      ] ],
      [ "dd4hep::cond::Entry", "d6/daa/classdd4hep_1_1cond_1_1Entry.html", null ],
      [ "dd4hep::detail::OpticalSurfaceManagerObject", "df/dd6/classdd4hep_1_1detail_1_1OpticalSurfaceManagerObject.html", null ],
      [ "dd4hep::detail::VolumeManagerObject", "d0/d42/classdd4hep_1_1detail_1_1VolumeManagerObject.html", null ]
    ] ],
    [ "dd4hep::cms::Namespace", "da/d33/classdd4hep_1_1cms_1_1Namespace.html", null ],
    [ "dd4hep::rec::NeighbourSurfacesStruct", "d9/d43/structdd4hep_1_1rec_1_1NeighbourSurfacesStruct.html", null ],
    [ "dd4hep::json::NodeList", "db/d1a/classdd4hep_1_1json_1_1NodeList.html", null ],
    [ "dd4hep::xml::NodeList", "d0/d9f/classdd4hep_1_1xml_1_1NodeList.html", null ],
    [ "dd4hep::ConditionExamples::NonDefaultCtorCond", "dd/d74/classdd4hep_1_1ConditionExamples_1_1NonDefaultCtorCond.html", null ],
    [ "dd4hep::tools::Evaluator::Object", "db/db2/classdd4hep_1_1tools_1_1Evaluator_1_1Object.html", null ],
    [ "object", null, [
      [ "DDSim.DD4hepSimulation.DD4hepSimulation", "dc/ded/classDDSim_1_1DD4hepSimulation_1_1DD4hepSimulation.html", null ],
      [ "DDSim.Helper.ConfigHelper.ConfigHelper", "d9/de8/classDDSim_1_1Helper_1_1ConfigHelper_1_1ConfigHelper.html", [
        [ "DDSim.Helper.Action.Action", "d8/da3/classDDSim_1_1Helper_1_1Action_1_1Action.html", null ],
        [ "DDSim.Helper.Filter.Filter", "dd/d83/classDDSim_1_1Helper_1_1Filter_1_1Filter.html", null ],
        [ "DDSim.Helper.Gun.Gun", "dd/d1a/classDDSim_1_1Helper_1_1Gun_1_1Gun.html", null ],
        [ "DDSim.Helper.Input.Input", "d2/d76/classDDSim_1_1Helper_1_1Input_1_1Input.html", [
          [ "DDSim.Helper.GuineaPig.GuineaPig", "dd/d08/classDDSim_1_1Helper_1_1GuineaPig_1_1GuineaPig.html", null ],
          [ "DDSim.Helper.HepMC3.HepMC3", "d8/d72/classDDSim_1_1Helper_1_1HepMC3_1_1HepMC3.html", null ],
          [ "DDSim.Helper.LCIO.LCIO", "d0/d67/classDDSim_1_1Helper_1_1LCIO_1_1LCIO.html", null ]
        ] ],
        [ "DDSim.Helper.MagneticField.MagneticField", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html", null ],
        [ "DDSim.Helper.Meta.Meta", "d8/df3/classDDSim_1_1Helper_1_1Meta_1_1Meta.html", null ],
        [ "DDSim.Helper.Output.Output", "d0/d9c/classDDSim_1_1Helper_1_1Output_1_1Output.html", null ],
        [ "DDSim.Helper.OutputConfig.OutputConfig", "d8/d2f/classDDSim_1_1Helper_1_1OutputConfig_1_1OutputConfig.html", null ],
        [ "DDSim.Helper.ParticleHandler.ParticleHandler", "d8/d35/classDDSim_1_1Helper_1_1ParticleHandler_1_1ParticleHandler.html", null ],
        [ "DDSim.Helper.Physics.Physics", "d0/d09/classDDSim_1_1Helper_1_1Physics_1_1Physics.html", null ],
        [ "DDSim.Helper.Random.Random", "d7/df8/classDDSim_1_1Helper_1_1Random_1_1Random.html", null ]
      ] ],
      [ "ddsix.Iterator", "db/d92/classddsix_1_1Iterator.html", null ],
      [ "ddsix.X", "d2/d24/classddsix_1_1X.html", null ],
      [ "ddsix._LazyDescr", "da/d9a/classddsix_1_1__LazyDescr.html", [
        [ "ddsix.MovedAttribute", "dd/de5/classddsix_1_1MovedAttribute.html", null ],
        [ "ddsix.MovedModule", "d1/d92/classddsix_1_1MovedModule.html", null ]
      ] ],
      [ "ddsix._SixMetaPathImporter", "de/d65/classddsix_1_1__SixMetaPathImporter.html", null ]
    ] ],
    [ "dd4hep::ObjectExtensions", "d3/d33/classdd4hep_1_1ObjectExtensions.html", [
      [ "dd4hep::DetElementObject", "db/dde/classdd4hep_1_1DetElementObject.html", null ],
      [ "dd4hep::SensitiveDetectorObject", "d6/d96/classdd4hep_1_1SensitiveDetectorObject.html", null ],
      [ "dd4hep::cond::Manager_Type1", "d9/da7/classdd4hep_1_1cond_1_1Manager__Type1.html", null ],
      [ "dd4hep::digi::DigiEvent", "d8/de1/classdd4hep_1_1digi_1_1DigiEvent.html", null ],
      [ "dd4hep::sim::Geant4Event", "d9/dec/classdd4hep_1_1sim_1_1Geant4Event.html", null ],
      [ "dd4hep::sim::Geant4Run", "d0/d1b/classdd4hep_1_1sim_1_1Geant4Run.html", null ]
    ] ],
    [ "dd4hep::OpaqueData", "d9/d1f/classdd4hep_1_1OpaqueData.html", [
      [ "dd4hep::OpaqueDataBlock", "d4/d12/classdd4hep_1_1OpaqueDataBlock.html", null ]
    ] ],
    [ "dd4hep::detail::OpaqueDataBinder", "da/da5/classdd4hep_1_1detail_1_1OpaqueDataBinder.html", null ],
    [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >::Operations", "d5/dca/structdd4hep_1_1Parsers_1_1MapGrammar_1_1Operations.html", null ],
    [ "dd4hep::Parsers::Pnt3DGrammar< Iterator, PointT, Skipper >::Operations", "d3/d73/structdd4hep_1_1Parsers_1_1Pnt3DGrammar_1_1Operations.html", null ],
    [ "dd4hep::Parsers::Pnt4DGrammar< Iterator, PointT, Skipper >::Operations", "d7/dbf/structdd4hep_1_1Parsers_1_1Pnt4DGrammar_1_1Operations.html", null ],
    [ "dd4hep::Parsers::Rot3DGrammar< Iterator, PointT, Skipper >::Operations", "dd/dfb/structdd4hep_1_1Parsers_1_1Rot3DGrammar_1_1Operations.html", null ],
    [ "dd4hep::cond::Operators", "d7/dbe/classdd4hep_1_1cond_1_1Operators.html", null ],
    [ "boost::iostreams::output_seekable", null, [
      [ "dd4hep::dd4hep_file_sink< T >::category", "d9/df3/structdd4hep_1_1dd4hep__file__sink_1_1category.html", null ]
    ] ],
    [ "dd4hep::sim::Geant4OutputAction::OutputContext< T >", "da/d6c/classdd4hep_1_1sim_1_1Geant4OutputAction_1_1OutputContext.html", null ],
    [ "dd4hep::ConditionExamples::OutputLevel", "d7/d82/classdd4hep_1_1ConditionExamples_1_1OutputLevel.html", [
      [ "dd4hep::ConditionExamples::ConditionNonDefaultCtorUpdate1", "de/d8c/classdd4hep_1_1ConditionExamples_1_1ConditionNonDefaultCtorUpdate1.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate1", "db/d4b/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate1.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate2", "df/d82/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate2.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate3", "d5/d36/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate3.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate4", "dc/da6/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate4.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate5", "d8/dce/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate5.html", null ],
      [ "dd4hep::ConditionExamples::ConditionUpdate6", "dd/d16/classdd4hep_1_1ConditionExamples_1_1ConditionUpdate6.html", null ],
      [ "dd4hep::ConditionExamples::ConditionsCreator", "d3/d29/structdd4hep_1_1ConditionExamples_1_1ConditionsCreator.html", null ],
      [ "dd4hep::ConditionExamples::ConditionsDataAccess", "de/d4b/structdd4hep_1_1ConditionExamples_1_1ConditionsDataAccess.html", null ],
      [ "dd4hep::ConditionExamples::ConditionsDependencyCreator", "d6/d8e/structdd4hep_1_1ConditionExamples_1_1ConditionsDependencyCreator.html", null ],
      [ "dd4hep::ConditionExamples::ConditionsKeys", "d9/d38/classdd4hep_1_1ConditionExamples_1_1ConditionsKeys.html", null ]
    ] ],
    [ "dd4hep::cad::OutputWriter", "d0/d96/classdd4hep_1_1cad_1_1OutputWriter.html", [
      [ "dd4hep::cad::ASSIMPWriter", "d2/d57/classdd4hep_1_1cad_1_1ASSIMPWriter.html", null ]
    ] ],
    [ "std::pair", null, [
      [ "dd4hep::digi::TypeName", "d3/db6/classdd4hep_1_1digi_1_1TypeName.html", null ],
      [ "dd4hep::sim::TypeName", "d3/d8e/classdd4hep_1_1sim_1_1TypeName.html", null ]
    ] ],
    [ "gaudi::ParameterMap::Parameter", "dd/d68/classgaudi_1_1ParameterMap_1_1Parameter.html", null ],
    [ "gaudi::ParameterMap", "de/d82/classgaudi_1_1ParameterMap.html", null ],
    [ "cond::ConditionsPrinter::ParamPrinter", "d8/d84/classConditionsPrinter_1_1ParamPrinter.html", null ],
    [ "dd4hep::cms::ParsingContext", "d7/da6/classdd4hep_1_1cms_1_1ParsingContext.html", null ],
    [ "dd4hep::sim::ParticleExtension", "d7/d5d/classdd4hep_1_1sim_1_1ParticleExtension.html", null ],
    [ "dd4hep::align::AlignmentsCalculator::PathOrdering", "d7/dbe/classdd4hep_1_1align_1_1AlignmentsCalculator_1_1PathOrdering.html", null ],
    [ "dd4hep::PersistencyExamples::PersistencyIO", "d5/df7/classdd4hep_1_1PersistencyExamples_1_1PersistencyIO.html", null ],
    [ "dd4hep::sim::Geant4Kernel::PhaseSelector", "d4/d58/classdd4hep_1_1sim_1_1Geant4Kernel_1_1PhaseSelector.html", null ],
    [ "dd4hep::PlacedVolumeProcessor", "d1/d54/classdd4hep_1_1PlacedVolumeProcessor.html", [
      [ "dd4hep::DDCMSDetElementCreator", "d1/d62/classdd4hep_1_1DDCMSDetElementCreator.html", null ],
      [ "dd4hep::DetElementCreator", "d5/deb/classdd4hep_1_1DetElementCreator.html", null ],
      [ "dd4hep::PlacementProcessor< T >", "d3/d95/classdd4hep_1_1PlacementProcessor.html", null ],
      [ "dd4hep::PlacementProcessorShared< T >", "de/df2/classdd4hep_1_1PlacementProcessorShared.html", null ],
      [ "dd4hep::VisDensityProcessor", "d8/dfe/classdd4hep_1_1VisDensityProcessor.html", null ],
      [ "dd4hep::VisMaterialProcessor", "d4/d54/classdd4hep_1_1VisMaterialProcessor.html", null ],
      [ "dd4hep::VisVolNameProcessor", "da/d9f/classdd4hep_1_1VisVolNameProcessor.html", null ]
    ] ],
    [ "dd4hep::PlacedVolumeScanner", "d7/d7d/classdd4hep_1_1PlacedVolumeScanner.html", null ],
    [ "dd4hep::PluginDebug", "db/ddf/structdd4hep_1_1PluginDebug.html", null ],
    [ "dd4hep::PluginFactoryBase", "d9/de3/structdd4hep_1_1PluginFactoryBase.html", [
      [ "dd4hep::ApplyFactory< T >", "d2/d96/classdd4hep_1_1ApplyFactory.html", null ],
      [ "dd4hep::DDCMSDetElementFactory< T >", "d6/dfc/classdd4hep_1_1DDCMSDetElementFactory.html", null ],
      [ "dd4hep::DDCMSDetElementFactory< T >", "d6/dfc/classdd4hep_1_1DDCMSDetElementFactory.html", null ],
      [ "dd4hep::DetectorConstructionFactory< T >", "d0/db2/classdd4hep_1_1DetectorConstructionFactory.html", null ],
      [ "dd4hep::Geant4SensitiveDetectorFactory< T >", "d4/de4/classdd4hep_1_1Geant4SensitiveDetectorFactory.html", null ],
      [ "dd4hep::Geant4SetupAction< T >", "d9/d9d/classdd4hep_1_1Geant4SetupAction.html", null ],
      [ "dd4hep::JsonDetElementFactory< T >", "d6/dda/classdd4hep_1_1JsonDetElementFactory.html", null ],
      [ "dd4hep::SegmentationFactory< T >", "da/d8a/classdd4hep_1_1SegmentationFactory.html", null ],
      [ "dd4hep::TranslationFactory< T >", "de/d33/classdd4hep_1_1TranslationFactory.html", null ],
      [ "dd4hep::XMLConversionFactory< T >", "d7/dd2/classdd4hep_1_1XMLConversionFactory.html", null ],
      [ "dd4hep::XMLDocumentReaderFactory< T >", "d9/d62/classdd4hep_1_1XMLDocumentReaderFactory.html", null ],
      [ "dd4hep::XMLElementFactory< T >", "db/d64/classdd4hep_1_1XMLElementFactory.html", null ],
      [ "dd4hep::XMLObjectFactory< T >", "d5/dd8/classdd4hep_1_1XMLObjectFactory.html", null ],
      [ "dd4hep::XmlDetElementFactory< T >", "d1/d5f/classdd4hep_1_1XmlDetElementFactory.html", null ]
    ] ],
    [ "dd4hep::PluginRegistry< DD4HEP_SIGNATURE >", "d7/dd8/classdd4hep_1_1PluginRegistry.html", null ],
    [ "dd4hep::PluginService", "dd/d77/classdd4hep_1_1PluginService.html", null ],
    [ "dd4hep::PluginTester", "d3/dd3/classdd4hep_1_1PluginTester.html", null ],
    [ "PolyhedralCalorimeter", null, [
      [ "dd4hep::detail::SectorBarrelCalorimeter", "de/d25/classdd4hep_1_1detail_1_1SectorBarrelCalorimeter.html", null ]
    ] ],
    [ "dd4hep::PopupMenu", "df/d51/classdd4hep_1_1PopupMenu.html", [
      [ "dd4hep::DD4hepMenu", "d5/d48/classdd4hep_1_1DD4hepMenu.html", null ],
      [ "dd4hep::ViewMenu", "d0/d8f/classdd4hep_1_1ViewMenu.html", null ]
    ] ],
    [ "dd4hep::sim::PrimaryExtension", "d2/dbe/classdd4hep_1_1sim_1_1PrimaryExtension.html", null ],
    [ "dd4hep::detail::Primitive< T >", "d5/d4f/structdd4hep_1_1detail_1_1Primitive.html", null ],
    [ "dd4hep::CondDB2Objects::PrintConfig", "d3/dcd/classdd4hep_1_1CondDB2Objects_1_1PrintConfig.html", null ],
    [ "dd4hep::DDDB2Objects::PrintConfig", "d3/d8b/classdd4hep_1_1DDDB2Objects_1_1PrintConfig.html", null ],
    [ "dd4hep::Printer< T >", "d5/dc8/structdd4hep_1_1Printer.html", null ],
    [ "dd4hep::PrintMap< T >", "d9/dea/structdd4hep_1_1PrintMap.html", null ],
    [ "dd4hep::sim::Geant4PhysicsList::Process", "dd/d78/classdd4hep_1_1sim_1_1Geant4PhysicsList_1_1Process.html", null ],
    [ "dd4hep::Alignment::Processor", "d1/d23/classdd4hep_1_1Alignment_1_1Processor.html", [
      [ "dd4hep::align::AlignmentsProcessor< T >", "d0/dcb/classdd4hep_1_1align_1_1AlignmentsProcessor.html", null ],
      [ "dd4hep::align::AlignmentsProcessorWrapper< T >", "d1/d0d/classdd4hep_1_1align_1_1AlignmentsProcessorWrapper.html", null ]
    ] ],
    [ "dd4hep::Condition::Processor", "d5/d84/classdd4hep_1_1Condition_1_1Processor.html", [
      [ "dd4hep::cond::ConditionsProcessor< T >", "d2/dfd/classdd4hep_1_1cond_1_1ConditionsProcessor.html", null ],
      [ "dd4hep::cond::ConditionsProcessorWrapper< T >", "dc/d7b/classdd4hep_1_1cond_1_1ConditionsProcessorWrapper.html", null ]
    ] ],
    [ "dd4hep::DetElement::Processor", "d7/d6a/classdd4hep_1_1DetElement_1_1Processor.html", null ],
    [ "dd4hep::PlacedVolume::Processor", "db/d04/classdd4hep_1_1PlacedVolume_1_1Processor.html", null ],
    [ "dd4hep::digi::DigiKernel::Processor", "d1/d2c/classDigiKernel_1_1Processor.html", null ],
    [ "dd4hep::Property", "de/dc2/classdd4hep_1_1Property.html", [
      [ "dd4hep::PropertyValue< TYPE >", "da/d5a/classdd4hep_1_1PropertyValue.html", null ]
    ] ],
    [ "dd4hep::PropertyConfigurator", "da/dde/classdd4hep_1_1PropertyConfigurator.html", null ],
    [ "dd4hep::PropertyGrammar", "d5/d22/classdd4hep_1_1PropertyGrammar.html", null ],
    [ "dd4hep::PropertyInterface", "d3/da3/classdd4hep_1_1PropertyInterface.html", [
      [ "dd4hep::PropertyConfigurable", "d4/d14/classdd4hep_1_1PropertyConfigurable.html", [
        [ "dd4hep::DDDB::DDDBHelper", "d9/d4f/classdd4hep_1_1DDDB_1_1DDDBHelper.html", null ],
        [ "dd4hep::DDDB::DDDBReader", "db/da7/classdd4hep_1_1DDDB_1_1DDDBReader.html", [
          [ "dd4hep::DDDB::DDDBFileReader", "d0/da8/classdd4hep_1_1DDDB_1_1DDDBFileReader.html", null ]
        ] ],
        [ "dd4hep::cond::ConditionsDataLoader", "d5/d82/classdd4hep_1_1cond_1_1ConditionsDataLoader.html", null ],
        [ "dd4hep::cond::ConditionsManagerObject", "d9/db2/classdd4hep_1_1cond_1_1ConditionsManagerObject.html", null ],
        [ "gaudi::DetService", "d1/dfa/classgaudi_1_1DetService.html", null ]
      ] ]
    ] ],
    [ "dd4hep::PropertyManager", "d6/d8e/classdd4hep_1_1PropertyManager.html", null ],
    [ "dd4hep::sim::Geant4GeometryInfo::PropertyVector", "de/da8/structdd4hep_1_1sim_1_1Geant4GeometryInfo_1_1PropertyVector.html", null ],
    [ "PyDDG4", "d7/de0/structPyDDG4.html", null ],
    [ "dd4hep::detail::FalphaNoise::random_engine_wrapper", "dd/def/structdd4hep_1_1detail_1_1FalphaNoise_1_1random__engine__wrapper.html", [
      [ "dd4hep::detail::FalphaNoise::random_engine< ENGINE >", "d2/d59/structdd4hep_1_1detail_1_1FalphaNoise_1_1random__engine.html", null ]
    ] ],
    [ "EVAL::Object::Struct::ReadLock", "db/d09/structEVAL_1_1Object_1_1Struct_1_1ReadLock.html", null ],
    [ "dd4hep::sim::RefCountedSequence< T >", "d6/d32/classdd4hep_1_1sim_1_1RefCountedSequence.html", null ],
    [ "dd4hep::sim::RefCountedSequence< Geant4SensDetActionSequence >", "d6/d32/classdd4hep_1_1sim_1_1RefCountedSequence.html", [
      [ "dd4hep::sim::Geant4SensDet", "df/d9f/classdd4hep_1_1sim_1_1Geant4SensDet.html", null ]
    ] ],
    [ "dd4hep::detail::RefCountHandle< T >", "db/d14/classdd4hep_1_1detail_1_1RefCountHandle.html", null ],
    [ "dd4hep::detail::ReferenceBitMask< T >", "d4/d37/classdd4hep_1_1detail_1_1ReferenceBitMask.html", null ],
    [ "dd4hep::detail::ReferenceObject< T >", "d6/d58/classdd4hep_1_1detail_1_1ReferenceObject.html", null ],
    [ "dd4hep::detail::ReferenceObjects< M >", "dd/d2a/classdd4hep_1_1detail_1_1ReferenceObjects.html", null ],
    [ "dd4hep::ReflectionBuilder", "da/d1e/classdd4hep_1_1ReflectionBuilder.html", null ],
    [ "Gaudi::PluginService::v1::Details::Registry", "dd/d4b/classGaudi_1_1PluginService_1_1v1_1_1Details_1_1Registry.html", null ],
    [ "dd4hep::detail::ReleaseHandle< T >", "df/d94/classdd4hep_1_1detail_1_1ReleaseHandle.html", null ],
    [ "dd4hep::detail::ReleaseHandles< M >", "d7/d35/classdd4hep_1_1detail_1_1ReleaseHandles.html", null ],
    [ "dd4hep::detail::ReleaseObject< T >", "da/d20/classdd4hep_1_1detail_1_1ReleaseObject.html", null ],
    [ "dd4hep::detail::ReleaseObjects< M >", "de/dda/classdd4hep_1_1detail_1_1ReleaseObjects.html", null ],
    [ "TiXmlString::Rep", "da/d0f/structTiXmlString_1_1Rep.html", null ],
    [ "dd4hep::align::AlignmentsCalculator::Result", "d6/d36/classdd4hep_1_1align_1_1AlignmentsCalculator_1_1Result.html", null ],
    [ "dd4hep::cond::ConditionsManager::Result", "de/d9f/classdd4hep_1_1cond_1_1ConditionsManager_1_1Result.html", null ],
    [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >::Operations::result< A, B, C, D >", "d6/de8/structdd4hep_1_1Parsers_1_1MapGrammar_1_1Operations_1_1result.html", null ],
    [ "dd4hep::Parsers::Pnt3DGrammar< Iterator, PointT, Skipper >::Operations::result< A, B, C, D >", "d9/dbf/structdd4hep_1_1Parsers_1_1Pnt3DGrammar_1_1Operations_1_1result.html", null ],
    [ "dd4hep::Parsers::Pnt4DGrammar< Iterator, PointT, Skipper >::Operations::result< A, B, C, D >", "d2/d66/structdd4hep_1_1Parsers_1_1Pnt4DGrammar_1_1Operations_1_1result.html", null ],
    [ "dd4hep::Parsers::Rot3DGrammar< Iterator, PointT, Skipper >::Operations::result< A, B, C, D >", "d3/d6f/structdd4hep_1_1Parsers_1_1Rot3DGrammar_1_1Operations_1_1result.html", null ],
    [ "RUNMANAGER", null, [
      [ "dd4hep::sim::Geant4RunManager< RUNMANAGER >", "d9/d7d/classdd4hep_1_1sim_1_1Geant4RunManager.html", null ]
    ] ],
    [ "std::runtime_error", null, [
      [ "dd4hep::DetectorData::InvalidObjectError", "de/d1a/structdd4hep_1_1DetectorData_1_1InvalidObjectError.html", null ],
      [ "dd4hep::invalid_handle_exception", "da/d6b/classdd4hep_1_1invalid__handle__exception.html", null ],
      [ "dd4hep::unrelated_type_error", "da/dea/structdd4hep_1_1unrelated__type__error.html", null ],
      [ "dd4hep::unrelated_value_error", "dc/dfd/structdd4hep_1_1unrelated__value__error.html", null ]
    ] ],
    [ "dd4hep::detail::safe_cast< TO >", "da/dbd/classdd4hep_1_1detail_1_1safe__cast.html", null ],
    [ "say_hello", "d4/d91/classsay__hello.html", null ],
    [ "dd4hep::align::AlignmentsCalculator::Scanner", "d8/df4/classdd4hep_1_1align_1_1AlignmentsCalculator_1_1Scanner.html", null ],
    [ "dd4hep::Parsers::KeyValueGrammar< Iterator, Skipper >::second", "d5/df7/structdd4hep_1_1Parsers_1_1KeyValueGrammar_1_1second.html", null ],
    [ "dd4hep::Parsers::PairGrammar< Iterator, PairT, Skipper >::second", "d0/d2a/structdd4hep_1_1Parsers_1_1PairGrammar_1_1second.html", null ],
    [ "dd4hep::rec::ConicalSupportStruct::Section", "df/d24/structdd4hep_1_1rec_1_1ConicalSupportStruct_1_1Section.html", null ],
    [ "boost::iostreams::seekable_device_tag", null, [
      [ "dd4hep::dd4hep_file< T >::category", "d5/d2d/structdd4hep_1_1dd4hep__file_1_1category.html", null ]
    ] ],
    [ "dd4hep::DDSegmentation::MegatileLayerGridXY::segInfo", "d7/d8b/structdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY_1_1segInfo.html", null ],
    [ "dd4hep::DDSegmentation::Segmentation", "d9/dec/classdd4hep_1_1DDSegmentation_1_1Segmentation.html", [
      [ "dd4hep::DDSegmentation::CartesianGrid", "d4/d61/classdd4hep_1_1DDSegmentation_1_1CartesianGrid.html", [
        [ "dd4hep::DDSegmentation::CartesianGridXY", "d0/d55/classdd4hep_1_1DDSegmentation_1_1CartesianGridXY.html", [
          [ "dd4hep::DDSegmentation::CartesianGridXYZ", "dc/d1f/classdd4hep_1_1DDSegmentation_1_1CartesianGridXYZ.html", null ]
        ] ],
        [ "dd4hep::DDSegmentation::CartesianGridXZ", "d7/d2d/classdd4hep_1_1DDSegmentation_1_1CartesianGridXZ.html", null ],
        [ "dd4hep::DDSegmentation::CartesianGridYZ", "d2/db1/classdd4hep_1_1DDSegmentation_1_1CartesianGridYZ.html", null ],
        [ "dd4hep::DDSegmentation::MegatileLayerGridXY", "d5/d23/classdd4hep_1_1DDSegmentation_1_1MegatileLayerGridXY.html", null ],
        [ "dd4hep::DDSegmentation::TiledLayerGridXY", "d4/dbc/classdd4hep_1_1DDSegmentation_1_1TiledLayerGridXY.html", null ],
        [ "dd4hep::DDSegmentation::WaferGridXY", "dc/d5a/classdd4hep_1_1DDSegmentation_1_1WaferGridXY.html", null ]
      ] ],
      [ "dd4hep::DDSegmentation::CartesianStrip", "dc/d4f/classdd4hep_1_1DDSegmentation_1_1CartesianStrip.html", [
        [ "dd4hep::DDSegmentation::CartesianStripX", "da/da5/classdd4hep_1_1DDSegmentation_1_1CartesianStripX.html", null ],
        [ "dd4hep::DDSegmentation::CartesianStripY", "d7/dfb/classdd4hep_1_1DDSegmentation_1_1CartesianStripY.html", null ],
        [ "dd4hep::DDSegmentation::CartesianStripZ", "df/d2a/classdd4hep_1_1DDSegmentation_1_1CartesianStripZ.html", null ]
      ] ],
      [ "dd4hep::DDSegmentation::CylindricalSegmentation", "d5/d7f/classdd4hep_1_1DDSegmentation_1_1CylindricalSegmentation.html", [
        [ "dd4hep::DDSegmentation::ProjectiveCylinder", "de/d5d/classdd4hep_1_1DDSegmentation_1_1ProjectiveCylinder.html", null ]
      ] ],
      [ "dd4hep::DDSegmentation::GridPhiEta", "d3/d10/classdd4hep_1_1DDSegmentation_1_1GridPhiEta.html", [
        [ "dd4hep::DDSegmentation::GridRPhiEta", "dd/d23/classdd4hep_1_1DDSegmentation_1_1GridRPhiEta.html", null ]
      ] ],
      [ "dd4hep::DDSegmentation::MultiSegmentation", "d9/d9d/classdd4hep_1_1DDSegmentation_1_1MultiSegmentation.html", null ],
      [ "dd4hep::DDSegmentation::NoSegmentation", "d5/dbc/classdd4hep_1_1DDSegmentation_1_1NoSegmentation.html", null ],
      [ "dd4hep::DDSegmentation::PolarGrid", "d0/dc8/classdd4hep_1_1DDSegmentation_1_1PolarGrid.html", [
        [ "dd4hep::DDSegmentation::PolarGridRPhi", "d7/db7/classdd4hep_1_1DDSegmentation_1_1PolarGridRPhi.html", null ],
        [ "dd4hep::DDSegmentation::PolarGridRPhi2", "d6/dc1/classdd4hep_1_1DDSegmentation_1_1PolarGridRPhi2.html", null ]
      ] ],
      [ "dd4hep::DDSegmentation::TiledLayerSegmentation", "d9/dc6/classdd4hep_1_1DDSegmentation_1_1TiledLayerSegmentation.html", null ]
    ] ],
    [ "dd4hep::digi::segmentation_data< SEGMENTATION >", "d8/ddd/classdd4hep_1_1digi_1_1segmentation__data.html", null ],
    [ "dd4hep::digi::segmentation_data< CartesianGridXY >", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html", null ],
    [ "dd4hep::digi::segmentation_data< CartesianGridXYZ >", "d7/d74/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXYZ_01_4.html", null ],
    [ "dd4hep::digi::segmentation_data< segmentation_t >", "d8/ddd/classdd4hep_1_1digi_1_1segmentation__data.html", null ],
    [ "dd4hep::SegmentationObject", "dd/dfd/classdd4hep_1_1SegmentationObject.html", [
      [ "dd4hep::SegmentationWrapper< IMP >", "d1/d72/classdd4hep_1_1SegmentationWrapper.html", null ]
    ] ],
    [ "dd4hep::DDSegmentation::SegmentationParameter", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html", [
      [ "dd4hep::DDSegmentation::TypedSegmentationParameter< TYPE >", "da/d71/classdd4hep_1_1DDSegmentation_1_1TypedSegmentationParameter.html", null ],
      [ "dd4hep::DDSegmentation::TypedSegmentationParameter< std::vector< TYPE > >", "dd/d49/classdd4hep_1_1DDSegmentation_1_1TypedSegmentationParameter_3_01std_1_1vector_3_01TYPE_01_4_01_4.html", null ]
    ] ],
    [ "dd4hep::detail::Select1st< T >", "de/d83/classdd4hep_1_1detail_1_1Select1st.html", null ],
    [ "dd4hep::detail::Select2nd< T >", "d2/d14/classdd4hep_1_1detail_1_1Select2nd.html", null ],
    [ "dd4hep::rec::ZDiskPetalsStruct::SensorType", "df/d28/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1SensorType.html", null ],
    [ "dd4hep::sim::SequenceHdl< T >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", null ],
    [ "dd4hep::sim::SequenceHdl< Geant4DetectorConstructionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserDetectorConstruction", "d2/da1/classdd4hep_1_1sim_1_1Geant4UserDetectorConstruction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4EventActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserEventAction", "df/d1d/classdd4hep_1_1sim_1_1Geant4UserEventAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4GeneratorActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserGeneratorAction", "d7/d26/classdd4hep_1_1sim_1_1Geant4UserGeneratorAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4RunActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserRunAction", "de/ded/classdd4hep_1_1sim_1_1Geant4UserRunAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4StackingActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserStackingAction", "d5/d63/classdd4hep_1_1sim_1_1Geant4UserStackingAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4SteppingActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserSteppingAction", "d6/d48/classdd4hep_1_1sim_1_1Geant4UserSteppingAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4TrackingActionSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserTrackingAction", "df/de2/classdd4hep_1_1sim_1_1Geant4UserTrackingAction.html", null ]
    ] ],
    [ "dd4hep::sim::SequenceHdl< Geant4UserInitializationSequence >", "d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html", [
      [ "dd4hep::sim::Geant4UserActionInitialization", "d2/d77/classdd4hep_1_1sim_1_1Geant4UserActionInitialization.html", null ]
    ] ],
    [ "dd4hep::detail::SetBinder", "d4/d36/structdd4hep_1_1detail_1_1SetBinder.html", null ],
    [ "DDG4TestSetup.Setup", "d9/d3b/classDDG4TestSetup_1_1Setup.html", [
      [ "MiniTelSetup.Setup", "d2/d9d/classMiniTelSetup_1_1Setup.html", null ],
      [ "MiniTelSetup.Setup", "d2/d9d/classMiniTelSetup_1_1Setup.html", null ]
    ] ],
    [ "dd4hep::sim::SimpleEvent", "de/de2/classdd4hep_1_1sim_1_1SimpleEvent.html", null ],
    [ "dd4hep::sim::SimpleRun", "dd/d91/classdd4hep_1_1sim_1_1SimpleRun.html", null ],
    [ "dd4hep::DDEve::SimulationHit", "d7/d81/classdd4hep_1_1DDEve_1_1SimulationHit.html", null ],
    [ "dd4hep::BasicGrammar::specialization_t", "d5/d7f/structdd4hep_1_1BasicGrammar_1_1specialization__t.html", null ],
    [ "dd4hep::SpecPar", "de/dde/structdd4hep_1_1SpecPar.html", null ],
    [ "dd4hep::SpecParRegistry", "d8/df7/structdd4hep_1_1SpecParRegistry.html", null ],
    [ "dd4hep::rec::Vector3D::Spherical", "d0/ddb/structdd4hep_1_1rec_1_1Vector3D_1_1Spherical.html", null ],
    [ "dd4hep::align::GlobalAlignmentStack::StackEntry", "d4/d7f/classdd4hep_1_1align_1_1GlobalAlignmentStack_1_1StackEntry.html", null ],
    [ "dd4hep::STD_Conditions", "d5/d37/classdd4hep_1_1STD__Conditions.html", null ],
    [ "dd4hep::sim::Geant4GeometryScanner::StepInfo", "d2/d35/classdd4hep_1_1sim_1_1Geant4GeometryScanner_1_1StepInfo.html", null ],
    [ "dd4hep::sim::Geant4MaterialScanner::StepInfo", "dc/de6/classdd4hep_1_1sim_1_1Geant4MaterialScanner_1_1StepInfo.html", null ],
    [ "std::string", null, [
      [ "dd4hep::Path", "df/d7b/classdd4hep_1_1Path.html", null ],
      [ "dd4hep::sim::Geant4PhysicsList::ParticleConstructor", "d2/d5e/classdd4hep_1_1sim_1_1Geant4PhysicsList_1_1ParticleConstructor.html", null ],
      [ "dd4hep::sim::Geant4PhysicsList::PhysicsConstructor", "d8/d4c/classdd4hep_1_1sim_1_1Geant4PhysicsList_1_1PhysicsConstructor.html", null ]
    ] ],
    [ "std::stringstream", null, [
      [ "dd4hep::cms::LogDebug", "d7/dfc/classdd4hep_1_1cms_1_1LogDebug.html", [
        [ "dd4hep::cms::LogWarn", "da/d5a/classdd4hep_1_1cms_1_1LogWarn.html", null ],
        [ "dd4hep::cms::LogWarn", "da/d5a/classdd4hep_1_1cms_1_1LogWarn.html", null ]
      ] ],
      [ "dd4hep::cms::LogDebug", "d7/dfc/classdd4hep_1_1cms_1_1LogDebug.html", null ]
    ] ],
    [ "dd4hep::DDSegmentation::StringTokenizer", "dc/dc9/classdd4hep_1_1DDSegmentation_1_1StringTokenizer.html", null ],
    [ "StripInfo", "d2/d71/classDeVeloSensor_1_1StripInfo.html", null ],
    [ "gaudi::DeVeloSensorElement::StripInfo", "d7/d07/classgaudi_1_1DeVeloSensorElement_1_1StripInfo.html", null ],
    [ "dd4hep::xml::Strng_t", "d6/d78/classdd4hep_1_1xml_1_1Strng__t.html", [
      [ "dd4hep::xml::Tag_t", "d8/d2c/classdd4hep_1_1xml_1_1Tag__t.html", null ]
    ] ],
    [ "EVAL::Object::Struct", "d5/dce/structEVAL_1_1Object_1_1Struct.html", null ],
    [ "ctypes.Structure", null, [
      [ "GaudiPluginService.cpluginsvc.Factory", "d5/d9c/classGaudiPluginService_1_1cpluginsvc_1_1Factory.html", null ],
      [ "GaudiPluginService.cpluginsvc.Property", "d6/d1e/classGaudiPluginService_1_1cpluginsvc_1_1Property.html", null ],
      [ "GaudiPluginService.cpluginsvc.Registry", "d5/de4/classGaudiPluginService_1_1cpluginsvc_1_1Registry.html", null ]
    ] ],
    [ "dd4hep::rec::SurfaceHelper", "df/d02/classdd4hep_1_1rec_1_1SurfaceHelper.html", null ],
    [ "dd4hep::SurfaceInstaller", "d5/d77/classdd4hep_1_1SurfaceInstaller.html", null ],
    [ "dd4hep::rec::SurfaceManager", "d4/d03/classdd4hep_1_1rec_1_1SurfaceManager.html", null ],
    [ "dd4hep::rec::SurfaceType", "d9/d8e/classdd4hep_1_1rec_1_1SurfaceType.html", null ],
    [ "T", null, [
      [ "dd4hep::rec::StructExtension< T >", "dd/dcc/structdd4hep_1_1rec_1_1StructExtension.html", null ]
    ] ],
    [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >::tag_key", "d8/d0e/structdd4hep_1_1Parsers_1_1MapGrammar_1_1tag__key.html", null ],
    [ "dd4hep::Parsers::MapGrammar< Iterator, MapT, Skipper >::tag_mapped", "d4/d83/structdd4hep_1_1Parsers_1_1MapGrammar_1_1tag__mapped.html", null ],
    [ "TestCounter", "dc/d46/structTestCounter.html", null ],
    [ "TestCounters", "d1/d06/structTestCounters.html", null ],
    [ "dd4hep::ConditionExamples::TestEnv", "db/d14/structdd4hep_1_1ConditionExamples_1_1TestEnv.html", null ],
    [ "TestTuple", "d7/d85/classTestTuple.html", null ],
    [ "TEveElementList", "d9/ddd/classTEveElementList.html", [
      [ "dd4hep::ElementList", "d8/d16/classdd4hep_1_1ElementList.html", null ]
    ] ],
    [ "TGeoConeSeg", "d1/d56/classTGeoConeSeg.html", null ],
    [ "TGeoExtension", "da/d5d/classTGeoExtension.html", [
      [ "dd4hep::PlacedVolumeExtension", "d8/d46/classdd4hep_1_1PlacedVolumeExtension.html", null ],
      [ "dd4hep::VolumeExtension", "d5/d43/classdd4hep_1_1VolumeExtension.html", null ]
    ] ],
    [ "TGeoTubeSeg", null, [
      [ "dd4hep::TwistedTubeObject", "de/d82/classdd4hep_1_1TwistedTubeObject.html", null ]
    ] ],
    [ "TGLAnnotation", "d3/d5f/classTGLAnnotation.html", [
      [ "dd4hep::Annotation", "d5/d78/classdd4hep_1_1Annotation.html", null ]
    ] ],
    [ "TGMainFrame", "d4/dd1/classTGMainFrame.html", [
      [ "dd4hep::FrameControl", "db/dc8/classdd4hep_1_1FrameControl.html", [
        [ "dd4hep::EventControl", "d1/d6f/classdd4hep_1_1EventControl.html", null ]
      ] ]
    ] ],
    [ "TiXmlAttributeSet", "df/dda/classTiXmlAttributeSet.html", null ],
    [ "TiXmlBase", "d8/d47/classTiXmlBase.html", [
      [ "TiXmlAttribute", "d4/dc1/classTiXmlAttribute.html", null ],
      [ "TiXmlNode", "d3/dd5/classTiXmlNode.html", [
        [ "TiXmlComment", "de/d43/classTiXmlComment.html", null ],
        [ "TiXmlDeclaration", "d2/df2/classTiXmlDeclaration.html", null ],
        [ "TiXmlDocument", "df/d09/classTiXmlDocument.html", null ],
        [ "TiXmlElement", "db/d59/classTiXmlElement.html", null ],
        [ "TiXmlText", "d4/d9a/classTiXmlText.html", null ],
        [ "TiXmlUnknown", "d5/d04/classTiXmlUnknown.html", null ]
      ] ]
    ] ],
    [ "TiXmlCursor", "d7/dfc/structTiXmlCursor.html", null ],
    [ "TiXmlHandle", "dd/d98/classTiXmlHandle.html", null ],
    [ "TiXmlHandle_t", "d5/d4e/classTiXmlHandle__t.html", null ],
    [ "TiXmlParsingData", "d6/d7d/classTiXmlParsingData.html", null ],
    [ "TiXmlString", "d1/d15/classTiXmlString.html", [
      [ "TiXmlOutStream", "dc/d34/classTiXmlOutStream.html", null ]
    ] ],
    [ "TiXmlVisitor", "d9/d74/classTiXmlVisitor.html", [
      [ "TiXmlPrinter", "d4/d3c/classTiXmlPrinter.html", null ]
    ] ],
    [ "TNamed", "d5/dda/classTNamed.html", [
      [ "DD4hepRootPersistency", "df/d89/classDD4hepRootPersistency.html", null ],
      [ "dd4hep::DetectorImp", "d5/dc6/classdd4hep_1_1DetectorImp.html", null ],
      [ "dd4hep::cond::ConditionsRootPersistency", "d5/d0a/classdd4hep_1_1cond_1_1ConditionsRootPersistency.html", null ],
      [ "dd4hep::cond::ConditionsTreePersistency", "d9/dae/classdd4hep_1_1cond_1_1ConditionsTreePersistency.html", null ],
      [ "dd4hep::sim::Geant4GeometryInfo", "d6/d8e/classdd4hep_1_1sim_1_1Geant4GeometryInfo.html", null ]
    ] ],
    [ "TObject", "d5/d0f/classTObject.html", [
      [ "LCIOTObject< T >", "dd/d3a/classLCIOTObject.html", null ],
      [ "dd4hep::ContextMenuHandler", "df/de9/classdd4hep_1_1ContextMenuHandler.html", null ]
    ] ],
    [ "dd4hep::sim::TrackerCombine", "d2/de4/structdd4hep_1_1sim_1_1TrackerCombine.html", null ],
    [ "dd4hep::sim::TrackerWeighted", "dc/d34/structdd4hep_1_1sim_1_1TrackerWeighted.html", null ],
    [ "TYPE", null, [
      [ "gaudi::DetectorElement< TYPE >", "d4/d80/classgaudi_1_1DetectorElement.html", null ],
      [ "gaudi::DetectorStaticElement< TYPE >", "dc/d82/classgaudi_1_1DetectorStaticElement.html", null ]
    ] ],
    [ "dd4hep::DDSegmentation::TypeName< TYPE >", "dc/dee/structdd4hep_1_1DDSegmentation_1_1TypeName.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< double >", "d6/d4a/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01double_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< float >", "d5/db3/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01float_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< int >", "d9/df9/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01int_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< std::string >", "d1/d59/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01std_1_1string_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< std::vector< double > >", "dd/d8e/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01std_1_1vector_3_01double_01_4_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< std::vector< float > >", "de/d06/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01std_1_1vector_3_01float_01_4_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< std::vector< int > >", "dd/def/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01std_1_1vector_3_01int_01_4_01_4.html", null ],
    [ "dd4hep::DDSegmentation::TypeName< std::vector< std::string > >", "d9/dfd/structdd4hep_1_1DDSegmentation_1_1TypeName_3_01std_1_1vector_3_01std_1_1string_01_4_01_4.html", null ],
    [ "std::unique_ptr", null, [
      [ "dd4hep::dd4hep_ptr< dd4hep::sim::DataExtension >", "dd/d27/classdd4hep_1_1dd4hep__ptr.html", null ],
      [ "dd4hep::dd4hep_ptr< dd4hep::sim::ParticleExtension >", "dd/d27/classdd4hep_1_1dd4hep__ptr.html", null ],
      [ "dd4hep::dd4hep_ptr< PrimaryExtension >", "dd/d27/classdd4hep_1_1dd4hep__ptr.html", null ],
      [ "dd4hep::dd4hep_ptr< dd4hep::sim::VertexExtension >", "dd/d27/classdd4hep_1_1dd4hep__ptr.html", null ],
      [ "dd4hep::dd4hep_ptr< T >", "dd/d27/classdd4hep_1_1dd4hep__ptr.html", null ]
    ] ],
    [ "dd4hep::xml::UriReader", "d4/dd6/classdd4hep_1_1xml_1_1UriReader.html", [
      [ "dd4hep::DDDB::DDDBReader", "db/da7/classdd4hep_1_1DDDB_1_1DDDBReader.html", null ],
      [ "dd4hep::xml::UriContextReader", "de/d26/classdd4hep_1_1xml_1_1UriContextReader.html", null ]
    ] ],
    [ "dd4hep::xml::UriReader::UserContext", "d0/dfe/structdd4hep_1_1xml_1_1UriReader_1_1UserContext.html", [
      [ "dd4hep::DDDB::DDDBReaderContext", "d7/d51/classdd4hep_1_1DDDB_1_1DDDBReaderContext.html", null ]
    ] ],
    [ "dd4hep::detail::ValueBinder", "db/d4d/structdd4hep_1_1detail_1_1ValueBinder.html", null ],
    [ "dd4hep::DisplayConfiguration::Config::Values", "d5/d44/uniondd4hep_1_1DisplayConfiguration_1_1Config_1_1Values.html", null ],
    [ "std::vector", null, [
      [ "dd4hep::PlacedVolumeExtension::VolIDs", "df/dc3/classdd4hep_1_1PlacedVolumeExtension_1_1VolIDs.html", null ],
      [ "dd4hep::digi::DigiContainer< T >", "d1/d68/classdd4hep_1_1digi_1_1DigiContainer.html", null ]
    ] ],
    [ "dd4hep::rec::Vector2D", "d3/d2c/classdd4hep_1_1rec_1_1Vector2D.html", null ],
    [ "dd4hep::DDSegmentation::Vector3D", "de/d55/structdd4hep_1_1DDSegmentation_1_1Vector3D.html", null ],
    [ "dd4hep::rec::Vector3D", "d9/d83/classdd4hep_1_1rec_1_1Vector3D.html", null ],
    [ "dd4hep::detail::VectorBinder", "d0/d7c/structdd4hep_1_1detail_1_1VectorBinder.html", null ],
    [ "dd4hep::sim::VertexExtension", "db/d6b/classdd4hep_1_1sim_1_1VertexExtension.html", null ],
    [ "dd4hep::View", "d5/d53/classdd4hep_1_1View.html", [
      [ "dd4hep::Calo3DProjection", "d2/d7f/classdd4hep_1_1Calo3DProjection.html", null ],
      [ "dd4hep::CaloLego", "d2/d33/classdd4hep_1_1CaloLego.html", null ],
      [ "dd4hep::MultiView", "d5/dd7/classdd4hep_1_1MultiView.html", null ],
      [ "dd4hep::Projection", "d5/dc8/classdd4hep_1_1Projection.html", [
        [ "dd4hep::Calo2DProjection", "da/ded/classdd4hep_1_1Calo2DProjection.html", null ],
        [ "dd4hep::RhoPhiProjection", "d4/dac/classdd4hep_1_1RhoPhiProjection.html", null ],
        [ "dd4hep::RhoZProjection", "df/d3f/classdd4hep_1_1RhoZProjection.html", null ]
      ] ],
      [ "dd4hep::View3D", "d5/d72/classdd4hep_1_1View3D.html", null ]
    ] ],
    [ "dd4hep::xml::tools::VolumeBuilder", "d3/da7/classdd4hep_1_1xml_1_1tools_1_1VolumeBuilder.html", null ],
    [ "dd4hep::detail::VolumeManager_Populator", "db/d50/classdd4hep_1_1detail_1_1VolumeManager__Populator.html", null ],
    [ "dd4hep::VolumeManagerContext", "d7/d32/classdd4hep_1_1VolumeManagerContext.html", [
      [ "dd4hep::detail::VolumeManagerContextExtension", "d4/d64/classdd4hep_1_1detail_1_1VolumeManagerContextExtension.html", null ]
    ] ],
    [ "dd4hep::cond::ConditionsDependencyHandler::Work", "dc/d71/structdd4hep_1_1cond_1_1ConditionsDependencyHandler_1_1Work.html", null ],
    [ "dd4hep::Callback::Wrapper< T >", "d0/d8e/classdd4hep_1_1Callback_1_1Wrapper.html", null ],
    [ "dd4hep::digi::DigiKernel::Wrapper", "d6/dc0/classDigiKernel_1_1Wrapper.html", null ],
    [ "EVAL::Object::Struct::WriteLock", "dd/df1/structEVAL_1_1Object_1_1Struct_1_1WriteLock.html", null ],
    [ "Xml", "d7/d01/unionXml.html", null ],
    [ "dd4hep::xml::XmlString", "d1/dfa/classdd4hep_1_1xml_1_1XmlString.html", null ],
    [ "dd4hep::rec::ZDiskPetalsStruct", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html", null ],
    [ "dd4hep::rec::ZPlanarStruct", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html", null ],
    [ "LHCbConfigurableUser", null, [
      [ "GeoExtract.MyTest", "de/d10/classGeoExtract_1_1MyTest.html", null ],
      [ "GeoExtract.MyTest", "de/d10/classGeoExtract_1_1MyTest.html", null ]
    ] ]
];
var DDCore_2include_2DD4hep_2config_8h =
[
    [ "DD4HEP_INSTANCE_COUNTS", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#a2919d6d64ce0d535150ff79b510e30f3", null ],
    [ "DD4HEP_MINIMAL_CONDITIONS", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#a34123d48874f2a87a4e08734ddb3379d", null ],
    [ "DD4HEP_PLUGINSVC_VERSION", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#a7d22a111ce0e698c4284e05cbdb2a1fa", null ],
    [ "DD4HEP_USE_SAFE_CAST", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#afc7d4c61ff41d536dd7eec175b69c250", null ],
    [ "DECREMENT_COUNTER", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#ab1be71398be7aa3d9544068de11384c3", null ],
    [ "INCREMENT_COUNTER", "df/d43/DDCore_2include_2DD4hep_2config_8h.html#a75d64779ae4a2116dcda0a332bcb5f9e", null ]
];
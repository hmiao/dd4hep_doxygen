var classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField =
[
    [ "__init__", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a177337b99c80425f741c4cb98b53d548", null ],
    [ "delta_chord", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#adf353e7b7c0b6bdb5a07a7878dd88748", null ],
    [ "delta_intersection", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a74da58e968400f08f6b8c9620416b1e5", null ],
    [ "delta_one_step", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a57216eef88d12c86540e4a10fc641e68", null ],
    [ "eps_max", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a427cc26a2e5b4f2958487f6be5cd0e8a", null ],
    [ "eps_min", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a5aab27849e1b5d84b2ad4e474d2de08e", null ],
    [ "equation", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#a83493c6afeb91afd1f1806dc9571f7bd", null ],
    [ "largest_step", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#abfae22283e4032961b3e4dcbab7e59b9", null ],
    [ "min_chord_step", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#acb9b1780b3880eba8512d4b245b01ab6", null ],
    [ "stepper", "df/dae/classDDSim_1_1Helper_1_1MagneticField_1_1MagneticField.html#ac9d1c532a2eeed45a3579b8480aec3cb", null ]
];
var Geant4Particle_8h =
[
    [ "Geant4ParticleProperties", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0", [
      [ "G4PARTICLE_CREATED_HIT", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a8b96375ce9d62179303e1efc6f1c2372", null ],
      [ "G4PARTICLE_PRIMARY", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a3a99da8d036d1b807a38ece55fe000eb", null ],
      [ "G4PARTICLE_HAS_SECONDARIES", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a4c98244faae986c9af7b6dd800823976", null ],
      [ "G4PARTICLE_ABOVE_ENERGY_THRESHOLD", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a22449b6ca53863fb13e1beaeeae57320", null ],
      [ "G4PARTICLE_KEEP_PROCESS", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a5a743f41165efaf4960e90267cb2ddec", null ],
      [ "G4PARTICLE_KEEP_PARENT", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a6eeb3899601c3c77c6d926d2e7a4df3b", null ],
      [ "G4PARTICLE_CREATED_CALORIMETER_HIT", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a2413504642289fe5373b246983c890ba", null ],
      [ "G4PARTICLE_CREATED_TRACKER_HIT", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0ac518a3f07ffc296ff22632efe42ade8d", null ],
      [ "G4PARTICLE_KEEP_USER", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a16f482af33e81253a67c294fbdf6389a", null ],
      [ "G4PARTICLE_KEEP_ALWAYS", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0aee78b70389e71fb4b1430519f92839ff", null ],
      [ "G4PARTICLE_FORCE_KILL", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0acfd0f2055cfe7f7e67077c01200bfca8", null ],
      [ "G4PARTICLE_GEN_EMPTY", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a22f796f63cf99b1256d9160a1c12b4c7", null ],
      [ "G4PARTICLE_GEN_STABLE", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a5bbf6c194e165d032449c34427629501", null ],
      [ "G4PARTICLE_GEN_DECAYED", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a64dcfcb80b46d1c6e007fd9046de2ef9", null ],
      [ "G4PARTICLE_GEN_DOCUMENTATION", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0ab545daeb6bc1ee885c69aa555c4e6e49", null ],
      [ "G4PARTICLE_GEN_BEAM", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a5e731c8c73efb4e135bd25dce435693e", null ],
      [ "G4PARTICLE_GEN_OTHER", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a1977087a1ba9ac6b8a42f39d692a6ed3", null ],
      [ "G4PARTICLE_GEN_GENERATOR", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0aeb44dad13823d17774aa6f385205d267", null ],
      [ "G4PARTICLE_GEN_STATUS", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a07dc7807e3d030eb45360dbd0ea8ac00", null ],
      [ "G4PARTICLE_GEN_STATUS_MASK", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0ac92a4e17a0091f7b9be93a79212a5b7e", null ],
      [ "G4PARTICLE_SIM_CREATED", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a5577c32f2b7e9dcf60e5ce1830a945a7", null ],
      [ "G4PARTICLE_SIM_BACKSCATTER", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a23558950a440aebcee974d7b166cf1cf", null ],
      [ "G4PARTICLE_SIM_DECAY_CALO", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a2f9d64879f9b2481c52308814654a871", null ],
      [ "G4PARTICLE_SIM_DECAY_TRACKER", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0ac16b4ce62aa9a66201efbc807fef202f", null ],
      [ "G4PARTICLE_SIM_STOPPED", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0af7f002bf2b13bd46b21307a6fc2f66cf", null ],
      [ "G4PARTICLE_SIM_LEFT_DETECTOR", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a1ac335be7a5f6ec108c683203b6845a9", null ],
      [ "G4PARTICLE_SIM_PARENT_RADIATED", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0aebe024ed0a8c7d0e82609bb4047390d6", null ],
      [ "G4PARTICLE_SIM_OVERLAY", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0a79528b976cf3bb4f56990b558f8974f5", null ],
      [ "G4PARTICLE_LAST_NOTHING", "df/d8f/Geant4Particle_8h.html#a210c35d042c278db01abf08317239ce0abecb61f644a987a07ab2028002d8b5fa", null ]
    ] ]
];
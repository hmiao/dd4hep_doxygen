var SegmentationUtil_8h =
[
    [ "cosThetaFromXYZ", "df/dd5/SegmentationUtil_8h.html#a2d5b0ed34eb9ad7be415dc2810466660", null ],
    [ "etaFromXYZ", "df/dd5/SegmentationUtil_8h.html#a8985d3f6042210fa39d2669280f0ed60", null ],
    [ "magFromRPhiZ", "df/dd5/SegmentationUtil_8h.html#a228f607a36c590c89614938b301751d5", null ],
    [ "magFromXYZ", "df/dd5/SegmentationUtil_8h.html#a7e78caf3854fe1b6d7b35b15815b4a09", null ],
    [ "phiFromXYZ", "df/dd5/SegmentationUtil_8h.html#a0ff71c9cdedc8085dc2bb1124fdaa7b6", null ],
    [ "positionFromMagThetaPhi", "df/dd5/SegmentationUtil_8h.html#a68505e37c39bb6113e49254e5dcb28dd", null ],
    [ "positionFromREtaPhi", "df/dd5/SegmentationUtil_8h.html#a3b993ce9a1a9da29dbe8b51acbffd339", null ],
    [ "positionFromRPhiZ", "df/dd5/SegmentationUtil_8h.html#aff19091012aea7486c209b43378544b5", null ],
    [ "positionFromRThetaPhi", "df/dd5/SegmentationUtil_8h.html#ad9d7777b5737fee17763ddf4aba00865", null ],
    [ "radiusFromXYZ", "df/dd5/SegmentationUtil_8h.html#a0b558bb65200c630cee4d32647f8b875", null ],
    [ "thetaFromRPhiZ", "df/dd5/SegmentationUtil_8h.html#aa0bf08dd22e7c6a5adbd34d63d1f89e9", null ],
    [ "thetaFromXYZ", "df/dd5/SegmentationUtil_8h.html#a4fc22ec74ce2445891bcd2e346d1e628", null ],
    [ "xFromRPhiZ", "df/dd5/SegmentationUtil_8h.html#aa7f6c10d4ba5c073e1d2833da1ff3d64", null ],
    [ "yFromRPhiZ", "df/dd5/SegmentationUtil_8h.html#ad4edee1af09b92d9bc6a63bf0e94d94a", null ]
];
var classdd4hep_1_1sim_1_1Geant4HitData =
[
    [ "Contribution", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a58f195eb5488a9db697fe10bf89fbe82", null ],
    [ "Contributions", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a8982583c254a365f444779ec4788e41a", null ],
    [ "HitFlags", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5", [
      [ "HIT_KILLED_TRACK", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a9c3f5121b71cb099ae186e05b787f4fa", null ],
      [ "HIT_SECONDARY_TRACK", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a77fe57bb4ff5db03a0aecf1e168d1661", null ],
      [ "HIT_NEW_TRACK", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a025679e43bea5b3d6a5f3bf2d5af6622", null ],
      [ "HIT_STARTED_INSIDE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a7345b0e758158b3a0ff0c8a0b6aa1132", null ],
      [ "HIT_STARTED_SURFACE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a7751ac1d39274cbc43bcbbe72f1fd34b", null ],
      [ "HIT_STARTED_OUTSIDE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a23a7db97f81fdf7d16f518492f156c46", null ],
      [ "HIT_ENDED_INSIDE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a668ccfec4a7124c29bcdb2774bf27568", null ],
      [ "HIT_ENDED_SURFACE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5a9eb65f015c93b61b91552c8c4012dca2", null ],
      [ "HIT_ENDED_OUTSIDE", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a1aaa34ecd2ddd1b0d963265b5509f8b5adcb74ee299933cd405a451c1094ec397", null ]
    ] ],
    [ "Geant4HitData", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a720c9f6193739b3ce4f23c321a4af1be", null ],
    [ "~Geant4HitData", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a57540b8795c4a198a0925773269d99f3", null ],
    [ "extractContribution", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a047b0d85023ac1cd8cad8317cc7899a0", null ],
    [ "extractContribution", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#aa05fcd729fd401c61e2fa7a48887efc5", null ],
    [ "extractContribution", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a53b06813612ad68f26dc3ad1ca2662b3", null ],
    [ "cellID", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#ad0947071968b76eb67ff1fb9e1e733a8", null ],
    [ "extension", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a21a4701e1ee2119daf7c2e0012b43f99", null ],
    [ "flag", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#ad6829fb04f2874ed3cbd69370619858d", null ],
    [ "g4ID", "df/da4/classdd4hep_1_1sim_1_1Geant4HitData.html#a333f6be2094fed7bd3c9baa7cfdd3095", null ]
];
var classdd4hep_1_1Layer =
[
    [ "Layer", "df/d05/classdd4hep_1_1Layer.html#a7352ae5bf1e60b24c56923b006e2d241", null ],
    [ "Layer", "df/d05/classdd4hep_1_1Layer.html#af13a088b4e2d5aa4cd2a17a7bec8f047", null ],
    [ "add", "df/d05/classdd4hep_1_1Layer.html#adc2bf887febf1e3e74fff945fb9a2af6", null ],
    [ "compute", "df/d05/classdd4hep_1_1Layer.html#a7353fc97976976a6d739e37af18954de", null ],
    [ "operator=", "df/d05/classdd4hep_1_1Layer.html#a7092c99e653c38da3d57b8122268f966", null ],
    [ "thickness", "df/d05/classdd4hep_1_1Layer.html#a5c8a334a87970731935c700637811f4b", null ],
    [ "thicknessWithPreOffset", "df/d05/classdd4hep_1_1Layer.html#a9cb56f66069db030c5507beba336f656", null ],
    [ "_preOffset", "df/d05/classdd4hep_1_1Layer.html#ac55877aaf30fe7be8712e5e6eefa5b6f", null ],
    [ "_slices", "df/d05/classdd4hep_1_1Layer.html#a8c1345502dea29d4a2400f75dc403791", null ],
    [ "_thickness", "df/d05/classdd4hep_1_1Layer.html#abdd55d735e7b4c63f048e0018c3fa13d", null ]
];
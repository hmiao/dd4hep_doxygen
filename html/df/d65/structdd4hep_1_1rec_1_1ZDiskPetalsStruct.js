var structdd4hep_1_1rec_1_1ZDiskPetalsStruct =
[
    [ "LayerLayout", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout" ],
    [ "SensorType", "df/d28/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1SensorType.html", null ],
    [ "angleStrip", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html#ab80e3174661c9f98b2a951793b7ec9ec", null ],
    [ "layers", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html#a789cad2e9fadb055f9d07ff6e2f00011", null ],
    [ "lengthStrip", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html#aa483904b7ea2b6a597d2118e9a01cc64", null ],
    [ "pitchStrip", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html#aa51175937fd6071d22deff003e041777", null ],
    [ "widthStrip", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html#a77637acfa992186ba56d1756a2be67c9", null ]
];
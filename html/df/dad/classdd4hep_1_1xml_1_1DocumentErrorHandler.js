var classdd4hep_1_1xml_1_1DocumentErrorHandler =
[
    [ "DocumentErrorHandler", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#a572d17a3a8f0fef694774bf14675f42f", null ],
    [ "~DocumentErrorHandler", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#aaabeea40897554bd0edf07f0c8f1ec26", null ],
    [ "error", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#a2446b2b35014fb643e122618f7b4d97b", null ],
    [ "fatalError", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#a306eda580315f483aeb615381c7b2367", null ],
    [ "handleError", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#aa33e3e1666eb28eec254d385dd5856d9", null ],
    [ "resetErrors", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#ae16a1a234671d3673831f9f8422fded4", null ],
    [ "warning", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html#a8f9278fa3177cd456debd575a00dcbee", null ]
];
var classdd4hep_1_1EventHandler =
[
    [ "Collection", "d2/dd2/classdd4hep_1_1EventHandler.html#a968c928eb942957b903dafbac8b02fb8", null ],
    [ "TypedEventCollections", "d2/dd2/classdd4hep_1_1EventHandler.html#a0e3024e6c29aed480a31dff0a105ef1c", null ],
    [ "CollectionType", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683c", [
      [ "NO_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683ca6533018554dfdd434b4b4cca7e721159", null ],
      [ "PARTICLE_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683ca90517c6b6c9cb7c1cd080022794e787d", null ],
      [ "TRACK_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683cad54fc2a6c4243ff55ffb593e7cf15d7e", null ],
      [ "CALO_HIT_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683ca51dc582e84992cf9c3cc279b9206fcf1", null ],
      [ "TRACKER_HIT_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683ca522c67f090173b54ed8f2b64a4170025", null ],
      [ "HIT_COLLECTION", "d2/dd2/classdd4hep_1_1EventHandler.html#a7ec248529aa290c15bde0eb05ccd683ca6efb5e53fb01831a6b8821b5d5013926", null ]
    ] ],
    [ "EventHandler", "d2/dd2/classdd4hep_1_1EventHandler.html#a49597a24bf95472af2b9d88d03b7ca25", null ],
    [ "~EventHandler", "d2/dd2/classdd4hep_1_1EventHandler.html#a4ea8263e9540831f4941a2cb738920a2", null ],
    [ "ClassDef", "d2/dd2/classdd4hep_1_1EventHandler.html#a2c760a5ddfc7a39df48d066b7f191ec7", null ],
    [ "collectionLoop", "d2/dd2/classdd4hep_1_1EventHandler.html#acb620b4123f3aab21a754ffb73ec06d3", null ],
    [ "collectionLoop", "d2/dd2/classdd4hep_1_1EventHandler.html#a43f0dddc94976b2fd054d2d62389b8cb", null ],
    [ "collectionType", "d2/dd2/classdd4hep_1_1EventHandler.html#a53ac74cd9bd9b63360d96f83bc0f8985", null ],
    [ "data", "d2/dd2/classdd4hep_1_1EventHandler.html#ac48d73487b3fa88956fdd5d6fd16df52", null ],
    [ "datasourceName", "d2/dd2/classdd4hep_1_1EventHandler.html#ac6d83b037ab0696f83f41dd554aa2a09", null ],
    [ "GotoEvent", "d2/dd2/classdd4hep_1_1EventHandler.html#a8af5b377225b3ed136d4cfd254aedcc8", null ],
    [ "hasEvent", "d2/dd2/classdd4hep_1_1EventHandler.html#a4b81519dbb0224fced41610d1b817e4e", null ],
    [ "hasFile", "d2/dd2/classdd4hep_1_1EventHandler.html#ae8fd1d22bf8b6ac03357e3f1d75c6742", null ],
    [ "NextEvent", "d2/dd2/classdd4hep_1_1EventHandler.html#af58a5c1e1ed279c8d879c2784f0193c4", null ],
    [ "numEvents", "d2/dd2/classdd4hep_1_1EventHandler.html#a76c7a440089f2c2db4d8b20492f3a805", null ],
    [ "Open", "d2/dd2/classdd4hep_1_1EventHandler.html#ad022ee8124f65c9224ed61cc8b3e87ba", null ],
    [ "PreviousEvent", "d2/dd2/classdd4hep_1_1EventHandler.html#a92124e8626aab4a55f3d91cfdd3ec304", null ],
    [ "m_hasEvent", "d2/dd2/classdd4hep_1_1EventHandler.html#aa7620a6a4c68b88a46a850d1fec5968a", null ],
    [ "m_hasFile", "d2/dd2/classdd4hep_1_1EventHandler.html#a2b307fe8490d57d7647fc312279f81b2", null ]
];
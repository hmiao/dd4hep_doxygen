var Segmentation_8h =
[
    [ "dd4hep::DDSegmentation::Vector3D", "de/d55/structdd4hep_1_1DDSegmentation_1_1Vector3D.html", "de/d55/structdd4hep_1_1DDSegmentation_1_1Vector3D" ],
    [ "dd4hep::DDSegmentation::Segmentation", "d9/dec/classdd4hep_1_1DDSegmentation_1_1Segmentation.html", "d9/dec/classdd4hep_1_1DDSegmentation_1_1Segmentation" ],
    [ "CellID", "dc/d4a/Segmentation_8h.html#a3cb7ee897780154ee0a53835ae2935e8", null ],
    [ "DoubleParameter", "dc/d4a/Segmentation_8h.html#a5aafa3ea75399b643d97e8221e67c387", null ],
    [ "DoubleVecParameter", "dc/d4a/Segmentation_8h.html#a5d8b3939f1943d2893dc6f0f481d8396", null ],
    [ "FloatParameter", "dc/d4a/Segmentation_8h.html#ab74b4e693ca9394158879df42de15311", null ],
    [ "FloatVecParameter", "dc/d4a/Segmentation_8h.html#a865306ebcf1d3b3d2d72b459f224dfe6", null ],
    [ "IntParameter", "dc/d4a/Segmentation_8h.html#adf9859c200f37c3b9c287e323c8b096e", null ],
    [ "IntVecParameter", "dc/d4a/Segmentation_8h.html#abc057b6025bd44cd05eaebaf2cea85f4", null ],
    [ "StringParameter", "dc/d4a/Segmentation_8h.html#abbed9b480571c99eca8b547ae7b351f0", null ],
    [ "StringVecParameter", "dc/d4a/Segmentation_8h.html#a87ee9ed8a7667364dc0bc6a207d0e80a", null ],
    [ "UnitType", "dc/d4a/Segmentation_8h.html#ad1fc92146b1587af3a8b84c667b39c78", null ],
    [ "VolumeID", "dc/d4a/Segmentation_8h.html#a06a60baf700cf0080bfe42b610668342", null ]
];
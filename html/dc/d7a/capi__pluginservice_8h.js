var capi__pluginservice_8h =
[
    [ "cgaudi_pluginsvc_t", "d6/d63/structcgaudi__pluginsvc__t.html", "d6/d63/structcgaudi__pluginsvc__t" ],
    [ "cgaudi_factory_t", "db/d10/structcgaudi__factory__t.html", "db/d10/structcgaudi__factory__t" ],
    [ "cgaudi_property_t", "d5/de5/structcgaudi__property__t.html", "d5/de5/structcgaudi__property__t" ],
    [ "CGAUDI_API", "dc/d7a/capi__pluginservice_8h.html#a525c1b96ca9f2c717e503d458c45431b", null ],
    [ "CGAUDI_EXPORT", "dc/d7a/capi__pluginservice_8h.html#a5b5b33acdc65da643028826e53efe199", null ],
    [ "CGAUDI_IMPORT", "dc/d7a/capi__pluginservice_8h.html#aa786c0c9a6ae859e26d5c1f3c9b24c44", null ],
    [ "CGAUDI_LOCAL", "dc/d7a/capi__pluginservice_8h.html#a317746cff8f9c568b558f63051612bb6", null ],
    [ "cgaudi_factory_get_classname", "dc/d7a/capi__pluginservice_8h.html#adf3d83f2c00ef906e4fdd8af58058c71", null ],
    [ "cgaudi_factory_get_library", "dc/d7a/capi__pluginservice_8h.html#ae68d1a0d2d9ac40ea2d35b09fa5e16cb", null ],
    [ "cgaudi_factory_get_property_at", "dc/d7a/capi__pluginservice_8h.html#a8eeae13c86df96e14f854f4c97399594", null ],
    [ "cgaudi_factory_get_property_size", "dc/d7a/capi__pluginservice_8h.html#a3bbc2cb78fc4681026f77ef2f5184dee", null ],
    [ "cgaudi_factory_get_type", "dc/d7a/capi__pluginservice_8h.html#ac706371f15812af2aa71f6ccc3623203", null ],
    [ "cgaudi_pluginsvc_get_factory_at", "dc/d7a/capi__pluginservice_8h.html#a05cfbc13772ff0eed30402de005d6f0d", null ],
    [ "cgaudi_pluginsvc_get_factory_size", "dc/d7a/capi__pluginservice_8h.html#a86456376aad5fef7f94362eab00356d1", null ],
    [ "cgaudi_pluginsvc_instance", "dc/d7a/capi__pluginservice_8h.html#a05cec74da334c00f0ac787975508359b", null ],
    [ "cgaudi_property_get_key", "dc/d7a/capi__pluginservice_8h.html#a92d63a05ba4fc3846002af6779c75db9", null ],
    [ "cgaudi_property_get_value", "dc/d7a/capi__pluginservice_8h.html#ac2b22fa9f20e11173f09e2c5264d3c27", null ]
];
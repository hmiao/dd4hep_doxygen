var structdd4hep_1_1DisplayConfiguration_1_1Calo3D =
[
    [ "dz", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html#aeb6a43b310167cd7962d9eaa5e44ac43", null ],
    [ "emax", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html#a40c4cf9e2f270442f7bc4a5679ebed04", null ],
    [ "rmin", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html#a127f3f0396683cad47095b1ca329a27e", null ],
    [ "threshold", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html#abcfe50545760310cedd0b29151620982", null ],
    [ "towerH", "dc/d9e/structdd4hep_1_1DisplayConfiguration_1_1Calo3D.html#a6850ba4b9f6b3fa7ca8f9d38261ecc1b", null ]
];
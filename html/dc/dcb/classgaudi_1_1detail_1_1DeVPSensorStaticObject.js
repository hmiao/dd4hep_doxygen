var classgaudi_1_1detail_1_1DeVPSensorStaticObject =
[
    [ "DE_CTORS_DEFAULT", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#adf79a2f63883890b38983819a6971943", null ],
    [ "initialize", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a6065196d689dcb66648db355fd6250f3", null ],
    [ "print", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a0927afcef53f2e453cc4352fe0f61e30", null ],
    [ "chipSize", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a363b721d7d289b58508bf112d5851605", null ],
    [ "interChipDist", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a99cc5759655a6ebbb09067338bcb501e", null ],
    [ "interChipPixelSize", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a1d6a2ace11d4440f566766696d3fe934", null ],
    [ "local_x", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a0817fbb9565fe762598b14a11f0e5d85", null ],
    [ "module", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a2316bb71c02db7f9226337550658d2cd", null ],
    [ "nChips", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a634e37cd53518995db59ba922e227eb6", null ],
    [ "nCols", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a612b316cab872fce160c5e6b6b089eab", null ],
    [ "nRows", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a0c626042390fd3fdca0aa98f9ccfc176", null ],
    [ "pixelSize", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a7e3eeeef688618b5f06191536577885f", null ],
    [ "sensorNumber", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a1e69e10fd14f62775cb3490d1e78e62d", null ],
    [ "sizeX", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a83a3d698a889962585d163129869219e", null ],
    [ "sizeY", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#af9d9177820aa0b0acf5dd6ae5f8aabb9", null ],
    [ "thickness", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a21ae5907ff2b00d205eeca98d380a86a", null ],
    [ "x_pitch", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#ad0a51b090969e614e50472d7d20f4de4", null ],
    [ "zpos", "dc/dcb/classgaudi_1_1detail_1_1DeVPSensorStaticObject.html#a24456a1b1602d49671c82d292f7055e5", null ]
];
var classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4 =
[
    [ "segmentation_xy", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#afabf9a97ad356bfa4c9d0b2a4fc57c99", null ],
    [ "x_f_offset", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#ad6bc288af4dcfef066779e9e5130df46", null ],
    [ "x_grid_size", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a4590584b6056b3d4773a551687283bd3", null ],
    [ "x_mask", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a3e4e4a3bccafa502dbd54183d808b1ed", null ],
    [ "x_offset", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a0780bb0a491caddbc61ed100c82a0e1f", null ],
    [ "y_f_offset", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a4cc48a5038584d894bef81feecfca016", null ],
    [ "y_grid_size", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#ae2bfa08efd3881c0d4805a2133aebf27", null ],
    [ "y_mask", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a578f72e2b085a485fc901c25c298060a", null ],
    [ "y_offset", "dc/d1c/classdd4hep_1_1digi_1_1segmentation__data_3_01CartesianGridXY_01_4.html#a8063bbf8bf97b31b337c52242f1dbada", null ]
];
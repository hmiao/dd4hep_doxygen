var structdd4hep_1_1rec_1_1ZPlanarStruct =
[
    [ "LayerLayout", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout" ],
    [ "angleStrip", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#a5771f86e116e2efa961b9199a8e6b5ef", null ],
    [ "gapShell", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#aec8871626375a26e1d94cebb5a4b3f1a", null ],
    [ "layers", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#afd7c118614a7900eb733d00ef9759675", null ],
    [ "lengthStrip", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#ac8b01c834199e6c53fb2067134ab4e26", null ],
    [ "pitchStrip", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#a7482f14ba273ae510a60005f190d4f05", null ],
    [ "rInnerShell", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#a3ad0c4a339e425a51c80fa2600f8ce09", null ],
    [ "rOuterShell", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#a0fc92006aec6995c927ffa3f95a4b8b5", null ],
    [ "widthStrip", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#aa23d34dbb5263ff33d4be1d455dfd4c9", null ],
    [ "zHalfShell", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html#a6dc2b7a11346e4b06078c1a3b26210ea", null ]
];
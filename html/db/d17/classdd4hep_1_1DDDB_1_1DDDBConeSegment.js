var classdd4hep_1_1DDDB_1_1DDDBConeSegment =
[
    [ "type", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#a63642d8835e0ab0ebba38215d87419ef", null ],
    [ "delta", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#aa882733ff283bbbf89432dd4ca6144e6", null ],
    [ "innerRadiusMZ", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#a52d49bd642a0a57cb66b7a2a1cfb2f20", null ],
    [ "innerRadiusPZ", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#a060258c2c6d8e3b1ca406c898018a8a0", null ],
    [ "outerRadiusMZ", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#a40d64ea987f8706612c6a02964314f01", null ],
    [ "outerRadiusPZ", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#aa08665e6769966d3b69a59963627d5ec", null ],
    [ "sizeZ", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#ae6a850cd04bd703a1719477e913b9371", null ],
    [ "start", "db/d17/classdd4hep_1_1DDDB_1_1DDDBConeSegment.html#aadcafb118f75b2a4a9feeb007e588c6e", null ]
];
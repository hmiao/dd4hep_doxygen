var classdd4hep_1_1MultipoleField =
[
    [ "MultipoleField", "db/d8c/classdd4hep_1_1MultipoleField.html#a66745189fa088d26122bbf841eb18f90", null ],
    [ "fieldComponents", "db/d8c/classdd4hep_1_1MultipoleField.html#a06782e6f0fbdfcf53394ee5dda83b052", null ],
    [ "B_z", "db/d8c/classdd4hep_1_1MultipoleField.html#af5bcb2046b3bfb4b5517b84733c76f89", null ],
    [ "coefficents", "db/d8c/classdd4hep_1_1MultipoleField.html#ab23ac24c6065b0361b5635e5f470b7be", null ],
    [ "skews", "db/d8c/classdd4hep_1_1MultipoleField.html#ad821fc5da059695af313b0544f479542", null ],
    [ "transform", "db/d8c/classdd4hep_1_1MultipoleField.html#ae3ba3db9610be89f05e361e153819e3e", null ],
    [ "volume", "db/d8c/classdd4hep_1_1MultipoleField.html#a2f0dbb5a752bed8b3f299922e2ed2f69", null ]
];
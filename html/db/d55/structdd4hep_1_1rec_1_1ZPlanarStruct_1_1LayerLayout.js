var structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout =
[
    [ "LayerLayout", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#abf181b68f9d4439bad8fbcb7a6ea5c66", null ],
    [ "distanceSensitive", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a1f29e1806f2efe66f26a4f73de2ea31f", null ],
    [ "distanceSupport", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#aac54a4225080ed28c42931177b43ab28", null ],
    [ "ladderNumber", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a9a6902eefaa97cfc8e06653635677c14", null ],
    [ "lengthSensor", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#afc9d4df69e13fb943422d5575055c65a", null ],
    [ "offsetSensitive", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#ad13a6bf0421f9cf47bdfbb6753a4c19c", null ],
    [ "offsetSupport", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a399199b0075e5b4ac59fe1cede5e94b5", null ],
    [ "phi0", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a72a8e65787cb3605292860d2bcec3545", null ],
    [ "sensorsPerLadder", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#ad55482552036f553f044aace0a3c2645", null ],
    [ "thicknessSensitive", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a653a314be99635df0b704237d9bb9763", null ],
    [ "thicknessSupport", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#aa7d0e936d83a959bd7919ce2557b5572", null ],
    [ "widthSensitive", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a65980409443e55187c809b5daa497ded", null ],
    [ "widthSupport", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a327f1f0c09164d63a94eb4fa3af5da15", null ],
    [ "zHalfSensitive", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a46fd9aa6067e0bcabf1278e3ee0cdd43", null ],
    [ "zHalfSupport", "db/d55/structdd4hep_1_1rec_1_1ZPlanarStruct_1_1LayerLayout.html#a28e5f851a39ac1b75fc5ccc86a493b8b", null ]
];
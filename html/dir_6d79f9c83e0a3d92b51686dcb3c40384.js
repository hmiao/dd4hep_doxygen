var dir_6d79f9c83e0a3d92b51686dcb3c40384 =
[
    [ "CLIC_G4Gun.py", "d2/db6/CLICSiD_2scripts_2CLIC__G4Gun_8py.html", "d2/db6/CLICSiD_2scripts_2CLIC__G4Gun_8py" ],
    [ "CLIC_G4hepmc.py", "dc/d0d/CLICSiD_2scripts_2CLIC__G4hepmc_8py.html", "dc/d0d/CLICSiD_2scripts_2CLIC__G4hepmc_8py" ],
    [ "CLIC_GDML.py", "d1/d13/CLICSiD_2scripts_2CLIC__GDML_8py.html", "d1/d13/CLICSiD_2scripts_2CLIC__GDML_8py" ],
    [ "CLICMagField.py", "d9/d68/CLICSiD_2scripts_2CLICMagField_8py.html", "d9/d68/CLICSiD_2scripts_2CLICMagField_8py" ],
    [ "CLICPhysics.py", "d6/d41/CLICSiD_2scripts_2CLICPhysics_8py.html", "d6/d41/CLICSiD_2scripts_2CLICPhysics_8py" ],
    [ "CLICRandom.py", "d9/d8b/CLICSiD_2scripts_2CLICRandom_8py.html", "d9/d8b/CLICSiD_2scripts_2CLICRandom_8py" ],
    [ "CLICSid.py", "dc/da4/CLICSiD_2scripts_2CLICSid_8py.html", "dc/da4/CLICSiD_2scripts_2CLICSid_8py" ],
    [ "CLICSiD_LoadROOTGeo.py", "da/d08/CLICSiD_2scripts_2CLICSiD__LoadROOTGeo_8py.html", "da/d08/CLICSiD_2scripts_2CLICSiD__LoadROOTGeo_8py" ],
    [ "CLICSiDScan.py", "df/d15/CLICSiD_2scripts_2CLICSiDScan_8py.html", "df/d15/CLICSiD_2scripts_2CLICSiDScan_8py" ],
    [ "SiD_ECAL_Parallel_Readout.py", "d2/d15/CLICSiD_2scripts_2SiD__ECAL__Parallel__Readout_8py.html", "d2/d15/CLICSiD_2scripts_2SiD__ECAL__Parallel__Readout_8py" ],
    [ "testDDPython.py", "d5/d23/CLICSiD_2scripts_2testDDPython_8py.html", "d5/d23/CLICSiD_2scripts_2testDDPython_8py" ]
];
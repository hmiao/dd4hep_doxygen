var classdd4hep_1_1SolenoidField =
[
    [ "SolenoidField", "dd/d4f/classdd4hep_1_1SolenoidField.html#a882732fe549f63858d40cca4c5133bf9", null ],
    [ "fieldComponents", "dd/d4f/classdd4hep_1_1SolenoidField.html#aef35ec6873e2227025d3b770bdbdc1e1", null ],
    [ "innerField", "dd/d4f/classdd4hep_1_1SolenoidField.html#a304bf00db6a9ac59eb508b8d6888379d", null ],
    [ "innerRadius", "dd/d4f/classdd4hep_1_1SolenoidField.html#a3c0251ac62640dd50ebcfe85530678d1", null ],
    [ "maxZ", "dd/d4f/classdd4hep_1_1SolenoidField.html#ab8ef48460e7a6c1b6caea90e2d57533c", null ],
    [ "minZ", "dd/d4f/classdd4hep_1_1SolenoidField.html#ae219036344f9a1d12614e260b680ce91", null ],
    [ "outerField", "dd/d4f/classdd4hep_1_1SolenoidField.html#aac256ef9842a0846cd3e7156aa788cd6", null ],
    [ "outerRadius", "dd/d4f/classdd4hep_1_1SolenoidField.html#a176a8e728c244464b27e2770fc501a29", null ]
];
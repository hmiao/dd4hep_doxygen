var classdd4hep_1_1DDDB_1_1DDDBPolygon =
[
    [ "type", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#a69c1768c5fde134a285445560cf31558", null ],
    [ "innerRadius", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#a5f7c9b824e5b35e348425fd70a17e607", null ],
    [ "nsides", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#ae5e9ed065733f8a5150d31fb36b6eca7", null ],
    [ "outerRadius", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#a456beced310a6e3657b61d9fd068ef4b", null ],
    [ "start", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#a79f72642f5e749da64c6e349cd614683", null ],
    [ "z", "dd/d65/classdd4hep_1_1DDDB_1_1DDDBPolygon.html#a9c5106a81c3305fa930099043faba829", null ]
];
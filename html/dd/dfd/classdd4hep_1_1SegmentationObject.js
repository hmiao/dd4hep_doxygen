var classdd4hep_1_1SegmentationObject =
[
    [ "SegmentationObject", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a8d83b24a6e4e8ea3e5c20d652e4a0298", null ],
    [ "~SegmentationObject", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ad2db3a9de2c3add55caf0c549cf93892", null ],
    [ "cellID", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a45ef8e9a429b44b45ec7a50cc0da3817", null ],
    [ "decoder", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ab41343b9bad5c39d48d88c50d930c958", null ],
    [ "description", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a930896247114bce26d42b6966e2f8ac5", null ],
    [ "fieldDescription", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a1f40d9e8f8024eee33a7b7a29509f9c7", null ],
    [ "name", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ae1e4b47501b2e0c27c7299abc19467b3", null ],
    [ "neighbours", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a68d868abddf1dc8116962b8ae6e40a26", null ],
    [ "parameter", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ad6e31ca119a9d3aa76ce0913df95bde6", null ],
    [ "parameters", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a069e75500af3cc3c6cdb39bd31b0b1e4", null ],
    [ "position", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a8b541e7ca32f66c59bcda9fd7d9c461f", null ],
    [ "setDecoder", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a74dfd659d2de7a45182fed4654695bb9", null ],
    [ "setName", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a2a00a3a453dd06de14f7341f8fb89aea", null ],
    [ "setParameters", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ae0141c4f0e5e7993cebc167aa02de2c8", null ],
    [ "type", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a1216616c2dce1fed63142d7ec139aa18", null ],
    [ "volumeID", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a0a89da9aa8101caea4a069e4cfb2ab6f", null ],
    [ "detector", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a7ed8ddf201a7dbe92166d96ee7011f4f", null ],
    [ "magic", "dd/dfd/classdd4hep_1_1SegmentationObject.html#ac77816691217b3866123457c38b73e32", null ],
    [ "segmentation", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a6e9ace317bdbac2f1c41f00d63c77444", null ],
    [ "sensitive", "dd/dfd/classdd4hep_1_1SegmentationObject.html#a03048ce99e432b8ced82e00bc9fb76c8", null ],
    [ "useForHitPosition", "dd/dfd/classdd4hep_1_1SegmentationObject.html#af6339fbb84ed26aca6dc598a74acd7cf", null ]
];
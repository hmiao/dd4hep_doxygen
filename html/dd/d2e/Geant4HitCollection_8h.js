var Geant4HitCollection_8h =
[
    [ "dd4hep::sim::Geant4HitWrapper::InvalidHit", "d2/dba/classdd4hep_1_1sim_1_1Geant4HitWrapper_1_1InvalidHit.html", "d2/dba/classdd4hep_1_1sim_1_1Geant4HitWrapper_1_1InvalidHit" ],
    [ "dd4hep::sim::Geant4HitCollection::CollectionFlags", "d8/dc6/uniondd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags.html", "d8/dc6/uniondd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags" ],
    [ "dd4hep::sim::Geant4HitCollection::CollectionFlags::BitItems", "de/da8/structdd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags_1_1BitItems.html", "de/da8/structdd4hep_1_1sim_1_1Geant4HitCollection_1_1CollectionFlags_1_1BitItems" ]
];
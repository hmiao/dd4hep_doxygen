var group__Plugins =
[
    [ "Geant4Action", "df/dcc/group__Geant4Action.html", "df/dcc/group__Geant4Action" ],
    [ "Geant4GeneratorAction", "d9/d61/group__Geant4GeneratorAction.html", "d9/d61/group__Geant4GeneratorAction" ],
    [ "Geant4RunActions", "da/d0c/group__Geant4RunActions.html", "da/d0c/group__Geant4RunActions" ],
    [ "Geant4EventReader", "dd/dde/group__Geant4EventReader.html", "dd/dde/group__Geant4EventReader" ],
    [ "Geant4SDActionPlugin", "d8/d55/group__Geant4SDActionPlugin.html", "d8/d55/group__Geant4SDActionPlugin" ],
    [ "Geant4PhysicsConstructor", "d3/d52/group__Geant4PhysicsConstructor.html", "d3/d52/group__Geant4PhysicsConstructor" ],
    [ "SurfacePlugin", "d0/dac/group__SurfacePlugin.html", "d0/dac/group__SurfacePlugin" ]
];
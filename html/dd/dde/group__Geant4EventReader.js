var group__Geant4EventReader =
[
    [ "HepMC3FileReader", "d3/d0c/namespaceHepMC3FileReader.html", null ],
    [ "LCIOFileReader", "d7/d2c/namespaceLCIOFileReader.html", null ],
    [ "LCIOStdHepReader", "d0/de2/namespaceLCIOStdHepReader.html", null ],
    [ "Geant4EventReaderGuineaPig", "d6/db9/namespaceGeant4EventReaderGuineaPig.html", null ],
    [ "Geant4EventReaderHepEvt", "d2/d66/namespaceGeant4EventReaderHepEvt.html", null ],
    [ "Geant4EventReaderHepMC", "d4/d90/namespaceGeant4EventReaderHepMC.html", null ]
];
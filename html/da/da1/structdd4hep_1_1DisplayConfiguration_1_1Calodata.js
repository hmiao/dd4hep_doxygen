var structdd4hep_1_1DisplayConfiguration_1_1Calodata =
[
    [ "dz", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a165ae346fc6a60e39ed78f07e2c197e7", null ],
    [ "emax", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a74be1bff153d0200127207e535adf8d2", null ],
    [ "eta_max", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#aca5b348d191fcb2120d6f64189c6a98e", null ],
    [ "eta_min", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#ad30c3e59400a085112be41b71e6d116e", null ],
    [ "n_eta", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a207d10a9ac0ecd2c7ec4d96786d5b198", null ],
    [ "n_phi", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a09309e50769638b7719c4f4518e2953d", null ],
    [ "phi_max", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a633cc4651ac8a3dfa814774063a11bef", null ],
    [ "phi_min", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a8c884419ab0ea0cc894fb6f5a8e3023f", null ],
    [ "rmin", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#af5123b62b683ba7b239783c1ebf1c6ca", null ],
    [ "spare", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#acb306f471ca62d4c67bb7d1b18313443", null ],
    [ "threshold", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#a4e9dd431846f17b04fe8ae2bf7f47c4f", null ],
    [ "towerH", "da/da1/structdd4hep_1_1DisplayConfiguration_1_1Calodata.html#af1710d8ee7aa3eac4a6f1b70d7364712", null ]
];
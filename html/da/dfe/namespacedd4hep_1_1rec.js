var namespacedd4hep_1_1rec =
[
    [ "CellIDPositionConverter", "d0/d77/classdd4hep_1_1rec_1_1CellIDPositionConverter.html", "d0/d77/classdd4hep_1_1rec_1_1CellIDPositionConverter" ],
    [ "ConeSurface", "dd/df5/classdd4hep_1_1rec_1_1ConeSurface.html", "dd/df5/classdd4hep_1_1rec_1_1ConeSurface" ],
    [ "ConicalSupportStruct", "da/dd3/structdd4hep_1_1rec_1_1ConicalSupportStruct.html", "da/dd3/structdd4hep_1_1rec_1_1ConicalSupportStruct" ],
    [ "CylinderSurface", "d0/db0/classdd4hep_1_1rec_1_1CylinderSurface.html", "d0/db0/classdd4hep_1_1rec_1_1CylinderSurface" ],
    [ "DetectorSurfaces", "d1/d99/classdd4hep_1_1rec_1_1DetectorSurfaces.html", "d1/d99/classdd4hep_1_1rec_1_1DetectorSurfaces" ],
    [ "FixedPadSizeTPCStruct", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct" ],
    [ "GearHandle", "d4/d47/classdd4hep_1_1rec_1_1GearHandle.html", "d4/d47/classdd4hep_1_1rec_1_1GearHandle" ],
    [ "ICone", "dd/ddd/classdd4hep_1_1rec_1_1ICone.html", "dd/ddd/classdd4hep_1_1rec_1_1ICone" ],
    [ "ICylinder", "d3/d8f/classdd4hep_1_1rec_1_1ICylinder.html", "d3/d8f/classdd4hep_1_1rec_1_1ICylinder" ],
    [ "IMaterial", "d8/dc5/classdd4hep_1_1rec_1_1IMaterial.html", "d8/dc5/classdd4hep_1_1rec_1_1IMaterial" ],
    [ "ISurface", "d4/d5f/classdd4hep_1_1rec_1_1ISurface.html", "d4/d5f/classdd4hep_1_1rec_1_1ISurface" ],
    [ "LayeredCalorimeterStruct", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct" ],
    [ "MapStringDoubleStruct", "d3/d78/structdd4hep_1_1rec_1_1MapStringDoubleStruct.html", "d3/d78/structdd4hep_1_1rec_1_1MapStringDoubleStruct" ],
    [ "MaterialData", "d7/d96/classdd4hep_1_1rec_1_1MaterialData.html", "d7/d96/classdd4hep_1_1rec_1_1MaterialData" ],
    [ "MaterialManager", "d2/d78/classdd4hep_1_1rec_1_1MaterialManager.html", "d2/d78/classdd4hep_1_1rec_1_1MaterialManager" ],
    [ "MaterialScan", "d2/d21/classdd4hep_1_1rec_1_1MaterialScan.html", "d2/d21/classdd4hep_1_1rec_1_1MaterialScan" ],
    [ "NeighbourSurfacesStruct", "d9/d43/structdd4hep_1_1rec_1_1NeighbourSurfacesStruct.html", "d9/d43/structdd4hep_1_1rec_1_1NeighbourSurfacesStruct" ],
    [ "StructExtension", "dd/dcc/structdd4hep_1_1rec_1_1StructExtension.html", "dd/dcc/structdd4hep_1_1rec_1_1StructExtension" ],
    [ "Surface", "d6/d86/classdd4hep_1_1rec_1_1Surface.html", "d6/d86/classdd4hep_1_1rec_1_1Surface" ],
    [ "SurfaceHelper", "df/d02/classdd4hep_1_1rec_1_1SurfaceHelper.html", "df/d02/classdd4hep_1_1rec_1_1SurfaceHelper" ],
    [ "SurfaceList", "d2/d29/classdd4hep_1_1rec_1_1SurfaceList.html", "d2/d29/classdd4hep_1_1rec_1_1SurfaceList" ],
    [ "SurfaceManager", "d4/d03/classdd4hep_1_1rec_1_1SurfaceManager.html", "d4/d03/classdd4hep_1_1rec_1_1SurfaceManager" ],
    [ "SurfaceType", "d9/d8e/classdd4hep_1_1rec_1_1SurfaceType.html", "d9/d8e/classdd4hep_1_1rec_1_1SurfaceType" ],
    [ "Vector2D", "d3/d2c/classdd4hep_1_1rec_1_1Vector2D.html", "d3/d2c/classdd4hep_1_1rec_1_1Vector2D" ],
    [ "Vector3D", "d9/d83/classdd4hep_1_1rec_1_1Vector3D.html", "d9/d83/classdd4hep_1_1rec_1_1Vector3D" ],
    [ "VolCone", "d0/d3f/classdd4hep_1_1rec_1_1VolCone.html", "d0/d3f/classdd4hep_1_1rec_1_1VolCone" ],
    [ "VolConeImpl", "d4/da3/classdd4hep_1_1rec_1_1VolConeImpl.html", "d4/da3/classdd4hep_1_1rec_1_1VolConeImpl" ],
    [ "VolCylinder", "dc/d6e/classdd4hep_1_1rec_1_1VolCylinder.html", "dc/d6e/classdd4hep_1_1rec_1_1VolCylinder" ],
    [ "VolCylinderImpl", "d2/d1b/classdd4hep_1_1rec_1_1VolCylinderImpl.html", "d2/d1b/classdd4hep_1_1rec_1_1VolCylinderImpl" ],
    [ "VolPlaneImpl", "d2/d92/classdd4hep_1_1rec_1_1VolPlaneImpl.html", "d2/d92/classdd4hep_1_1rec_1_1VolPlaneImpl" ],
    [ "VolSurface", "d4/d6f/classdd4hep_1_1rec_1_1VolSurface.html", "d4/d6f/classdd4hep_1_1rec_1_1VolSurface" ],
    [ "VolSurfaceBase", "d2/d8d/classdd4hep_1_1rec_1_1VolSurfaceBase.html", "d2/d8d/classdd4hep_1_1rec_1_1VolSurfaceBase" ],
    [ "VolSurfaceHandle", "d7/d55/classdd4hep_1_1rec_1_1VolSurfaceHandle.html", "d7/d55/classdd4hep_1_1rec_1_1VolSurfaceHandle" ],
    [ "VolSurfaceList", "d9/d30/structdd4hep_1_1rec_1_1VolSurfaceList.html", "d9/d30/structdd4hep_1_1rec_1_1VolSurfaceList" ],
    [ "ZDiskPetalsStruct", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct.html", "df/d65/structdd4hep_1_1rec_1_1ZDiskPetalsStruct" ],
    [ "ZPlanarStruct", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct.html", "db/d91/structdd4hep_1_1rec_1_1ZPlanarStruct" ],
    [ "CellID", "da/dfe/namespacedd4hep_1_1rec.html#a9f244cce73607eb0ee06dfc3e3713d82", null ],
    [ "ConicalSupportData", "da/dfe/namespacedd4hep_1_1rec.html#a5229d7257b44aeb2a79ffeb55308237c", null ],
    [ "DoubleParameters", "da/dfe/namespacedd4hep_1_1rec.html#a95b724723172c12a076288dbb0a13bf9", null ],
    [ "FixedPadSizeTPCData", "da/dfe/namespacedd4hep_1_1rec.html#a90031731f389c674bfdfc49cfe709074", null ],
    [ "LayeredCalorimeterData", "da/dfe/namespacedd4hep_1_1rec.html#a00d58bbe46dec55d1553d56363032c5e", null ],
    [ "long64", "da/dfe/namespacedd4hep_1_1rec.html#a2862fedfe9c5edc4a1afb45974a0b7ad", null ],
    [ "MaterialVec", "da/dfe/namespacedd4hep_1_1rec.html#ab1f61322731362af9f9e512aca1f8276", null ],
    [ "NeighbourSurfacesData", "da/dfe/namespacedd4hep_1_1rec.html#ac70b1817414916f82d5307df8455d732", null ],
    [ "PlacementVec", "da/dfe/namespacedd4hep_1_1rec.html#afab1afd1a53bf07adec4698f2e7391a9", null ],
    [ "SurfaceMap", "da/dfe/namespacedd4hep_1_1rec.html#a22ec9bf9ae8bf5a83f499ed709e5fb29", null ],
    [ "VolPlane", "da/dfe/namespacedd4hep_1_1rec.html#a5d145b40aa2889f86d731f4fe28922d0", null ],
    [ "VolumeID", "da/dfe/namespacedd4hep_1_1rec.html#a10354fc6c3b15887fb401d554bbdfbb7", null ],
    [ "ZDiskPetalsData", "da/dfe/namespacedd4hep_1_1rec.html#a68c9518457599494e41ac4239e3deee7", null ],
    [ "ZPlanarData", "da/dfe/namespacedd4hep_1_1rec.html#ad6520083e4e908480a67cea2766e1973", null ],
    [ "createGearForCLIC", "da/dfe/namespacedd4hep_1_1rec.html#a612e82be670851231ad7995350edd3f8", null ],
    [ "createGearForILD", "da/dfe/namespacedd4hep_1_1rec.html#a98bb7387937f62085a70b0f8ea460a60", null ],
    [ "createGearForSiD", "da/dfe/namespacedd4hep_1_1rec.html#ac3caab8703634252203464094d21d40e", null ],
    [ "createGearMgr", "da/dfe/namespacedd4hep_1_1rec.html#a0561bb836fa148bd2096d1d93cbe4130", null ],
    [ "createSurfaceManager", "da/dfe/namespacedd4hep_1_1rec.html#a8b5d074af231e798225c9a13b9f20bf6", null ],
    [ "findVolume", "da/dfe/namespacedd4hep_1_1rec.html#a7cd02031b03d3de0935853164fc4957e", null ],
    [ "operator*", "da/dfe/namespacedd4hep_1_1rec.html#ad028898c1a6e3a84102b5ce825e8cab3", null ],
    [ "operator*", "da/dfe/namespacedd4hep_1_1rec.html#abaf383353d029a495504a61c5aba4930", null ],
    [ "operator+", "da/dfe/namespacedd4hep_1_1rec.html#ab5a8ab2763adf5a29085bdbc0027d261", null ],
    [ "operator-", "da/dfe/namespacedd4hep_1_1rec.html#a71be1afe4e93dab8cd5461528a4975b3", null ],
    [ "operator-", "da/dfe/namespacedd4hep_1_1rec.html#a3ed1f4f42a41b120f6de7e2f67d53de5", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a57c0c0e19950973cd723176d6fae05f3", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a8e77480c1f0774de711b3360362ffe12", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a28f90445084bf938cefaaba0bde77be2", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#aed7b764146d1eb0fa061f10e3b0cf4ae", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#aa341213247ee8fb48754a255d8e42abb", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#aee1ee5435f8950dba94c8d7751711062", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a22d1900f6eeaa446bc29f07507d61fb8", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#af2c722901853f941cb208b2b3af2a0a7", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#ab8c0d552a6859a222bd90f795cdeb75e", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a2b29d60ceb24eb12c96876712b0c3898", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#ac2d52987645aae45355ba9e531e92e3b", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a62022be005c4f998a29f98f4c764416f", null ],
    [ "operator<<", "da/dfe/namespacedd4hep_1_1rec.html#a1f35a7e819c739a7292b01d05cb57b7f", null ],
    [ "operator==", "da/dfe/namespacedd4hep_1_1rec.html#a8cfff78f6d11a861a7f8dd0922e89a05", null ],
    [ "volSurfaceList", "da/dfe/namespacedd4hep_1_1rec.html#a452974b6836e82511a399cec696bb10d", null ]
];
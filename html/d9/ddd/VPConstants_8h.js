var VPConstants_8h =
[
    [ "UserFlags", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890de", [
      [ "LEFT", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890dea5801193c51c6a121a41d9cc1688ffd6e", null ],
      [ "RIGHT", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890deaba8bc508697ad07cebc14088a500ece6", null ],
      [ "MAIN", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890deaf8aebbf83620d31199d1b555fb779d65", null ],
      [ "SIDE", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890dea7e1963a70977b5007750f18119f8f7e7", null ],
      [ "SUPPORT", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890dea37856d5ced2a34b4fc472a490ecf876a", null ],
      [ "MODULE", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890deae877cc6f99d9832ff822683ee3d407ca", null ],
      [ "LADDER", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890deaf40727bc1d9a05487352d0cd04e0b139", null ],
      [ "SENSOR", "d9/ddd/VPConstants_8h.html#acbd4a68ae11add201ec57a3e656890dea41391a9e991337d61e52d03a5d847797", null ]
    ] ],
    [ "NChipsPerSensor", "d9/ddd/VPConstants_8h.html#af1ac425e29f4c638cde3f60f01b91748", null ],
    [ "NColumns", "d9/ddd/VPConstants_8h.html#a557e76fd380d977fe4d727396eebf6ba", null ],
    [ "NModules", "d9/ddd/VPConstants_8h.html#a93c052d82fbc4f970d702b2059c43e7c", null ],
    [ "NPixelsPerSensor", "d9/ddd/VPConstants_8h.html#ad06c0f28a70ac51e5e6d6c62ad59fcc4", null ],
    [ "NRows", "d9/ddd/VPConstants_8h.html#a6c01374f77d6541b6acc3906a83bd5ea", null ],
    [ "NSensorColumns", "d9/ddd/VPConstants_8h.html#aa29db755b1353944acee6e5b2ed53244", null ],
    [ "NSensors", "d9/ddd/VPConstants_8h.html#ac13973c2ad246c538a9323c70ee7f0d5", null ],
    [ "NSensorsPerModule", "d9/ddd/VPConstants_8h.html#ac2d5a0939788e944b2569b01f57244c8", null ],
    [ "Pitch", "d9/ddd/VPConstants_8h.html#a657261b60fdc567170ddaf044e0147df", null ]
];
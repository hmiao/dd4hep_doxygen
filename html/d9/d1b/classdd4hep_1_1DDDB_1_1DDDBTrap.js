var classdd4hep_1_1DDDB_1_1DDDBTrap =
[
    [ "type", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a2927267d439e89ac7e2841375ceae7f5", null ],
    [ "alpha1", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a1e02df706ddd16adfc497a93b82905ff", null ],
    [ "alpha2", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#ad05852f9ee40f3bd8fb4b8ba9b7bd9c7", null ],
    [ "bl1", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a759e0e5b74b19d0979f82ab26450f5ab", null ],
    [ "bl2", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#adde46379d97c34b57f79dac0214245eb", null ],
    [ "dz", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#ac6633bd8843e25e2f147c698b1d59a09", null ],
    [ "h1", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a9c2eadea1fce2bc10288840718757bcd", null ],
    [ "h2", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#aa366c9c2713cf278ae60d5b228583e84", null ],
    [ "phi", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a31573084d58d6b1372e386df265da97a", null ],
    [ "theta", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a565eafde904d96f2efb6de27aa6b00a7", null ],
    [ "tl1", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a4afc1319b614f424a8553beae85cbe9c", null ],
    [ "tl2", "d9/d1b/classdd4hep_1_1DDDB_1_1DDDBTrap.html#a59d40ea71cd66a2e4af634c554eac251", null ]
];
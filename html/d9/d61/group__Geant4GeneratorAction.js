var group__Geant4GeneratorAction =
[
    [ "Geant4GeneratorWrapper", "dd/d0b/namespaceGeant4GeneratorWrapper.html", null ],
    [ "Geant4InputAction", "de/dd6/namespaceGeant4InputAction.html", null ],
    [ "Geant4InteractionMerger", "d6/dee/namespaceGeant4InteractionMerger.html", null ],
    [ "Geant4InteractionVertexBoost", "d9/d04/namespaceGeant4InteractionVertexBoost.html", null ],
    [ "Geant4InteractionVertexSmear", "d0/d97/namespaceGeant4InteractionVertexSmear.html", null ],
    [ "Geant4ParticleGun", "dc/dae/namespaceGeant4ParticleGun.html", null ],
    [ "Geant4PrimaryHandler", "da/d22/namespaceGeant4PrimaryHandler.html", null ]
];
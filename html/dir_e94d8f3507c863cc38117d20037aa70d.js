var dir_e94d8f3507c863cc38117d20037aa70d =
[
    [ "gear", "dir_4fac22b14e92e8be663b7cad1fa10890.html", "dir_4fac22b14e92e8be663b7cad1fa10890" ],
    [ "plugins", "dir_0c692609d0f5e2594cddbf6431953574.html", "dir_0c692609d0f5e2594cddbf6431953574" ],
    [ "CellIDPositionConverter.cpp", "d0/def/CellIDPositionConverter_8cpp.html", null ],
    [ "convertToGear.cc", "dc/d1e/convertToGear_8cc.html", "dc/d1e/convertToGear_8cc" ],
    [ "DetectorData.cpp", "d3/d15/DDRec_2src_2DetectorData_8cpp.html", "d3/d15/DDRec_2src_2DetectorData_8cpp" ],
    [ "DetectorSurfaces.cpp", "d5/de9/DetectorSurfaces_8cpp.html", null ],
    [ "MaterialManager.cpp", "d6/ddb/MaterialManager_8cpp.html", "d6/ddb/MaterialManager_8cpp" ],
    [ "MaterialScan.cpp", "d3/d3a/MaterialScan_8cpp.html", null ],
    [ "RecDictionary.h", "d6/dcb/RecDictionary_8h.html", null ],
    [ "Surface.cpp", "d5/d24/Surface_8cpp.html", "d5/d24/Surface_8cpp" ],
    [ "SurfaceHelper.cpp", "d5/d7a/SurfaceHelper_8cpp.html", null ],
    [ "SurfaceManager.cpp", "d7/def/SurfaceManager_8cpp.html", null ]
];
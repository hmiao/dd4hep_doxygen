var classdd4hep_1_1DipoleField =
[
    [ "DipoleField", "d8/d58/classdd4hep_1_1DipoleField.html#a7a431d40e83bd52565fccf2ed507780e", null ],
    [ "fieldComponents", "d8/d58/classdd4hep_1_1DipoleField.html#afee57d23345cdd6a457475d8fe6ac4a7", null ],
    [ "coefficents", "d8/d58/classdd4hep_1_1DipoleField.html#a21add026c38588c23762bd9f5d45470d", null ],
    [ "rmax", "d8/d58/classdd4hep_1_1DipoleField.html#a623516741163be73377e364e9cbbf4fc", null ],
    [ "zmax", "d8/d58/classdd4hep_1_1DipoleField.html#a0bb8ba9af6c13bc78e01fb06d56088b1", null ],
    [ "zmin", "d8/d58/classdd4hep_1_1DipoleField.html#acf9730088355543b3c618c794cb5a246", null ]
];
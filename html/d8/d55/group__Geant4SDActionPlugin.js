var group__Geant4SDActionPlugin =
[
    [ "Geant4VoidSensitiveAction", "d4/d27/namespaceGeant4VoidSensitiveAction.html", null ],
    [ "Geant4TrackerAction", "d1/de7/namespaceGeant4TrackerAction.html", null ],
    [ "Geant4CalorimeterAction", "d1/d7f/namespaceGeant4CalorimeterAction.html", null ],
    [ "Geant4OpticalCalorimeterAction", "dc/db0/namespaceGeant4OpticalCalorimeterAction.html", null ],
    [ "Geant4TrackerCombineAction", "db/dfd/namespaceGeant4TrackerCombineAction.html", null ],
    [ "Geant4TrackerWeightedAction", "dd/d64/namespaceGeant4TrackerWeightedAction.html", null ],
    [ "MyTrackerSDAction", "d4/d9b/namespaceMyTrackerSDAction.html", null ]
];
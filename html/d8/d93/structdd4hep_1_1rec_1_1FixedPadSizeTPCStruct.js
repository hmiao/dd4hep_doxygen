var structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct =
[
    [ "driftLength", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a10c6a02edcfffdfb74a90854ba6894dd", null ],
    [ "innerWallThickness", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#abdd38bf03c7adc01a725be88bfca8dac", null ],
    [ "maxRow", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#aec1243027fafc35d7ff465c686bbf876", null ],
    [ "outerWallThickness", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a1aabc361b6494361a9ca0369fc8cae26", null ],
    [ "padGap", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a4230ca2aba4d2f2c697c059ec2c33d56", null ],
    [ "padHeight", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a544c17711f59cdbf3cfd54237bf27a78", null ],
    [ "padWidth", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#aca91cdc07c01689e0df0883d672b0460", null ],
    [ "rMax", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a997bec2e582ccfc49604ffd030f52bba", null ],
    [ "rMaxReadout", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#ad0960f76059008748f8b109d5ab9a68a", null ],
    [ "rMin", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a97d356f48df144cb8370e147a8ede2e2", null ],
    [ "rMinReadout", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a8795191a55faff0d98ab8e6fc88d35dc", null ],
    [ "zHalf", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#a3d2a5c6febe8284ab900ed1b6034f2ce", null ],
    [ "zMinReadout", "d8/d93/structdd4hep_1_1rec_1_1FixedPadSizeTPCStruct.html#ab6e2614e15224e8096c698d227f8042b", null ]
];
var classdd4hep_1_1DDDB_1_1DDDBCons =
[
    [ "type", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#ad90af4fe81404ef87871d403872a709c", null ],
    [ "innerRadiusMZ", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#ae9f959cac2d65a6fb8d5dc5600c8bc63", null ],
    [ "innerRadiusPZ", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#af045247b5fbd20704b0d855a0bff28d0", null ],
    [ "outerRadiusMZ", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#ab7d3199476d322a89b14896084ca9876", null ],
    [ "outerRadiusPZ", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#a2324e400f2def441ee038c52cf3a85e2", null ],
    [ "sizeZ", "d8/df3/classdd4hep_1_1DDDB_1_1DDDBCons.html#a202e580a54cb9a306a40bd2f305328a7", null ]
];
var Geant4Converter_8cpp =
[
    [ "TO_G4_FINISH", "d8/d17/Geant4Converter_8cpp.html#af72293557c7bb0c2e0930ac763804ea5", null ],
    [ "TO_G4_MODEL", "d8/d17/Geant4Converter_8cpp.html#a797fa127168ebd6a7c00a8fff4b3be97", null ],
    [ "TO_G4_TYPE", "d8/d17/Geant4Converter_8cpp.html#a4d21fd67312ce4ea8c4269b14f3590d3", null ],
    [ "geant4_surface_finish", "d8/d17/Geant4Converter_8cpp.html#a1159046d836e85bdd345d107bf4cc761", null ],
    [ "geant4_surface_model", "d8/d17/Geant4Converter_8cpp.html#a32da5febeb462dc90b24a0a57bdc9635", null ],
    [ "geant4_surface_type", "d8/d17/Geant4Converter_8cpp.html#ae28d749c9ecd0bdc3bab0e26ea8c850d", null ],
    [ "printSolid", "d8/d17/Geant4Converter_8cpp.html#ac5a01b817225c14295ef43363d867087", null ],
    [ "CM_2_MM", "d8/d17/Geant4Converter_8cpp.html#a56887ac533d34da735ec8e03d2269285", null ],
    [ "GEANT4_TAG_IGNORE", "d8/d17/Geant4Converter_8cpp.html#aac5673fd87a2d910d85a940d6d2fdec9", null ]
];
var namespacedd4hep_1_1xml =
[
    [ "cond", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html", [
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#aa2e017da210ac7f9474725cdd0e17e59", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#ad34066c5ac218411c6b2ca34e2f572b8", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#ad045507105f0b8e4cf9baa9c685e86df", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a99848ec772acbb2a4e66c8444c7d9580", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a0c7bf9ce5a4c3fffbcef6f38557f2f24", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#aa0d6d854c786501d863a2cb743b3026b", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a45eab5ca94ad7425aa3690668d281e88", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a88d2040b28e9128a76594854e155b9d5", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a7710760d27346b1da71df280b855b86f", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#ae58af380ed5acf4578fdfa0c0a3242c9", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a55699563c2b9c9dff48188af5697df31", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a484338a2eee90f828e966f58dd36f71b", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#ad1a47cd3aa9cf4329496b06e09eb048c", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a91503663149d5f93d7ed326dc8d0502c", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a567bcea9296dce57c57921641e68b1e9", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#acc776473c589815d50846694883acb9a", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#ae28d6aeb9ff09e5f3b1862b61ba76dd0", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a03a7da531e57350f36877332e7113933", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#af240d7b8016036927d3b776c75cfb79c", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#aec644efa8014f934ef83b82b3eacd344", null ],
      [ "UNICODE", "dd/dd4/namespacedd4hep_1_1xml_1_1cond.html#a7565214aabc6db2b3b60931523ad0cb6", null ]
    ] ],
    [ "tools", "d1/d8e/namespacedd4hep_1_1xml_1_1tools.html", "d1/d8e/namespacedd4hep_1_1xml_1_1tools" ],
    [ "Collection_t", "de/d4b/classdd4hep_1_1xml_1_1Collection__t.html", "de/d4b/classdd4hep_1_1xml_1_1Collection__t" ],
    [ "Document", "dc/d7a/classdd4hep_1_1xml_1_1Document.html", "dc/d7a/classdd4hep_1_1xml_1_1Document" ],
    [ "DocumentErrorHandler", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler.html", "df/dad/classdd4hep_1_1xml_1_1DocumentErrorHandler" ],
    [ "DocumentHandler", "d3/d25/classdd4hep_1_1xml_1_1DocumentHandler.html", "d3/d25/classdd4hep_1_1xml_1_1DocumentHandler" ],
    [ "DocumentHolder", "d3/df3/classdd4hep_1_1xml_1_1DocumentHolder.html", "d3/df3/classdd4hep_1_1xml_1_1DocumentHolder" ],
    [ "Element", "d3/dc6/classdd4hep_1_1xml_1_1Element.html", "d3/dc6/classdd4hep_1_1xml_1_1Element" ],
    [ "Handle_t", "d8/dc2/classdd4hep_1_1xml_1_1Handle__t.html", "d8/dc2/classdd4hep_1_1xml_1_1Handle__t" ],
    [ "LayeringCnv", "d7/dc1/classdd4hep_1_1xml_1_1LayeringCnv.html", "d7/dc1/classdd4hep_1_1xml_1_1LayeringCnv" ],
    [ "NodeList", "d0/d9f/classdd4hep_1_1xml_1_1NodeList.html", "d0/d9f/classdd4hep_1_1xml_1_1NodeList" ],
    [ "RefElement", "dc/d57/classdd4hep_1_1xml_1_1RefElement.html", "dc/d57/classdd4hep_1_1xml_1_1RefElement" ],
    [ "Strng_t", "d6/d78/classdd4hep_1_1xml_1_1Strng__t.html", "d6/d78/classdd4hep_1_1xml_1_1Strng__t" ],
    [ "Tag_t", "d8/d2c/classdd4hep_1_1xml_1_1Tag__t.html", "d8/d2c/classdd4hep_1_1xml_1_1Tag__t" ],
    [ "UriContextReader", "de/d26/classdd4hep_1_1xml_1_1UriContextReader.html", "de/d26/classdd4hep_1_1xml_1_1UriContextReader" ],
    [ "UriReader", "d4/dd6/classdd4hep_1_1xml_1_1UriReader.html", "d4/dd6/classdd4hep_1_1xml_1_1UriReader" ],
    [ "XmlString", "d1/dfa/classdd4hep_1_1xml_1_1XmlString.html", "d1/dfa/classdd4hep_1_1xml_1_1XmlString" ],
    [ "Attribute", "d0/d54/namespacedd4hep_1_1xml.html#ab3e19831a0e913db7162be30dd12c02e", null ],
    [ "cpXmlChar", "d0/d54/namespacedd4hep_1_1xml.html#ae9440c10374de8290fdfd57b10c463e1", null ],
    [ "CSTR", "d0/d54/namespacedd4hep_1_1xml.html#a1ed06ee1ca266532fb79942b1fae527a", null ],
    [ "XmlChar", "d0/d54/namespacedd4hep_1_1xml.html#a1582d291c09b59c8e7d878fb7e1bcc06", null ],
    [ "XmlException", "d0/d54/namespacedd4hep_1_1xml.html#a16db4f83986366ead12d1c52513da5b7", null ],
    [ "XmlSize_t", "d0/d54/namespacedd4hep_1_1xml.html#a5b593b2ffdd572f1e3a397f7c66127f4", null ],
    [ "_ptrToString", "d0/d2c/group__DD4HEP__XML.html#ga2ff62c05c9f898cd17f46d2ec300d388", null ],
    [ "_toBool", "d0/d2c/group__DD4HEP__XML.html#gab224ec0c4d74722d8013ce3a73c9fc91", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#ga1435af6dd230e4f5f51d930e376b243f", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#gaed60b00771eb819b53fb7aae8769dfda", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#gafdb5d2a3feb32f4fa0475f5f5166e5c7", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#ga8bf830cf94d59eb235302afcb4764ceb", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#gad3595979bc2fb64202d5fcfc0a7f3425", null ],
    [ "_toDictionary", "d0/d2c/group__DD4HEP__XML.html#gab5cbbc7f509d68be1b4fb0f096521515", null ],
    [ "_toDouble", "d0/d2c/group__DD4HEP__XML.html#gae8c25178c0b82c9f96c6e2ca67954672", null ],
    [ "_toFloat", "d0/d2c/group__DD4HEP__XML.html#gabe6e639c687b6b4829951867704bdb78", null ],
    [ "_toInt", "d0/d2c/group__DD4HEP__XML.html#ga417239b92ac6dd0a71c5982b84782c86", null ],
    [ "_toLong", "d0/d2c/group__DD4HEP__XML.html#gaa8b2841b247afd4ecba22519f4d1a61b", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga2b971f38ffca0696f1fb8a08c2edc5e1", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga2b39fd79e92b35d904e8d999e18c601d", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#gad9615c5e222a7399b1c0a76d2fd939a2", null ],
    [ "_toString", "d0/d54/namespacedd4hep_1_1xml.html#ad6b9d78a240672136ff9a36f522eb4a5", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga1bd6bdc3461aa38b8eba5f731016e91e", null ],
    [ "_toString", "d0/d54/namespacedd4hep_1_1xml.html#a12e03cc11daff267d062bf82971e40f7", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga950fd23bb6e3fd6a575fc5d0c708307b", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga5d795fced40f0607f7e1988d4791e016", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga0eb0bb73426941e1a7c1d6c6cb0e7be5", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga2c50b531704b95be08fbd450a4083a18", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#gac245d49051087e922b63676bd6c14c15", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga88474012b48cfa2b6e8168f409b02c1d", null ],
    [ "_toString", "d0/d2c/group__DD4HEP__XML.html#ga4639f6266eb3a83895e304f37ff72d07", null ],
    [ "_toUInt", "d0/d2c/group__DD4HEP__XML.html#gacb7f3f47633d2b25472840c4de1226ac", null ],
    [ "_toULong", "d0/d2c/group__DD4HEP__XML.html#ga1387961c4a2a334a62fb5a7a8b79204f", null ],
    [ "createPlacedEnvelope", "d0/d54/namespacedd4hep_1_1xml.html#a1a14be78dfbf08516ec29dedc72a458f", null ],
    [ "createShape", "d0/d54/namespacedd4hep_1_1xml.html#ada73f98673f52817bbc6a628ec652b21", null ],
    [ "createStdVolume", "d0/d54/namespacedd4hep_1_1xml.html#a286d3e66f05d30b4a6c73de839e9d95f", null ],
    [ "createTransformation", "d0/d54/namespacedd4hep_1_1xml.html#a3f261067f9a2b73792ccb0e622a9fc07", null ],
    [ "createVolume", "d0/d54/namespacedd4hep_1_1xml.html#a38912f83580c5636b107ee1d86b5fe06", null ],
    [ "dump_doc", "d0/d54/namespacedd4hep_1_1xml.html#a9f1978b51850fcf8aaf6ba616d824749", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#af7c2209f0357a4f417710d16c858a806", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#ab5530c6958298b25d2a4990afdacba62", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#a47c2b893760643d56a07d710efeb7dbf", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#a6f5c523c11041ea4328716a71fae9f86", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#aa477544261b6908e16378b2aec408b48", null ],
    [ "dump_tree", "d0/d54/namespacedd4hep_1_1xml.html#acd23796bb964bd6cb4fcbcbb8f64427e", null ],
    [ "dumpTree", "d0/d54/namespacedd4hep_1_1xml.html#aa7473258f0cf904d02580cd581bf2269", null ],
    [ "dumpTree", "d0/d54/namespacedd4hep_1_1xml.html#a0b4f98ed0515aba2ade09729b186cf4b", null ],
    [ "enableEnvironResolution", "d0/d54/namespacedd4hep_1_1xml.html#af03f50957d455857289b5a4652bffaf2", null ],
    [ "getEnviron", "d0/d54/namespacedd4hep_1_1xml.html#abcd91506046ed5370bb4a38f46b505f2", null ],
    [ "Handle_t::attr< std::string >", "d0/d54/namespacedd4hep_1_1xml.html#ae1a6697d40836e53fd17e529008833a9", null ],
    [ "Handle_t::attr< std::string >", "d0/d54/namespacedd4hep_1_1xml.html#a9f7ceef5b481014f539f8ed22076f3f4", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga54f0396e723a209b0ee3b083fdf3d431", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gafbbab0c20bd44fdc1a2d5cbde9fb8c2f", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gaef4ee05579eb687d6249e02ff2310523", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga15157c4cfcc96f4052e7e642773e7ed0", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga8762aa97bc32bfdb0ba0f23be069cf84", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gad064a2611e44a254a45469a9e79c58ae", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gaf2e1f89ee92ff302419b6db56a252b08", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gacd61c07c437327e87a3672c89de7979f", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga3c342e257e714c5fb4125ee58ff02e5e", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga0a32da4c9e1c36e392d52960d8e50602", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga5aa8b07f2554cfa3ae48f69adbe31f73", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga89c231e3ee39bce4786d250020b8cfba", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#ga315f1af91167ae5f4a2f65814151fd92", null ],
    [ "operator+", "d0/d2c/group__DD4HEP__XML.html#gadc69ed16a7db7d7e6a8ff87fde3a1455", null ],
    [ "operator==", "d0/d2c/group__DD4HEP__XML.html#gad9035ca4af465c00460dfd672113ba86", null ],
    [ "parse", "d0/d54/namespacedd4hep_1_1xml.html#acdbd7122dbe1d4158b74be95101b2b34", null ],
    [ "parse", "d0/d54/namespacedd4hep_1_1xml.html#afd1c12bf64f79464a556aec80434d812", null ],
    [ "parse", "d0/d54/namespacedd4hep_1_1xml.html#ac380311224c07d06a02832980fdfb1cb", null ],
    [ "parse", "d0/d54/namespacedd4hep_1_1xml.html#a5030d0bcdf4a99650a0794cfaf8990d5", null ],
    [ "parse_delta", "d0/d54/namespacedd4hep_1_1xml.html#a2bce69fbc6032e48e5d46443c8ff0c64", null ],
    [ "parse_mapping", "d0/d54/namespacedd4hep_1_1xml.html#ade3ef38be9d88f22ccad129b10d79ec9", null ],
    [ "parse_sequence", "d0/d54/namespacedd4hep_1_1xml.html#a6b37e81d0549363d748a495934307f11", null ],
    [ "setDetectorTypeFlag", "d0/d54/namespacedd4hep_1_1xml.html#ac4edc8641d0a286a50e99aa2faf065b2", null ],
    [ "setXMLParserDebug", "d0/d54/namespacedd4hep_1_1xml.html#ad6b88841049d6ba1ce0d8760e7adbc11", null ],
    [ "tags_init", "d0/d54/namespacedd4hep_1_1xml.html#a6c9083a1e0f0101d8cd3f37b43699c69", null ],
    [ "Unicode_empty", "d0/d54/namespacedd4hep_1_1xml.html#a595a2008e4b1fcdd133d7dc06a9ed3b0", null ],
    [ "Unicode_NULL", "d0/d54/namespacedd4hep_1_1xml.html#a928b12e45211043e4057775f405e0cad", null ],
    [ "Unicode_PI", "d0/d54/namespacedd4hep_1_1xml.html#a873daf59a29822a5c46b3a7f8046a198", null ],
    [ "Unicode_star", "d0/d54/namespacedd4hep_1_1xml.html#a0e03fc726cfec0fb8897bc986e5aabf3", null ],
    [ "Unicode_TWOPI", "d0/d54/namespacedd4hep_1_1xml.html#a89c6b71738b8e0e1adee95d16f0d3501", null ]
];
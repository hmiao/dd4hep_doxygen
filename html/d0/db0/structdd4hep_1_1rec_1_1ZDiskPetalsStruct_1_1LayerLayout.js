var structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout =
[
    [ "LayerLayout", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a2617f1a953d195660d2c6d8a819864c7", null ],
    [ "alphaPetal", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a2085914f3d0e8a7742d87500398ae00f", null ],
    [ "distanceSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a387d76af8b3daf5bc14f61537d91cf54", null ],
    [ "distanceSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a10a6c5afb43fc894997d30296b344cb5", null ],
    [ "lengthSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a771b3c1d0e37e73ad329700893b53a24", null ],
    [ "lengthSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#ad469317d6693075851da2efcfb6a303c", null ],
    [ "petalHalfAngle", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a7afefe75ebd298772de23f2fdf9f9c62", null ],
    [ "petalNumber", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#ab8d07a05690886dd5f05b199892a15fb", null ],
    [ "phi0", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a42d69de246cf6f2a9fe1e13db107699c", null ],
    [ "sensorsPerPetal", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#af28885fe0ffdf5aafe4cd88b0b5fe775", null ],
    [ "thicknessSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a594bedc8ae3fba940d02ee048851fe45", null ],
    [ "thicknessSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a03adf0e2dc57a48b9e271aa88f8ed907", null ],
    [ "typeFlags", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a549c205ce3b89d0e0e819b797d247e34", null ],
    [ "widthInnerSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a59a1aacafa1672bfea9d68f8f1abcc74", null ],
    [ "widthInnerSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a78869a5f673820323afdeae4cf7c714d", null ],
    [ "widthOuterSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a32eb6795fef824c77b11403e595f8dd0", null ],
    [ "widthOuterSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a9c1c2a410c1035e736aa773122ca58d3", null ],
    [ "zOffsetSensitive", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a3b329f12f2731da9c020b50890de01d2", null ],
    [ "zOffsetSupport", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a5bd69b142bfe76d2b20b8876881643ff", null ],
    [ "zPosition", "d0/db0/structdd4hep_1_1rec_1_1ZDiskPetalsStruct_1_1LayerLayout.html#a641d35a5f1415aa95da97cdf95a5b84b", null ]
];
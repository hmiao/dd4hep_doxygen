var structdd4hep_1_1rec_1_1LayeredCalorimeterStruct =
[
    [ "Layer", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer" ],
    [ "LayoutType", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a3cde4caec94cd51630fb0e88e61969b2", [
      [ "BarrelLayout", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a3cde4caec94cd51630fb0e88e61969b2a0bd28304134109b7b64039a427fec49a", null ],
      [ "EndcapLayout", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a3cde4caec94cd51630fb0e88e61969b2a0d2179fbd571a90d9be037a5fc1b916d", null ],
      [ "ConicalLayout", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a3cde4caec94cd51630fb0e88e61969b2a466ba05ee95bc40107a40987c1e8386c", null ]
    ] ],
    [ "extent", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a13bccedc7cf2aecb121fcbbf5c861d13", null ],
    [ "gap0", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#aedd1b481fd6780fd9f80ba26bff16831", null ],
    [ "gap1", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a9105d3b1883168c67ecb16b068ba53cd", null ],
    [ "gap2", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#acea9ab018ae2df842fada441669c0e9a", null ],
    [ "inner_phi0", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a12d7cdbb1118aae98c106f18616e2249", null ],
    [ "inner_symmetry", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a06245a04028200ff57d22f9ec3b7125c", null ],
    [ "layers", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#af68d0a27e03e5862338c071cc77c7ea1", null ],
    [ "layoutType", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a69738c2a591afba8076d37a526475ad8", null ],
    [ "outer_phi0", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a7c59a94e7087d517d7d38c9bdecebde7", null ],
    [ "outer_symmetry", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#aac5d356df12a8c1feacd08dd37c5aa92", null ],
    [ "phi0", "d6/d34/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct.html#a1e5410fb56f033e27e4daf40a438f7fa", null ]
];
var structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer =
[
    [ "Layer", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#af003fef672f9e68563b4c5b7cfc7eeec", null ],
    [ "absorberThickness", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#ae11716e6dc737850579a509ba6f67a8c", null ],
    [ "cellSize0", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a5cbc832b9c28ff65576b3d4a657bdc1c", null ],
    [ "cellSize1", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a454252dddac2f025824f8b546f5da3cd", null ],
    [ "distance", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a7c032c71dd0c5c80c5114c889ede55c2", null ],
    [ "inner_nInteractionLengths", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a2e5579f8fe4cbb338f8a6d06ab58a622", null ],
    [ "inner_nRadiationLengths", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a4d21e457991f9ad0b49df03c986ba006", null ],
    [ "inner_thickness", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a1b0568da76d3cd25b2fcda3a8ef2e832", null ],
    [ "outer_nInteractionLengths", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a10e2cd2f3540bc6126563308c9bbe151", null ],
    [ "outer_nRadiationLengths", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#af4189aad43225ae349f921494e50cdeb", null ],
    [ "outer_thickness", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a1b968f4236e9b5324228f49e6b9ac36a", null ],
    [ "phi0", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a416ef7b241ba5926799b019b903bf38c", null ],
    [ "sensitive_thickness", "d6/dd5/structdd4hep_1_1rec_1_1LayeredCalorimeterStruct_1_1Layer.html#a2c249f62c3bf4c14961ac704c2c811cc", null ]
];
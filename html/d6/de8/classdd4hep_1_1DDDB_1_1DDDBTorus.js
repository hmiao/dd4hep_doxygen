var classdd4hep_1_1DDDB_1_1DDDBTorus =
[
    [ "type", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#ac7415e1a3c02b774159e4c01e5c30498", null ],
    [ "dphi", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#ac18ac9c1b3bc37ae9f28f49ccbda214f", null ],
    [ "phi", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#a63d9032612553d42d7d48605519e161c", null ],
    [ "r", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#ac09a6a8a27f1c6c72d4279e94fed8bba", null ],
    [ "rmax", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#aa57e2a9e4ae8d1198c78e805233bc2ea", null ],
    [ "rmin", "d6/de8/classdd4hep_1_1DDDB_1_1DDDBTorus.html#abe1117de7aa1a874a5fd896a4fc09dfa", null ]
];
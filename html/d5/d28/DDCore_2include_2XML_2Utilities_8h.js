var DDCore_2include_2XML_2Utilities_8h =
[
    [ "createPlacedEnvelope", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#a1a14be78dfbf08516ec29dedc72a458f", null ],
    [ "createShape", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#ada73f98673f52817bbc6a628ec652b21", null ],
    [ "createStdVolume", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#a286d3e66f05d30b4a6c73de839e9d95f", null ],
    [ "createTransformation", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#a3f261067f9a2b73792ccb0e622a9fc07", null ],
    [ "createVolume", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#a38912f83580c5636b107ee1d86b5fe06", null ],
    [ "setDetectorTypeFlag", "d5/d28/DDCore_2include_2XML_2Utilities_8h.html#ac4edc8641d0a286a50e99aa2faf065b2", null ]
];
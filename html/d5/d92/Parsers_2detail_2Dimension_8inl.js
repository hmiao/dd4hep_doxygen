var Parsers_2detail_2Dimension_8inl =
[
    [ "XML_ATTR_ACCESSOR", "d5/d92/Parsers_2detail_2Dimension_8inl.html#accabcb0a0a37bcff066b1281c9814553", null ],
    [ "XML_ATTR_ACCESSOR_BOOL", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a8efa1be16815f59e7b049cab22c08a21", null ],
    [ "XML_ATTR_ACCESSOR_DEFAULT", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a5329c32158716b05939bad5ab5e572df", null ],
    [ "XML_ATTR_ACCESSOR_DOUBLE", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a3b2602f2e6cdf903b4d9acd047d61b12", null ],
    [ "XML_ATTR_ACCESSOR_INT", "d5/d92/Parsers_2detail_2Dimension_8inl.html#aa9f53a44681848d2aeda1c5cdac2efb8", null ],
    [ "XML_ATTR_NS_ACCESSOR", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a712f343791889ac7912b917b64ccb32a", null ],
    [ "XML_ATTR_NS_ACCESSOR_DEFAULT", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a621d9cfc07f37e663ecb8f6c5bd22dfa", null ],
    [ "XML_ATTR_NS_ACCESSOR_DOUBLE", "d5/d92/Parsers_2detail_2Dimension_8inl.html#a41ecea5c5f5c5ebc82e772c4dea4c97c", null ],
    [ "XML_ATTR_NS_ACCESSOR_INT", "d5/d92/Parsers_2detail_2Dimension_8inl.html#ad521d34a53e9f1ab96da2b2566438d95", null ],
    [ "XML_ATTR_NS_ACCESSOR_STRING", "d5/d92/Parsers_2detail_2Dimension_8inl.html#acd82de7a6fdd451bdc3f3d7665c66899", null ],
    [ "XML_CHILD_ACCESSOR_XML_DIM", "d5/d92/Parsers_2detail_2Dimension_8inl.html#af3b6c67a307b3febcce9072bfcfd4d5e", null ]
];
var classdd4hep_1_1STD__Conditions =
[
    [ "Conventions", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646f", [
      [ "STP", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646fa653dc13519198d04e2d84a65b288f5d0", null ],
      [ "NTP", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646fa60f06d7aca74fe14f32c2420c0170236", null ],
      [ "USER", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646fafa0bbc49fd09434fc1ee4c2896ee2b08", null ],
      [ "USER_SET", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646fa6f4357be89edf1a15411ce98f5e12283", null ],
      [ "USER_NOTIFIED", "d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646fa9ca3bc48f17851f943049c6bba861687", null ]
    ] ],
    [ "is_NTP", "d5/d37/classdd4hep_1_1STD__Conditions.html#a88d9af311a8bbc8df4454e4d12fa435e", null ],
    [ "is_STP", "d5/d37/classdd4hep_1_1STD__Conditions.html#adfc8b3a3053cc94cfdd8aaa04e30f425", null ],
    [ "is_user_defined", "d5/d37/classdd4hep_1_1STD__Conditions.html#ae7c0b21f999b351829110d362f6a47cc", null ],
    [ "convention", "d5/d37/classdd4hep_1_1STD__Conditions.html#ab98b233b94d54c4ad54286eb61edb3f8", null ],
    [ "pressure", "d5/d37/classdd4hep_1_1STD__Conditions.html#a0fdb641efd5ace27ef07393b2606a736", null ],
    [ "temperature", "d5/d37/classdd4hep_1_1STD__Conditions.html#aaebabf9cb7361ca5011a809ad4122333", null ]
];
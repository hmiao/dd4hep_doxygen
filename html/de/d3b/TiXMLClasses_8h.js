var TiXMLClasses_8h =
[
    [ "TiXmlAttribute", "d4/dc1/classTiXmlAttribute.html", "d4/dc1/classTiXmlAttribute" ],
    [ "TiXmlAttributeSet", "df/dda/classTiXmlAttributeSet.html", "df/dda/classTiXmlAttributeSet" ],
    [ "TiXmlBase", "d8/d47/classTiXmlBase.html", "d8/d47/classTiXmlBase" ],
    [ "TiXmlBase::Entity", "d8/d33/structTiXmlBase_1_1Entity.html", "d8/d33/structTiXmlBase_1_1Entity" ],
    [ "TiXmlComment", "de/d43/classTiXmlComment.html", "de/d43/classTiXmlComment" ],
    [ "TiXmlCursor", "d7/dfc/structTiXmlCursor.html", "d7/dfc/structTiXmlCursor" ],
    [ "TiXmlDeclaration", "d2/df2/classTiXmlDeclaration.html", "d2/df2/classTiXmlDeclaration" ],
    [ "TiXmlDocument", "df/d09/classTiXmlDocument.html", "df/d09/classTiXmlDocument" ],
    [ "TiXmlElement", "db/d59/classTiXmlElement.html", "db/d59/classTiXmlElement" ],
    [ "TiXmlHandle", "dd/d98/classTiXmlHandle.html", null ],
    [ "TiXmlHandle_t", "d5/d4e/classTiXmlHandle__t.html", "d5/d4e/classTiXmlHandle__t" ],
    [ "TiXmlNode", "d3/dd5/classTiXmlNode.html", "d3/dd5/classTiXmlNode" ],
    [ "TiXmlOutStream", "dc/d34/classTiXmlOutStream.html", "dc/d34/classTiXmlOutStream" ],
    [ "TiXmlParsingData", "d6/d7d/classTiXmlParsingData.html", "d6/d7d/classTiXmlParsingData" ],
    [ "TiXmlPrinter", "d4/d3c/classTiXmlPrinter.html", "d4/d3c/classTiXmlPrinter" ],
    [ "TiXmlString", "d1/d15/classTiXmlString.html", "d1/d15/classTiXmlString" ],
    [ "TiXmlString::Rep", "da/d0f/structTiXmlString_1_1Rep.html", "da/d0f/structTiXmlString_1_1Rep" ],
    [ "TiXmlText", "d4/d9a/classTiXmlText.html", "d4/d9a/classTiXmlText" ],
    [ "TiXmlUnknown", "d5/d04/classTiXmlUnknown.html", "d5/d04/classTiXmlUnknown" ],
    [ "TiXmlVisitor", "d9/d74/classTiXmlVisitor.html", "d9/d74/classTiXmlVisitor" ]
];
var classdd4hep_1_1DDSegmentation_1_1SegmentationParameter =
[
    [ "UnitType", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a804ab6386b83ba9da08e2caca65f95a8", [
      [ "NoUnit", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a804ab6386b83ba9da08e2caca65f95a8a65bbc8608613c0f82cca3dfe6fa6f147", null ],
      [ "LengthUnit", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a804ab6386b83ba9da08e2caca65f95a8a6c24e3c42296fc5b1a89193854438640", null ],
      [ "AngleUnit", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a804ab6386b83ba9da08e2caca65f95a8a1297b9f123feddee2842b4a77aab896f", null ]
    ] ],
    [ "~SegmentationParameter", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#aa7d4e670993f6335b80b50d99aa3f86a", null ],
    [ "SegmentationParameter", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a8a91367dea6089e483c2ffd6015db3f2", null ],
    [ "defaultValue", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a57777c93b747d32b02cb45cd197c95b7", null ],
    [ "description", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#ae381447baa49c10d4840ba43da02037a", null ],
    [ "isOptional", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#ad59ea5165878a7363aab696fc9bd5280", null ],
    [ "name", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a8b01325079a8832bf6d7353672704ae6", null ],
    [ "setValue", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a864791af6c6f76c0892318623b16ddc2", null ],
    [ "toString", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#add45f7968714372e30dcf13c5e87dcf5", null ],
    [ "type", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a945a72b63ff1193d5219a52b9c42e6a2", null ],
    [ "unitType", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a6054220d433b5b01e45e51cfe53d8604", null ],
    [ "value", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a54eed3841ccb6b3c3dd256575223c73c", null ],
    [ "_description", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a8ec5f498a06b99d98d27296e65d0b157", null ],
    [ "_isOptional", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#ac2197c57bfcfae073d8e79bc81181892", null ],
    [ "_name", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#aa5fbefd614c5d1aeacf1a64e59afc149", null ],
    [ "_unitType", "de/df4/classdd4hep_1_1DDSegmentation_1_1SegmentationParameter.html#a97b4780f817d7d1d66e085abd2a840fb", null ]
];
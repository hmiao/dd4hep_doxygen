var Geant4InputHandling_8h =
[
    [ "boostInteraction", "de/d32/Geant4InputHandling_8h.html#a1e509be8107d7e845e471be446eef0a1", null ],
    [ "createPrimary", "de/d32/Geant4InputHandling_8h.html#a040f8ce1a097dbf0eddd6b8944ed3728", null ],
    [ "createPrimary", "de/d32/Geant4InputHandling_8h.html#a50e9cc8cf213cb62b18ec0b285e10f7a", null ],
    [ "createPrimary", "de/d32/Geant4InputHandling_8h.html#a9a5cd6d8bab2fad5b4aa785f79688337", null ],
    [ "generatePrimaries", "de/d32/Geant4InputHandling_8h.html#a13f1e9968abe19228bfc26500f9ffe80", null ],
    [ "generationInitialization", "de/d32/Geant4InputHandling_8h.html#a25aba22b7b370c8af82c21e21986e5a3", null ],
    [ "mergeInteractions", "de/d32/Geant4InputHandling_8h.html#ac32960911db3e2102719bf8231586247", null ],
    [ "smearInteraction", "de/d32/Geant4InputHandling_8h.html#a206c0ff93c8cdb4b66b5f2a25cfa35fe", null ]
];
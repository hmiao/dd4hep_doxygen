var dir_853536ff2f303df2cd440a474099a72d =
[
    [ "CellIDPositionConverter.h", "d9/d02/CellIDPositionConverter_8h.html", "d9/d02/CellIDPositionConverter_8h" ],
    [ "DDGear.h", "d2/d1f/DDGear_8h.html", "d2/d1f/DDGear_8h" ],
    [ "DetectorData.h", "dc/d1c/DDRec_2include_2DDRec_2DetectorData_8h.html", "dc/d1c/DDRec_2include_2DDRec_2DetectorData_8h" ],
    [ "DetectorSurfaces.h", "d1/d0f/DetectorSurfaces_8h.html", "d1/d0f/DetectorSurfaces_8h" ],
    [ "IMaterial.h", "dd/d48/IMaterial_8h.html", "dd/d48/IMaterial_8h" ],
    [ "ISurface.h", "d8/d38/ISurface_8h.html", "d8/d38/ISurface_8h" ],
    [ "Material.h", "df/d12/Material_8h.html", "df/d12/Material_8h" ],
    [ "MaterialManager.h", "d0/d9a/MaterialManager_8h.html", "d0/d9a/MaterialManager_8h" ],
    [ "MaterialScan.h", "d4/dc9/MaterialScan_8h.html", "d4/dc9/MaterialScan_8h" ],
    [ "Surface.h", "da/d51/Surface_8h.html", "da/d51/Surface_8h" ],
    [ "SurfaceHelper.h", "d5/d5c/SurfaceHelper_8h.html", "d5/d5c/SurfaceHelper_8h" ],
    [ "SurfaceManager.h", "dc/d63/SurfaceManager_8h.html", "dc/d63/SurfaceManager_8h" ],
    [ "Vector2D.h", "d3/db0/Vector2D_8h.html", "d3/db0/Vector2D_8h" ],
    [ "Vector3D.h", "d5/dd9/Vector3D_8h.html", "d5/dd9/Vector3D_8h" ]
];
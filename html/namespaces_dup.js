var namespaces_dup =
[
    [ "align", null, [
      [ "AlignmentsCalib", null, [
        [ "Entry", "d4/d42/classAlignmentsCalib_1_1Entry.html", "d4/d42/classAlignmentsCalib_1_1Entry" ]
      ] ]
    ] ],
    [ "Assemblies", "dd/d66/namespaceAssemblies.html", [
      [ "run", "dd/d66/namespaceAssemblies.html#a8fae9a6571877a4db3195032cbcfe7d5", null ]
    ] ],
    [ "BoxSegment", "d5/ddd/namespaceBoxSegment.html", [
      [ "detector_BoxSegment", "d5/ddd/namespaceBoxSegment.html#a7cf89d010f2be0e25fda94e00eb21e5f", null ]
    ] ],
    [ "Check_Air", "dc/ddd/namespaceCheck__Air.html", [
      [ "help", "dc/ddd/namespaceCheck__Air.html#aeab800e3588b5e9995102b35436164cd", null ],
      [ "run", "dc/ddd/namespaceCheck__Air.html#a2eb3ed83fa4cb0676312c23ea66fc3c2", null ],
      [ "format", "dc/ddd/namespaceCheck__Air.html#a910b3e4127acc213318e382e565c4881", null ],
      [ "level", "dc/ddd/namespaceCheck__Air.html#af8c3340778a5c0a3aa6320600794a4d8", null ]
    ] ],
    [ "Check_reflection", "d0/da4/namespaceCheck__reflection.html", [
      [ "help", "d0/da4/namespaceCheck__reflection.html#a3a081c4b609020b7efe34c9a0bd9b287", null ],
      [ "run", "d0/da4/namespaceCheck__reflection.html#a78f58f9d3a2465b46a78fa8278b5f6d6", null ],
      [ "format", "d0/da4/namespaceCheck__reflection.html#a329f163c318968218a4b6316b2b9b3e2", null ],
      [ "level", "d0/da4/namespaceCheck__reflection.html#a2735b7f91a360faaa7dec5020798be14", null ],
      [ "logger", "d0/da4/namespaceCheck__reflection.html#a9e5b597ce3d6c89c51cbab921d5dc670", null ]
    ] ],
    [ "Check_shape", "d7/dbf/namespaceCheck__shape.html", [
      [ "help", "d7/dbf/namespaceCheck__shape.html#a88f6cf5ba52a2d833ce236a6674f2274", null ],
      [ "run", "d7/dbf/namespaceCheck__shape.html#a2a9318f5fadccd20816fb40e97cc7316", null ],
      [ "format", "d7/dbf/namespaceCheck__shape.html#a9db3c6aa1c08f7ac6ad474dbb80e3fad", null ],
      [ "level", "d7/dbf/namespaceCheck__shape.html#ace9c4606ee74443975fc6e909aba04fb", null ],
      [ "logger", "d7/dbf/namespaceCheck__shape.html#a63a6c00249b34e0c4230783d00efdbd6", null ]
    ] ],
    [ "checkGeometry", "dc/d77/namespacecheckGeometry.html", [
      [ "args", "dc/d77/namespacecheckGeometry.html#a94c2784cf3681cc226ff3efb34fc72ae", null ],
      [ "default", "dc/d77/namespacecheckGeometry.html#a2379032f54dbc7826e8ca393286df9a0", null ],
      [ "description", "dc/d77/namespacecheckGeometry.html#a2d452d1b445b7c6ee38ca107a4057435", null ],
      [ "dest", "dc/d77/namespacecheckGeometry.html#ae37df47758626cb485e682a78200cd90", null ],
      [ "format", "dc/d77/namespacecheckGeometry.html#a541d049dcc68dc879aa97516dc35be0a", null ],
      [ "help", "dc/d77/namespacecheckGeometry.html#af73d803a74a5865c49332f0dbe61bbf7", null ],
      [ "level", "dc/d77/namespacecheckGeometry.html#aefb4c4d010b04aea96bf1484c4c90d3c", null ],
      [ "logger", "dc/d77/namespacecheckGeometry.html#af13ba0d409983e37a58bdad271643dd4", null ],
      [ "metavar", "dc/d77/namespacecheckGeometry.html#a8c1dc5d5bc81ccf7ae03406ba8fd2cd4", null ],
      [ "num_tracks", "dc/d77/namespacecheckGeometry.html#a2f9b97a93c9ae5f5346ab07c0dad9a22", null ],
      [ "opts", "dc/d77/namespacecheckGeometry.html#a7ccf9328faa5f9e2724c13b79f09bf85", null ],
      [ "parser", "dc/d77/namespacecheckGeometry.html#a343ec2625bf7c9f79f22507294f77471", null ],
      [ "vx", "dc/d77/namespacecheckGeometry.html#a6d0a3625687ee33302f1cb3b9d055bf4", null ],
      [ "vy", "dc/d77/namespacecheckGeometry.html#a74e040ed8bb627cbd4b55b75681efccd", null ],
      [ "vz", "dc/d77/namespacecheckGeometry.html#aa7a8783161e8148b838ef97fa8d71e64", null ],
      [ "width", "dc/d77/namespacecheckGeometry.html#af654fe91047ce0feddc90290747b0d38", null ]
    ] ],
    [ "checkOverlaps", "db/d1c/namespacecheckOverlaps.html", [
      [ "action", "db/d1c/namespacecheckOverlaps.html#a58e8a186663d3faafc02e0193edeba6e", null ],
      [ "args", "db/d1c/namespacecheckOverlaps.html#a48295734e9e5696b21a6e167a82a42f7", null ],
      [ "default", "db/d1c/namespacecheckOverlaps.html#a4b564abd59b0e478305a7b2543fa4b4d", null ],
      [ "description", "db/d1c/namespacecheckOverlaps.html#a7167e12df7fd441af106a06f84a0916a", null ],
      [ "dest", "db/d1c/namespacecheckOverlaps.html#ae83b3adbb8e85f13f2c2aadf7bb46646", null ],
      [ "format", "db/d1c/namespacecheckOverlaps.html#a5ccf98bb221d14c553c8d6cecca2a691", null ],
      [ "help", "db/d1c/namespacecheckOverlaps.html#a99006c0aeae44763cf0b72650b35bf87", null ],
      [ "level", "db/d1c/namespacecheckOverlaps.html#ac18ad54df27f451260f0f5f0ca217a0d", null ],
      [ "logger", "db/d1c/namespacecheckOverlaps.html#a8a0cf10408f703b0dbf3e6a235be02a3", null ],
      [ "metavar", "db/d1c/namespacecheckOverlaps.html#a19b9b98cbacc7f417dbf158d5f52c39c", null ],
      [ "opts", "db/d1c/namespacecheckOverlaps.html#a90771057eeada414705f411bb19fc32e", null ],
      [ "parser", "db/d1c/namespacecheckOverlaps.html#a54607469bd60e66588db5d3e1d1c6a5f", null ],
      [ "tolerance", "db/d1c/namespacecheckOverlaps.html#aa2c9711a4066e1e35365aeb28ed89147", null ],
      [ "width", "db/d1c/namespacecheckOverlaps.html#a1d9fa6b490926f8248df8578ef910a40", null ]
    ] ],
    [ "CLHEP", "db/da0/namespaceCLHEP.html", [
      [ "crc32ul", "db/da0/namespaceCLHEP.html#a5222e5dfca56b9684ab59bea27e89405", null ]
    ] ],
    [ "CLIC_G4Gun", "d3/d2e/namespaceCLIC__G4Gun.html", [
      [ "run", "d3/d2e/namespaceCLIC__G4Gun.html#a78f6c65e4d2d4b1a2a38661091cf350f", null ],
      [ "format", "d3/d2e/namespaceCLIC__G4Gun.html#ad3bacb0d46d510ec17259bd97125d03b", null ],
      [ "level", "d3/d2e/namespaceCLIC__G4Gun.html#a0513e12315ae61263f0fafa8854a9c1a", null ],
      [ "logger", "d3/d2e/namespaceCLIC__G4Gun.html#ae3bd7931d7dc3247cc2f2ec5c0a160fa", null ]
    ] ],
    [ "CLIC_G4hepmc", "d4/dd4/namespaceCLIC__G4hepmc.html", [
      [ "run", "d4/dd4/namespaceCLIC__G4hepmc.html#a10de312a8c0cfe4c0cc48ab02d9aff41", null ],
      [ "format", "d4/dd4/namespaceCLIC__G4hepmc.html#a88268a3032857b07090943fc504d6805", null ],
      [ "level", "d4/dd4/namespaceCLIC__G4hepmc.html#af9c4c1bb0a516c9333daeedc9895f57a", null ],
      [ "logger", "d4/dd4/namespaceCLIC__G4hepmc.html#a85499db14d7c733bd75df28eef54c10c", null ]
    ] ],
    [ "CLIC_GDML", "da/d6d/namespaceCLIC__GDML.html", [
      [ "run", "da/d6d/namespaceCLIC__GDML.html#a7e070d2ccc2657ae96de949d01c1e219", null ]
    ] ],
    [ "CLICMagField", "d8/d44/namespaceCLICMagField.html", [
      [ "quiet", "d8/d44/namespaceCLICMagField.html#a2980531954d619b0f515941c65fc18fa", null ],
      [ "sid", "d8/d44/namespaceCLICMagField.html#a3bb025c22ce09a5e0dd729df544dabd3", null ]
    ] ],
    [ "CLICPhysics", "df/d49/namespaceCLICPhysics.html", [
      [ "sid", "df/d49/namespaceCLICPhysics.html#a71dac23bf8ec8f95c8759c1d32b6f1d4", null ]
    ] ],
    [ "CLICRandom", "da/d95/namespaceCLICRandom.html", [
      [ "format", "da/d95/namespaceCLICRandom.html#a4ea853a985a18a7fddaa6405abeda3eb", null ],
      [ "have_geo", "da/d95/namespaceCLICRandom.html#a53813538475ab126d1cc853e0a40715f", null ],
      [ "level", "da/d95/namespaceCLICRandom.html#a812bdd3b955b12e44b50888d2c3666dc", null ],
      [ "logger", "da/d95/namespaceCLICRandom.html#ae102f8370eb70a0853e844dbc65f784e", null ],
      [ "rndm", "da/d95/namespaceCLICRandom.html#a380a7ba5c4e5c5aeb84922766c6a2bbb", null ],
      [ "rndm1", "da/d95/namespaceCLICRandom.html#aaadbac5943ac88c875168757ffde4120", null ],
      [ "rndm2", "da/d95/namespaceCLICRandom.html#a49cb4d37676e49c9f572273011f6d9d4", null ],
      [ "sid", "da/d95/namespaceCLICRandom.html#a39a8ff72c1fbaf1d200edfac3879ccaf", null ]
    ] ],
    [ "CLICSid", "db/da3/namespaceCLICSid.html", "db/da3/namespaceCLICSid" ],
    [ "CLICSiD_LoadROOTGeo", "dd/d16/namespaceCLICSiD__LoadROOTGeo.html", [
      [ "run", "dd/d16/namespaceCLICSiD__LoadROOTGeo.html#abced31cc700012159a4f43ef0808049d", null ],
      [ "format", "dd/d16/namespaceCLICSiD__LoadROOTGeo.html#a7fd758a817fde0ec321a5ae62ce86c08", null ],
      [ "level", "dd/d16/namespaceCLICSiD__LoadROOTGeo.html#a39699a4a9914bacb0c5e434c28761b9a", null ],
      [ "logger", "dd/d16/namespaceCLICSiD__LoadROOTGeo.html#ac20c07a585c666777552b1927dfb6458", null ]
    ] ],
    [ "CLICSiDScan", "da/d8f/namespaceCLICSiDScan.html", [
      [ "run", "da/d8f/namespaceCLICSiDScan.html#a976a9c178f6e9302d05e68d331b03bbd", null ],
      [ "format", "da/d8f/namespaceCLICSiDScan.html#acca65ac2aa1c8eb5568efe29e3527d2b", null ],
      [ "level", "da/d8f/namespaceCLICSiDScan.html#afd694e33e3777cda30496f0aef5ba028", null ],
      [ "logger", "da/d8f/namespaceCLICSiDScan.html#a6a515938a28dc1f61b9ad4ab16c68321", null ]
    ] ],
    [ "CLICSidSimuLCIO", "d9/d9f/namespaceCLICSidSimuLCIO.html", [
      [ "run", "d9/d9f/namespaceCLICSidSimuLCIO.html#afa89c306a276c7d4a10b217fb36d5df9", null ],
      [ "format", "d9/d9f/namespaceCLICSidSimuLCIO.html#a1c844730b62caa67615324f1483194ec", null ],
      [ "level", "d9/d9f/namespaceCLICSidSimuLCIO.html#a4b48915490c8fdc8fac47ea8981303e1", null ],
      [ "logger", "d9/d9f/namespaceCLICSidSimuLCIO.html#a729abe971af7db088aee5a317c1187b8", null ]
    ] ],
    [ "CLICSidSimuMarkus", "d0/d12/namespaceCLICSidSimuMarkus.html", [
      [ "run", "d0/d12/namespaceCLICSidSimuMarkus.html#aca8ee18febea92e1968fe70ccecdfa05", null ],
      [ "format", "d0/d12/namespaceCLICSidSimuMarkus.html#ad10728483bb32b5602d7b4a7677a3e02", null ],
      [ "level", "d0/d12/namespaceCLICSidSimuMarkus.html#a885bdb9904140d376630d9a3843e87f4", null ],
      [ "logger", "d0/d12/namespaceCLICSidSimuMarkus.html#a6a57a587dcfbabaf199d8fefc0386ff7", null ]
    ] ],
    [ "CMSEcalSim", "d5/d14/namespaceCMSEcalSim.html", [
      [ "run", "d5/d14/namespaceCMSEcalSim.html#a29770840db595425d4a8760e86053001", null ],
      [ "format", "d5/d14/namespaceCMSEcalSim.html#aee0d8f60d019d1d2248f655cbfe646fd", null ],
      [ "level", "d5/d14/namespaceCMSEcalSim.html#a399251593fcec5be642c0c2b526fd080", null ],
      [ "logger", "d5/d14/namespaceCMSEcalSim.html#a0719de3478e8ed27c6927d3b63a21b60", null ]
    ] ],
    [ "CMSTrackerSim", "df/dc3/namespaceCMSTrackerSim.html", [
      [ "run", "df/dc3/namespaceCMSTrackerSim.html#a823baeeba1c1801890230c1f9655bf4b", null ],
      [ "format", "df/dc3/namespaceCMSTrackerSim.html#a0301ca2e23ea7fa50a3ae025373c1469", null ],
      [ "level", "df/dc3/namespaceCMSTrackerSim.html#ae51e248fea181049ae5cff21364acb1a", null ],
      [ "logger", "df/dc3/namespaceCMSTrackerSim.html#aed8a799d5c965525eae07ee3570d8ba5", null ]
    ] ],
    [ "CODEX-b-alone", "d1/d69/namespaceCODEX-b-alone.html", [
      [ "run", "d1/d69/namespaceCODEX-b-alone.html#a1f3365e043f2682ff029d7e3a5654c75", null ]
    ] ],
    [ "cond", null, [
      [ "ConditionsPrinter", null, [
        [ "ParamPrinter", "d8/d84/classConditionsPrinter_1_1ParamPrinter.html", "d8/d84/classConditionsPrinter_1_1ParamPrinter" ]
      ] ]
    ] ],
    [ "CreateParsers", "d1/d27/namespaceCreateParsers.html", [
      [ "createContainerFile", "d1/d27/namespaceCreateParsers.html#a0c983e4086a57539bb1e5b1db5ac6b71", null ],
      [ "createMapFile", "d1/d27/namespaceCreateParsers.html#ab6c2dadb479b89f7cec8e48b53618d13", null ],
      [ "createMappedFile", "d1/d27/namespaceCreateParsers.html#a4351b7915a711ab1b1863cbb8a2dad78", null ],
      [ "createParsers", "d1/d27/namespaceCreateParsers.html#adf45bd87134423d80ba323a53321ef92", null ],
      [ "LICENSE", "d1/d27/namespaceCreateParsers.html#a3be3cfddc6454b023d4f9331ee1394a7", null ]
    ] ],
    [ "dd4hep", "d3/d0b/namespacedd4hep.html", "d3/d0b/namespacedd4hep" ],
    [ "dd4hep_base", "dc/dc2/namespacedd4hep__base.html", "dc/dc2/namespacedd4hep__base" ],
    [ "dd4hep_GenericSurfaceInstallerPlugin", "d4/d3b/namespacedd4hep__GenericSurfaceInstallerPlugin.html", null ],
    [ "dd4hepFactories", "d6/d30/namespacedd4hepFactories.html", "d6/d30/namespacedd4hepFactories" ],
    [ "DDDigi", "da/d15/namespaceDDDigi.html", "da/d15/namespaceDDDigi" ],
    [ "DDG4", "d7/d66/namespaceDDG4.html", "d7/d66/namespaceDDG4" ],
    [ "DDG4TestSetup", "d9/df8/namespaceDDG4TestSetup.html", "d9/df8/namespaceDDG4TestSetup" ],
    [ "DDRec", "df/de1/namespaceDDRec.html", [
      [ "import_namespace_item", "df/de1/namespaceDDRec.html#a8a09278ed47b04b86def51e88bfab184", null ],
      [ "import_rec", "df/de1/namespaceDDRec.html#ac05682ea9256acec3d731fac04b2edb8", null ],
      [ "loadDDRec", "df/de1/namespaceDDRec.html#a16263d1812c1104053bce39f8dfd5b7a", null ],
      [ "logger", "df/de1/namespaceDDRec.html#a42a2cf10ade1c679da5650504fed8d66", null ],
      [ "name_space", "df/de1/namespaceDDRec.html#a94fc0393eaa11ca9b1862d0672621316", null ],
      [ "rec", "df/de1/namespaceDDRec.html#a962b3ad3bd7f4c6e1efe315cadc3420f", null ],
      [ "std_list_ISurface", "df/de1/namespaceDDRec.html#a59484691080a4d6f2113cd89c9b36fcf", null ],
      [ "std_list_VolSurface", "df/de1/namespaceDDRec.html#a1ab19a6d85e1a49dcb0a8035d9075ac9", null ]
    ] ],
    [ "ddsim", "dd/d6b/namespaceddsim.html", [
      [ "format", "dd/d6b/namespaceddsim.html#a420811cea1ea3c27522ca0e335049392", null ],
      [ "globalToSet", "dd/d6b/namespaceddsim.html#a0c5a3540bbaaf39f899a466e2eae1235", null ],
      [ "INFO", "dd/d6b/namespaceddsim.html#a5f818299ed7242d90acb8b573d9ce5f6", null ],
      [ "level", "dd/d6b/namespaceddsim.html#a0ad4bb4272e18ba58ddd9812ab1a88b4", null ],
      [ "logger", "dd/d6b/namespaceddsim.html#a579bb05591295ed4b2bd347221f93c50", null ],
      [ "RUNNER", "dd/d6b/namespaceddsim.html#a8cf4bc3df6f19e71ba28978471002e94", null ],
      [ "stream", "dd/d6b/namespaceddsim.html#a82ab21b5edbb13ba0424737dec0da9d9", null ]
    ] ],
    [ "DDSim", "d9/df1/namespaceDDSim.html", "d9/df1/namespaceDDSim" ],
    [ "ddsix", "d3/d9d/namespaceddsix.html", "d3/d9d/namespaceddsix" ],
    [ "dumpDetectorData", "d1/ddf/namespacedumpDetectorData.html", [
      [ "dumpData", "d1/ddf/namespacedumpDetectorData.html#a398195a04fb719e25d5f44148ce77b5d", null ],
      [ "args", "d1/ddf/namespacedumpDetectorData.html#a241d3f73d7fec9a56cec43e95a40c672", null ],
      [ "default", "d1/ddf/namespacedumpDetectorData.html#aac3b578d15eae66798fc858b284e846f", null ],
      [ "description", "d1/ddf/namespacedumpDetectorData.html#a40a05956ae62dd5030d3e33befd8750f", null ],
      [ "dest", "d1/ddf/namespacedumpDetectorData.html#a4b03b6d011fcef167e88b5191cfafab6", null ],
      [ "det", "d1/ddf/namespacedumpDetectorData.html#ae7a350290a18d3511a7e949f2bd09689", null ],
      [ "format", "d1/ddf/namespacedumpDetectorData.html#a1a863b7f5b8898c43cc28804ca1e0daa", null ],
      [ "help", "d1/ddf/namespacedumpDetectorData.html#a59253472e72fef11a8492e7defbfcd35", null ],
      [ "level", "d1/ddf/namespacedumpDetectorData.html#ae4160caec3054f1b6fb8b2ce44be2f77", null ],
      [ "logger", "d1/ddf/namespacedumpDetectorData.html#abb804ae4b8ec220123808ed750be1df4", null ],
      [ "metavar", "d1/ddf/namespacedumpDetectorData.html#ac104d7a08d62ad4855643100801d1cfc", null ],
      [ "opts", "d1/ddf/namespacedumpDetectorData.html#a286c9a7ad6f95055c7982536ae0e3610", null ],
      [ "parser", "d1/ddf/namespacedumpDetectorData.html#a2b7eb44739d9f449569c30aee8a5e0e2", null ],
      [ "width", "d1/ddf/namespacedumpDetectorData.html#a4e21a268171ea02b122ec8c5abc203f7", null ]
    ] ],
    [ "EVAL", null, [
      [ "Object", null, [
        [ "Struct", "d5/dce/structEVAL_1_1Object_1_1Struct.html", "d5/dce/structEVAL_1_1Object_1_1Struct" ]
      ] ]
    ] ],
    [ "EVENT", "db/da5/namespaceEVENT.html", null ],
    [ "EventSeed", "d4/d75/namespaceEventSeed.html", null ],
    [ "FCC_Hcal", "d0/d8d/namespaceFCC__Hcal.html", [
      [ "run", "d0/d8d/namespaceFCC__Hcal.html#aefff85e5a7204ae79bb1a50c7b0fe436", null ]
    ] ],
    [ "g4GeometryScan", "d2/d39/namespaceg4GeometryScan.html", [
      [ "materialScan", "d2/d39/namespaceg4GeometryScan.html#a69ab603899290831c69aeae374f6e368", null ],
      [ "printOpts", "d2/d39/namespaceg4GeometryScan.html#aa16ca510109f5442ed77b58f2bc2d32b", null ],
      [ "args", "d2/d39/namespaceg4GeometryScan.html#a0c323532174ab532b0e882f697114e5d", null ],
      [ "default", "d2/d39/namespaceg4GeometryScan.html#affc8b2be9b3d5fddd77c500c4071d4f8", null ],
      [ "description", "d2/d39/namespaceg4GeometryScan.html#a8e17078c01a10e315a95cbd42983cce5", null ],
      [ "dest", "d2/d39/namespaceg4GeometryScan.html#a25732b4f1bf9132fcd7448b80f4f7e28", null ],
      [ "direction", "d2/d39/namespaceg4GeometryScan.html#a15b8485abe54e758c0d3006bbd706b96", null ],
      [ "format", "d2/d39/namespaceg4GeometryScan.html#a4eb59283b367d14f3b6804c6a344a0cd", null ],
      [ "help", "d2/d39/namespaceg4GeometryScan.html#a65ce4b21ca6ff68e6a36195157b3ec12", null ],
      [ "level", "d2/d39/namespaceg4GeometryScan.html#a468b276eb858176eeb9463d4e2dc42ad", null ],
      [ "logger", "d2/d39/namespaceg4GeometryScan.html#a05d24dbbb9061d33ccc0e8e4e8d90ca4", null ],
      [ "metavar", "d2/d39/namespaceg4GeometryScan.html#a30f633269ad040dd66a2e12fe9aa59fa", null ],
      [ "opts", "d2/d39/namespaceg4GeometryScan.html#a20fba9df49916bbd65028c11c5984fdc", null ],
      [ "parser", "d2/d39/namespaceg4GeometryScan.html#a463b85cd4c8cd8a3de3fedaf9c72b5d2", null ],
      [ "position", "d2/d39/namespaceg4GeometryScan.html#a181e77456b0cb78db9a7dde4b2a13b34", null ],
      [ "ret", "d2/d39/namespaceg4GeometryScan.html#a5f82eb224c2bba305efd16b23cc5bac3", null ],
      [ "width", "d2/d39/namespaceg4GeometryScan.html#aef3a95ba1b72a4d8997588fc97aecc83", null ]
    ] ],
    [ "g4MaterialScan", "d9/d16/namespaceg4MaterialScan.html", [
      [ "materialScan", "d9/d16/namespaceg4MaterialScan.html#acef2d52a534fef3532ee32f9c0afecb7", null ],
      [ "printOpts", "d9/d16/namespaceg4MaterialScan.html#a2197aad190cdba1b3ab9957d0667aaba", null ],
      [ "args", "d9/d16/namespaceg4MaterialScan.html#a175630505907fe78649a3d24a38b9556", null ],
      [ "default", "d9/d16/namespaceg4MaterialScan.html#a46c47af830e4e317485873d5b83e9798", null ],
      [ "description", "d9/d16/namespaceg4MaterialScan.html#a0c0f6c3ce526c2c952d9c03270d9f1bb", null ],
      [ "dest", "d9/d16/namespaceg4MaterialScan.html#aefa6263ec4c6e055b754b85d8199a8c6", null ],
      [ "direction", "d9/d16/namespaceg4MaterialScan.html#a482f17093aa29cb9daddb6d398f79fb4", null ],
      [ "format", "d9/d16/namespaceg4MaterialScan.html#a32219abb4c3cc697bb343491ad4c2256", null ],
      [ "help", "d9/d16/namespaceg4MaterialScan.html#a3c6ce3751f1cabc62e4cba41c339faa1", null ],
      [ "level", "d9/d16/namespaceg4MaterialScan.html#a3359c9dc6d5cd19873f13e77a3baf8e9", null ],
      [ "logger", "d9/d16/namespaceg4MaterialScan.html#a80cfbd7703018505bd3d34f384fd5c1f", null ],
      [ "metavar", "d9/d16/namespaceg4MaterialScan.html#a14b919743b051572c359962cc0cf6a5b", null ],
      [ "opts", "d9/d16/namespaceg4MaterialScan.html#ae8b254a49b9e1809925e7763ab55bcf4", null ],
      [ "parser", "d9/d16/namespaceg4MaterialScan.html#ae3eec06729b666563dfd0ad7392d7370", null ],
      [ "position", "d9/d16/namespaceg4MaterialScan.html#ae8ad3e729fefaa8f4de1a6149140226c", null ],
      [ "ret", "d9/d16/namespaceg4MaterialScan.html#a700159b5305e3cf5fa5f6362568745b1", null ],
      [ "width", "d9/d16/namespaceg4MaterialScan.html#a42a5c1452b2a9ca4438eecc6e983ecd2", null ]
    ] ],
    [ "g4units", "dc/d0a/namespaceg4units.html", [
      [ "ampere", "dc/d0a/namespaceg4units.html#a4af25b2328bba5374449a666a0b55167", null ],
      [ "angstrom", "dc/d0a/namespaceg4units.html#aa6567f095c1a829a02f6d54dbde3340a", null ],
      [ "atmosphere", "dc/d0a/namespaceg4units.html#ae224460c90cdf83a09bd5fa039753453", null ],
      [ "bar", "dc/d0a/namespaceg4units.html#a8858be75f514cd172bed84c394b1afa2", null ],
      [ "barn", "dc/d0a/namespaceg4units.html#a80716c1ecbd04ab4909758a4ba538e31", null ],
      [ "becquerel", "dc/d0a/namespaceg4units.html#a9f3c59ab949ba6d80db52a1c2fd7a640", null ],
      [ "candela", "dc/d0a/namespaceg4units.html#abe5a199839df5b20ca3cf8d869a849eb", null ],
      [ "centimeter", "dc/d0a/namespaceg4units.html#a0c9b96a3386aaaa014baa4a988804a64", null ],
      [ "centimeter2", "dc/d0a/namespaceg4units.html#a7c7f1a8dd73480a1a57a3c31df18575b", null ],
      [ "centimeter3", "dc/d0a/namespaceg4units.html#a410614a29a40d3b0cac55c78e630a957", null ],
      [ "cm", "dc/d0a/namespaceg4units.html#ab49e4b977a53560770bef0db6e5379d3", null ],
      [ "cm2", "dc/d0a/namespaceg4units.html#a996782c97b2fd3dbb0b0d2acddc41b50", null ],
      [ "cm3", "dc/d0a/namespaceg4units.html#a1243dcfbf7a3138f0283e7af65149f40", null ],
      [ "coulomb", "dc/d0a/namespaceg4units.html#a996c9088cc83d97431a223b92423a7e9", null ],
      [ "curie", "dc/d0a/namespaceg4units.html#a720cb53a6acdce77c7378fb24f1f189a", null ],
      [ "deg", "dc/d0a/namespaceg4units.html#a498c30529f21722c83a9b7b90b031453", null ],
      [ "degree", "dc/d0a/namespaceg4units.html#a45f36f52662f1b48009f0f58f292c33d", null ],
      [ "e_SI", "dc/d0a/namespaceg4units.html#ac964700a431c15b39c38b0632c17d6d1", null ],
      [ "electronvolt", "dc/d0a/namespaceg4units.html#a73bba26614b5777fb2809725179eda22", null ],
      [ "eplus", "dc/d0a/namespaceg4units.html#a3792d98cf33f6a12a369886966aa4a8c", null ],
      [ "eV", "dc/d0a/namespaceg4units.html#ac6ec3a631741955842cf19753bc1e615", null ],
      [ "farad", "dc/d0a/namespaceg4units.html#af59cf58f3e33297d8cb4bad695e4968d", null ],
      [ "femtosecond", "dc/d0a/namespaceg4units.html#a918b3650e0be7c315f4ec62778cbc97f", null ],
      [ "fermi", "dc/d0a/namespaceg4units.html#a6fc19f576ed0c5e31c3ef7f9a90deb13", null ],
      [ "g", "dc/d0a/namespaceg4units.html#a2fdc757e5084fd05094cf1f6e05fd5b8", null ],
      [ "gauss", "dc/d0a/namespaceg4units.html#aeab0851bbf223ad5db785ede0644ee30", null ],
      [ "GeV", "dc/d0a/namespaceg4units.html#aeda0f2ec5668b3ab098e43b421535773", null ],
      [ "gigaelectronvolt", "dc/d0a/namespaceg4units.html#a9a0668837fce966423d021f0d55eec60", null ],
      [ "gram", "dc/d0a/namespaceg4units.html#a284d4052d184f63291ec37688abe617a", null ],
      [ "gray", "dc/d0a/namespaceg4units.html#aa25ed81bf2ccb6d9d7a0dcc658595daf", null ],
      [ "henry", "dc/d0a/namespaceg4units.html#a52312b59cdb885eccc39a8c7239a3faa", null ],
      [ "hep_pascal", "dc/d0a/namespaceg4units.html#ae3b812201fd9295441a3dba5af967726", null ],
      [ "hertz", "dc/d0a/namespaceg4units.html#aceeaab7ff4e1d8c76af7772ee2d7cfd9", null ],
      [ "joule", "dc/d0a/namespaceg4units.html#adc83e6e692f798e961324b8e300972cb", null ],
      [ "kelvin", "dc/d0a/namespaceg4units.html#a074ef39bb38e4d32cfd94d8795867564", null ],
      [ "keV", "dc/d0a/namespaceg4units.html#a71ee957c2a04784e84614e8962ce24cc", null ],
      [ "kg", "dc/d0a/namespaceg4units.html#a2d25f8e38da8ead3ff0858043dada319", null ],
      [ "kiloelectronvolt", "dc/d0a/namespaceg4units.html#a9bf84a6bf793eeb32702b6f60e6a1d71", null ],
      [ "kilogauss", "dc/d0a/namespaceg4units.html#a40e187c5fe78df97c0694e862c72f440", null ],
      [ "kilogram", "dc/d0a/namespaceg4units.html#adcbf5dd03a83bcbee4affb34dcf6543c", null ],
      [ "kilohertz", "dc/d0a/namespaceg4units.html#a16f9fa9fb01ed04b9a81ba213a8d7365", null ],
      [ "kilometer", "dc/d0a/namespaceg4units.html#acb2c6491f4a817b57c0b78dc7c4d9288", null ],
      [ "kilometer2", "dc/d0a/namespaceg4units.html#aaf9c51902874adf4a6203231f58f774c", null ],
      [ "kilometer3", "dc/d0a/namespaceg4units.html#a5d6652c125dbf69c37c99653ad5d88b5", null ],
      [ "kilovolt", "dc/d0a/namespaceg4units.html#a71d60b04aead3ccb90d11dc560b0fd03", null ],
      [ "km", "dc/d0a/namespaceg4units.html#a5ed96857f58755d4331ecf0ffa7757d0", null ],
      [ "km2", "dc/d0a/namespaceg4units.html#aecaffe213889b32c321e21c7ab6aa8ec", null ],
      [ "km3", "dc/d0a/namespaceg4units.html#abc30e1f172f2ccbde287ce04bcc38892", null ],
      [ "lumen", "dc/d0a/namespaceg4units.html#a907f8da95eace3804b6ed9359ef502a9", null ],
      [ "lux", "dc/d0a/namespaceg4units.html#ac576fb583dec5397abb088c75aaa2266", null ],
      [ "m", "dc/d0a/namespaceg4units.html#a292301f10cd34031c98492bc77fa32dc", null ],
      [ "m2", "dc/d0a/namespaceg4units.html#a541baad0c6a65e0684b5d37147ba323e", null ],
      [ "m3", "dc/d0a/namespaceg4units.html#aa12e4a13ffbf4467cbd59d7098d8d3c7", null ],
      [ "megaelectronvolt", "dc/d0a/namespaceg4units.html#ae036ce0a6cd17d90fc8e0691257aec30", null ],
      [ "megahertz", "dc/d0a/namespaceg4units.html#aef1ad93d3a6453c7099a52c2c0b865be", null ],
      [ "megavolt", "dc/d0a/namespaceg4units.html#acd1f3753fc8058431470a0d0a6929710", null ],
      [ "meter", "dc/d0a/namespaceg4units.html#a9eacef091818d1238cae59a72a94f5f6", null ],
      [ "meter2", "dc/d0a/namespaceg4units.html#afd209a3166dba61a6f72072e08088345", null ],
      [ "meter3", "dc/d0a/namespaceg4units.html#a1a1d47c2b40ff382f107731c8e38786d", null ],
      [ "MeV", "dc/d0a/namespaceg4units.html#a3c3517c5008ad6a72ad8356ee6f30d1c", null ],
      [ "mg", "dc/d0a/namespaceg4units.html#a0601c1b6b7d7a9e9750c7948edf2090a", null ],
      [ "microampere", "dc/d0a/namespaceg4units.html#a53303b9677734ccfb124dd62ec5a057a", null ],
      [ "microbarn", "dc/d0a/namespaceg4units.html#a1f7dfcfc0d4e11a8fc8f3094fad70ac5", null ],
      [ "microfarad", "dc/d0a/namespaceg4units.html#a738aebe0f72d1f6b3049a6ea6cb161a3", null ],
      [ "micrometer", "dc/d0a/namespaceg4units.html#a9178ab4efa29cbf84c0926f2180d0c57", null ],
      [ "microsecond", "dc/d0a/namespaceg4units.html#a4250fcfdd93904b8352f5988008a37ed", null ],
      [ "milliampere", "dc/d0a/namespaceg4units.html#aa8fe000ce054da86509843683c11547e", null ],
      [ "millibarn", "dc/d0a/namespaceg4units.html#ad4d669235e0ef498c290e27a60db61ca", null ],
      [ "millifarad", "dc/d0a/namespaceg4units.html#a7007696b43d16403b177b50b50946a23", null ],
      [ "milligram", "dc/d0a/namespaceg4units.html#ae8417aaf81b566617ac827b115c2bda2", null ],
      [ "millimeter", "dc/d0a/namespaceg4units.html#a87ef08e7649cda499d994abfe6a7f555", null ],
      [ "millimeter2", "dc/d0a/namespaceg4units.html#a7801ea6a44c0a81bc3742acb2e2c5a47", null ],
      [ "millimeter3", "dc/d0a/namespaceg4units.html#a0ed74629e372c4ac7c5c1bab97ce10ad", null ],
      [ "milliradian", "dc/d0a/namespaceg4units.html#a92c213703957bd25df05c9d0b890ec4f", null ],
      [ "millisecond", "dc/d0a/namespaceg4units.html#ad36ff52b303a61a30369141e5a4018df", null ],
      [ "mm", "dc/d0a/namespaceg4units.html#a66cde209ec0e43118e9debfd0784bcdd", null ],
      [ "mm2", "dc/d0a/namespaceg4units.html#af195ce0332a0688ef67eba0a261d977e", null ],
      [ "mm3", "dc/d0a/namespaceg4units.html#aef6112adc46736b565c75d9f045b104b", null ],
      [ "mole", "dc/d0a/namespaceg4units.html#aca185453b0eb3a0d02e4d9cea0d81271", null ],
      [ "mrad", "dc/d0a/namespaceg4units.html#a5ecef7e7158c38309799fd483be0c5ad", null ],
      [ "ms", "dc/d0a/namespaceg4units.html#a7e1b5833c846ecbbc4671e6166634cf5", null ],
      [ "nanoampere", "dc/d0a/namespaceg4units.html#a966fd7dd30032d467da51e419b35bf11", null ],
      [ "nanobarn", "dc/d0a/namespaceg4units.html#a3d1ef859158f078054bca2c76fdcfdfb", null ],
      [ "nanofarad", "dc/d0a/namespaceg4units.html#a3277a9c4bc80228db5abb9de34f15c70", null ],
      [ "nanometer", "dc/d0a/namespaceg4units.html#acc8023146330842653a0d1286304dd23", null ],
      [ "nanosecond", "dc/d0a/namespaceg4units.html#a8bfb8be785fbb8b8f993f1b3d6938185", null ],
      [ "newton", "dc/d0a/namespaceg4units.html#a393dcd7d5d6f434f3bc5e2ae4bac7074", null ],
      [ "ns", "dc/d0a/namespaceg4units.html#a88916c8c7494af2f4d670a68db1de824", null ],
      [ "ohm", "dc/d0a/namespaceg4units.html#a945509daddcc8892c420c7885bda231f", null ],
      [ "parsec", "dc/d0a/namespaceg4units.html#a253b4fbf4a9664f8620d79acfb1f45e1", null ],
      [ "pascal", "dc/d0a/namespaceg4units.html#a3220d39c7e45517717454d5e6aab546a", null ],
      [ "pc", "dc/d0a/namespaceg4units.html#af1c35bbf00a0cac47a92d6f082a3614f", null ],
      [ "perCent", "dc/d0a/namespaceg4units.html#abe03579fafdffbefdcfaab33ea354c0c", null ],
      [ "perMillion", "dc/d0a/namespaceg4units.html#a61d9952943c6f09921961419765d010a", null ],
      [ "perThousand", "dc/d0a/namespaceg4units.html#a1e8ae2124c67dcc22e5d86a2f047357b", null ],
      [ "petaelectronvolt", "dc/d0a/namespaceg4units.html#a11ff7e720242093f760321fb55cca48a", null ],
      [ "PeV", "dc/d0a/namespaceg4units.html#a882de37b82e7c61a93d0d96be3d8d95a", null ],
      [ "picobarn", "dc/d0a/namespaceg4units.html#a1114b41f88641b49064a9bc76897b4ef", null ],
      [ "picofarad", "dc/d0a/namespaceg4units.html#ad8747a097e45be70686846f4b0830096", null ],
      [ "picosecond", "dc/d0a/namespaceg4units.html#adbeb0f0e93b92389ced41af04144ff5e", null ],
      [ "rad", "dc/d0a/namespaceg4units.html#a1912eb3f2ce1d9cc6a2679dbe33667b6", null ],
      [ "radian", "dc/d0a/namespaceg4units.html#a2e63c94a61288032ab9dd91205507906", null ],
      [ "s", "dc/d0a/namespaceg4units.html#ad08e7135ce315ba3d231082258508ad9", null ],
      [ "second", "dc/d0a/namespaceg4units.html#a12b7a7d6c4eab4b102e309f36c00bcea", null ],
      [ "sr", "dc/d0a/namespaceg4units.html#a752898213dd6f2f2edd79a1efb4782fa", null ],
      [ "steradian", "dc/d0a/namespaceg4units.html#a2ee03029510f0498e2bb0142d3aa0b27", null ],
      [ "teraelectronvolt", "dc/d0a/namespaceg4units.html#a6489edfac86c811adb6a5911a531b6b5", null ],
      [ "tesla", "dc/d0a/namespaceg4units.html#a3a1eb44b97406583cd978ba0d822010c", null ],
      [ "TeV", "dc/d0a/namespaceg4units.html#a80f82a2b07a6b6038aeea12b5c84d779", null ],
      [ "volt", "dc/d0a/namespaceg4units.html#a4b21d9572cdd28d90e4d9cba2f396686", null ],
      [ "watt", "dc/d0a/namespaceg4units.html#a96c79a647f68a10445b305f0c3f00cde", null ],
      [ "weber", "dc/d0a/namespaceg4units.html#a9ec6b8e4c3a75150cbed4960c63d3e46", null ]
    ] ],
    [ "Gaudi", "da/d5e/namespaceGaudi.html", "da/d5e/namespaceGaudi" ],
    [ "gaudi", "d3/d19/namespacegaudi.html", "d3/d19/namespacegaudi" ],
    [ "GaudiPluginService", "d5/d43/namespaceGaudiPluginService.html", "d5/d43/namespaceGaudiPluginService" ],
    [ "GdmlDetector", "dd/d91/namespaceGdmlDetector.html", [
      [ "run", "dd/d91/namespaceGdmlDetector.html#a18bea4b3fca5b04e95a749a8599850bd", null ]
    ] ],
    [ "Geant4CalorimeterAction", "d1/d7f/namespaceGeant4CalorimeterAction.html", null ],
    [ "Geant4CerenkovPhysics", "d9/d45/namespaceGeant4CerenkovPhysics.html", null ],
    [ "Geant4EventReaderGuineaPig", "d6/db9/namespaceGeant4EventReaderGuineaPig.html", null ],
    [ "Geant4EventReaderHepEvt", "d2/d66/namespaceGeant4EventReaderHepEvt.html", null ],
    [ "Geant4EventReaderHepMC", "d4/d90/namespaceGeant4EventReaderHepMC.html", null ],
    [ "Geant4ExtraParticles", "d8/d73/namespaceGeant4ExtraParticles.html", null ],
    [ "Geant4GeneratorWrapper", "dd/d0b/namespaceGeant4GeneratorWrapper.html", null ],
    [ "Geant4InputAction", "de/dd6/namespaceGeant4InputAction.html", null ],
    [ "Geant4InteractionMerger", "d6/dee/namespaceGeant4InteractionMerger.html", null ],
    [ "Geant4InteractionVertexBoost", "d9/d04/namespaceGeant4InteractionVertexBoost.html", null ],
    [ "Geant4InteractionVertexSmear", "d0/d97/namespaceGeant4InteractionVertexSmear.html", null ],
    [ "Geant4Optical", "db/da1/namespaceGeant4Optical.html", null ],
    [ "Geant4OpticalCalorimeterAction", "dc/db0/namespaceGeant4OpticalCalorimeterAction.html", null ],
    [ "Geant4ParticleGun", "dc/dae/namespaceGeant4ParticleGun.html", null ],
    [ "Geant4ParticleHandler", "d2/d2b/namespaceGeant4ParticleHandler.html", null ],
    [ "Geant4PrimaryHandler", "da/d22/namespaceGeant4PrimaryHandler.html", null ],
    [ "Geant4RangeCut", "d1/d44/namespaceGeant4RangeCut.html", null ],
    [ "Geant4ScintillationPhysics", "d5/dfe/namespaceGeant4ScintillationPhysics.html", null ],
    [ "Geant4TCUserParticleHandler", "d9/d25/namespaceGeant4TCUserParticleHandler.html", null ],
    [ "Geant4TrackerAction", "d1/de7/namespaceGeant4TrackerAction.html", null ],
    [ "Geant4TrackerCombineAction", "db/dfd/namespaceGeant4TrackerCombineAction.html", null ],
    [ "Geant4TrackerWeightedAction", "dd/d64/namespaceGeant4TrackerWeightedAction.html", null ],
    [ "Geant4UserParticleHandler", "d9/d9b/namespaceGeant4UserParticleHandler.html", null ],
    [ "Geant4VoidSensitiveAction", "d4/d27/namespaceGeant4VoidSensitiveAction.html", null ],
    [ "gear", "d5/dbc/namespacegear.html", null ],
    [ "GeoExtract", "d9/d23/namespaceGeoExtract.html", "d9/d23/namespaceGeoExtract" ],
    [ "HepMC3", "dc/dd2/namespaceHepMC3.html", null ],
    [ "HepMC3FileReader", "d3/d0c/namespaceHepMC3FileReader.html", null ],
    [ "IMPL", "d5/d18/namespaceIMPL.html", null ],
    [ "IO", "d5/dc3/namespaceIO.html", null ],
    [ "Issue_786", "d0/db0/namespaceIssue__786.html", [
      [ "run", "d0/db0/namespaceIssue__786.html#a3e0fc8188b30ccdb6a170fefc14aa90b", null ]
    ] ],
    [ "LCIOFileReader", "d7/d2c/namespaceLCIOFileReader.html", null ],
    [ "LCIOStdHepReader", "d0/de2/namespaceLCIOStdHepReader.html", null ],
    [ "LHeD", "df/da6/namespaceLHeD.html", "df/da6/namespaceLHeD" ],
    [ "LHeD_G4Gun", "d2/dd0/namespaceLHeD__G4Gun.html", [
      [ "run", "d2/dd0/namespaceLHeD__G4Gun.html#af1ed2ba949fb6770d08529c2eea99052", null ],
      [ "format", "d2/dd0/namespaceLHeD__G4Gun.html#aa7df3a14cc0fdd7321819f7ecde49be3", null ],
      [ "level", "d2/dd0/namespaceLHeD__G4Gun.html#a671fc1fd12ae5ff35e8e96117a6c53a6", null ],
      [ "logger", "d2/dd0/namespaceLHeD__G4Gun.html#a64bca8be4ee547a4ce85fc271abbcb07", null ]
    ] ],
    [ "LheD_tracker", "dc/dd8/namespaceLheD__tracker.html", [
      [ "run", "dc/dd8/namespaceLheD__tracker.html#a05232bcda69f09f6c7e00d01ddc1e6a1", null ],
      [ "format", "dc/dd8/namespaceLheD__tracker.html#a6974cd1a0aa9d8224d304b03c3a311ba", null ],
      [ "level", "dc/dd8/namespaceLheD__tracker.html#a3e9d58085c30c7188ae5c666c7671a87", null ],
      [ "logger", "dc/dd8/namespaceLheD__tracker.html#a0281e56f9df2546fdde0d64f3ff38906", null ]
    ] ],
    [ "LHeDMagField", "d8/d2a/namespaceLHeDMagField.html", [
      [ "lhed", "d8/d2a/namespaceLHeDMagField.html#a46cbe945788d4d46ad2fa031e1229047", null ],
      [ "quiet", "d8/d2a/namespaceLHeDMagField.html#a60554eed8b2a372a0be4987d1ac32dc5", null ]
    ] ],
    [ "LHeDPhysics", "d0/dc2/namespaceLHeDPhysics.html", [
      [ "lhed", "d0/dc2/namespaceLHeDPhysics.html#ad786c274fc866b39ab38086d505375c0", null ]
    ] ],
    [ "LHeDRandom", "d7/d01/namespaceLHeDRandom.html", [
      [ "format", "d7/d01/namespaceLHeDRandom.html#aa45726b4432d0f9442eefae58ec1f966", null ],
      [ "have_geo", "d7/d01/namespaceLHeDRandom.html#a8c57b178ccc8c1f8adfe9af693ce1dd4", null ],
      [ "level", "d7/d01/namespaceLHeDRandom.html#a740e398dc1d2426996f0cb7b722faf10", null ],
      [ "lhed", "d7/d01/namespaceLHeDRandom.html#a5b657a03b1dc18527ba54cf96a299b70", null ],
      [ "logger", "d7/d01/namespaceLHeDRandom.html#a661b24c18be72b4cfbde58ace84d93da", null ],
      [ "rndm", "d7/d01/namespaceLHeDRandom.html#a0f086f6bbd77b101223724e9bc654a81", null ],
      [ "rndm1", "d7/d01/namespaceLHeDRandom.html#a9af49471d5e683b4ffb555d85ae2d5e5", null ],
      [ "rndm2", "d7/d01/namespaceLHeDRandom.html#a708a4f7ddb2bc3a2e447da2ae00af7f8", null ]
    ] ],
    [ "LHeDScan", "d5/d81/namespaceLHeDScan.html", [
      [ "run", "d5/d81/namespaceLHeDScan.html#ab4b3fc270a6826bd9c4b9bfbe53aa57e", null ],
      [ "format", "d5/d81/namespaceLHeDScan.html#a35c5270bc30661bd9a41514be1a0d8b7", null ],
      [ "level", "d5/d81/namespaceLHeDScan.html#ab279fe2172c1f88b51c0a5b3d39df417", null ],
      [ "logger", "d5/d81/namespaceLHeDScan.html#a08f7353249d84ac69256584be096143c", null ]
    ] ],
    [ "LheSimu", "de/dcc/namespaceLheSimu.html", [
      [ "run", "de/dcc/namespaceLheSimu.html#a36d990907832b5e8a1e05933be6f2728", null ],
      [ "format", "de/dcc/namespaceLheSimu.html#af5e4ec637b63865eb335457df34a9939", null ],
      [ "level", "de/dcc/namespaceLheSimu.html#a2d727085dd98e0068585618a76202cdc", null ],
      [ "logger", "de/dcc/namespaceLheSimu.html#a3751c36b51c25fe2394511a517a90d38", null ]
    ] ],
    [ "MiniTel", "db/d40/namespaceMiniTel.html", [
      [ "run", "db/d40/namespaceMiniTel.html#a13912863ec97761fe276769fc4c9ef57", null ]
    ] ],
    [ "MiniTel_hepmc", "dc/dae/namespaceMiniTel__hepmc.html", [
      [ "run", "dc/dae/namespaceMiniTel__hepmc.html#abe490fab71c675222e509dee2c870e90", null ]
    ] ],
    [ "MiniTelEnergyDeposits", "d3/d47/namespaceMiniTelEnergyDeposits.html", [
      [ "run", "d3/d47/namespaceMiniTelEnergyDeposits.html#ac5ba88fac0627e47419353838c7e87da", null ]
    ] ],
    [ "MiniTelRegions", "d8/d1e/namespaceMiniTelRegions.html", [
      [ "act", "d8/d1e/namespaceMiniTelRegions.html#a0b4a7d1217295e51a24077aefcb74dfd", null ],
      [ "DebugRegions", "d8/d1e/namespaceMiniTelRegions.html#a832e4a29b2bd3085e10a5a1f8fbd948a", null ],
      [ "format", "d8/d1e/namespaceMiniTelRegions.html#a9d77e416c7d3eb82d1ea2423870a4125", null ],
      [ "level", "d8/d1e/namespaceMiniTelRegions.html#a146688bffcaafa00f944be4944bdfab0", null ],
      [ "logger", "d8/d1e/namespaceMiniTelRegions.html#abaed33a7506a67c94b2314b60e70e54b", null ],
      [ "m", "d8/d1e/namespaceMiniTelRegions.html#a3e3d562b4a44766136117b5a0e6659f0", null ],
      [ "seq", "d8/d1e/namespaceMiniTelRegions.html#a518b0f1e44a3bc51149a532f1661a11f", null ]
    ] ],
    [ "MiniTelSetup", "df/d05/namespaceMiniTelSetup.html", "df/d05/namespaceMiniTelSetup" ],
    [ "MSG", "d3/dad/namespaceMSG.html", [
      [ "DEBUG", "d3/dad/namespaceMSG.html#a2546a4357ea4286e6eeeff82b8c57873", null ],
      [ "ERROR", "d3/dad/namespaceMSG.html#af3de301bb210b0c449a6a6fca901b381", null ],
      [ "INFO", "d3/dad/namespaceMSG.html#ad36b70e31ce7d76adca983c02041f741", null ]
    ] ],
    [ "MultiCollections", "d3/d0e/namespaceMultiCollections.html", [
      [ "run", "d3/d0e/namespaceMultiCollections.html#abf8aea688708c3f879f46541064a3f8d", null ]
    ] ],
    [ "MultiSegmentCollections", "d4/d9f/namespaceMultiSegmentCollections.html", [
      [ "run", "d4/d9f/namespaceMultiSegmentCollections.html#a3505c3b90370e7e6f5c5e29bd6a48cdf", null ]
    ] ],
    [ "myanalysis", "df/d40/namespacemyanalysis.html", "df/d40/namespacemyanalysis" ],
    [ "MyTrackerSD_sim", "d9/dcc/namespaceMyTrackerSD__sim.html", [
      [ "run", "d9/dcc/namespaceMyTrackerSD__sim.html#aa8d8a5dcc9e700dd605296fdfb6c89c4", null ]
    ] ],
    [ "MyTrackerSDAction", "d4/d9b/namespaceMyTrackerSDAction.html", null ],
    [ "NestedBoxReflection", "d4/dd5/namespaceNestedBoxReflection.html", [
      [ "help", "d4/dd5/namespaceNestedBoxReflection.html#aaf72d9caf560adce682c658e0ed6d1c6", null ],
      [ "run", "d4/dd5/namespaceNestedBoxReflection.html#a0de2dfffcf7e7dee3f24f39339eb9ac7", null ],
      [ "format", "d4/dd5/namespaceNestedBoxReflection.html#a712db5600833f44e4d1f4fb4900183ab", null ],
      [ "level", "d4/dd5/namespaceNestedBoxReflection.html#a9ff786879b22d0ef0e97d8fe87e2b6f1", null ],
      [ "logger", "d4/dd5/namespaceNestedBoxReflection.html#a0b77376b6fe2d9efa6be64fa5ce80634", null ]
    ] ],
    [ "NestedDetectors", "d5/d25/namespaceNestedDetectors.html", [
      [ "run", "d5/d25/namespaceNestedDetectors.html#a11613e7735395c96cae496da64451482", null ]
    ] ],
    [ "ns", "de/d6e/namespacens.html", null ],
    [ "ODH", "d5/d27/namespaceODH.html", [
      [ "ECrossType", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416b", [
        [ "kCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baee6567af865766b35da636de2033e75a", null ],
        [ "kUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba03b6161158f4b5486ac9824a2a2f9b8a", null ],
        [ "kDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba8b2aad6bfd29737616678639f1b95607", null ],
        [ "kPunchedCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba26a34e98d49144ec30bbc94847dc6c84", null ],
        [ "kPunchedUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baa393e6f771a5e75268b80bf135b82899", null ],
        [ "kPunchedDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2b50e94be106d209a1ec7e1b924746b5", null ],
        [ "kUpstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba7d503bbd189ef95dc1cff5f3a255a3df", null ],
        [ "kDnstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3390b8bfa6d443c84053dc8feb25d8fc", null ],
        [ "kUpstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba0bf608223962cea887c8f60ae7438a0a", null ],
        [ "kDnstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2e3621ba99e39da546a6053c4a7f0179", null ],
        [ "kUpstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba382d0c3a19acc259e1c72ae416897554", null ],
        [ "kDnstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba28ebd0dd4e2f83c5f222a4717dd854e9", null ],
        [ "kUpstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba46d1980225fdd462cfdd383c08f0f754", null ],
        [ "kDnstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba93e3396272f058a659656fac906d77a1", null ],
        [ "kUpstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3b7c57dc4f47bcbc1b7c6340c1fa2430", null ],
        [ "kDnstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba4377bcbef32e69921e9174ae4e889c04", null ],
        [ "kUpstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bab786c832196d8ba2e7e41a8444aab3eb", null ],
        [ "kDnstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bacda846beb70436033b5847019b718a53", null ],
        [ "kCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baee6567af865766b35da636de2033e75a", null ],
        [ "kUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba03b6161158f4b5486ac9824a2a2f9b8a", null ],
        [ "kDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba8b2aad6bfd29737616678639f1b95607", null ],
        [ "kPunchedCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba26a34e98d49144ec30bbc94847dc6c84", null ],
        [ "kPunchedUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baa393e6f771a5e75268b80bf135b82899", null ],
        [ "kPunchedDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2b50e94be106d209a1ec7e1b924746b5", null ],
        [ "kUpstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba7d503bbd189ef95dc1cff5f3a255a3df", null ],
        [ "kDnstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3390b8bfa6d443c84053dc8feb25d8fc", null ],
        [ "kUpstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba0bf608223962cea887c8f60ae7438a0a", null ],
        [ "kDnstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2e3621ba99e39da546a6053c4a7f0179", null ],
        [ "kUpstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba382d0c3a19acc259e1c72ae416897554", null ],
        [ "kDnstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba28ebd0dd4e2f83c5f222a4717dd854e9", null ],
        [ "kUpstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba46d1980225fdd462cfdd383c08f0f754", null ],
        [ "kDnstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba93e3396272f058a659656fac906d77a1", null ],
        [ "kUpstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3b7c57dc4f47bcbc1b7c6340c1fa2430", null ],
        [ "kDnstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba4377bcbef32e69921e9174ae4e889c04", null ],
        [ "kUpstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bab786c832196d8ba2e7e41a8444aab3eb", null ],
        [ "kDnstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bacda846beb70436033b5847019b718a53", null ]
      ] ],
      [ "ECrossType", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416b", [
        [ "kCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baee6567af865766b35da636de2033e75a", null ],
        [ "kUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba03b6161158f4b5486ac9824a2a2f9b8a", null ],
        [ "kDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba8b2aad6bfd29737616678639f1b95607", null ],
        [ "kPunchedCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba26a34e98d49144ec30bbc94847dc6c84", null ],
        [ "kPunchedUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baa393e6f771a5e75268b80bf135b82899", null ],
        [ "kPunchedDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2b50e94be106d209a1ec7e1b924746b5", null ],
        [ "kUpstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba7d503bbd189ef95dc1cff5f3a255a3df", null ],
        [ "kDnstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3390b8bfa6d443c84053dc8feb25d8fc", null ],
        [ "kUpstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba0bf608223962cea887c8f60ae7438a0a", null ],
        [ "kDnstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2e3621ba99e39da546a6053c4a7f0179", null ],
        [ "kUpstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba382d0c3a19acc259e1c72ae416897554", null ],
        [ "kDnstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba28ebd0dd4e2f83c5f222a4717dd854e9", null ],
        [ "kUpstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba46d1980225fdd462cfdd383c08f0f754", null ],
        [ "kDnstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba93e3396272f058a659656fac906d77a1", null ],
        [ "kUpstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3b7c57dc4f47bcbc1b7c6340c1fa2430", null ],
        [ "kDnstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba4377bcbef32e69921e9174ae4e889c04", null ],
        [ "kUpstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bab786c832196d8ba2e7e41a8444aab3eb", null ],
        [ "kDnstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bacda846beb70436033b5847019b718a53", null ],
        [ "kCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baee6567af865766b35da636de2033e75a", null ],
        [ "kUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba03b6161158f4b5486ac9824a2a2f9b8a", null ],
        [ "kDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba8b2aad6bfd29737616678639f1b95607", null ],
        [ "kPunchedCenter", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba26a34e98d49144ec30bbc94847dc6c84", null ],
        [ "kPunchedUpstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416baa393e6f771a5e75268b80bf135b82899", null ],
        [ "kPunchedDnstream", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2b50e94be106d209a1ec7e1b924746b5", null ],
        [ "kUpstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba7d503bbd189ef95dc1cff5f3a255a3df", null ],
        [ "kDnstreamClippedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3390b8bfa6d443c84053dc8feb25d8fc", null ],
        [ "kUpstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba0bf608223962cea887c8f60ae7438a0a", null ],
        [ "kDnstreamClippedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2e3621ba99e39da546a6053c4a7f0179", null ],
        [ "kUpstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba382d0c3a19acc259e1c72ae416897554", null ],
        [ "kDnstreamClippedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba28ebd0dd4e2f83c5f222a4717dd854e9", null ],
        [ "kUpstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba46d1980225fdd462cfdd383c08f0f754", null ],
        [ "kDnstreamSlicedFront", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba93e3396272f058a659656fac906d77a1", null ],
        [ "kUpstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba3b7c57dc4f47bcbc1b7c6340c1fa2430", null ],
        [ "kDnstreamSlicedRear", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba4377bcbef32e69921e9174ae4e889c04", null ],
        [ "kUpstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bab786c832196d8ba2e7e41a8444aab3eb", null ],
        [ "kDnstreamSlicedBoth", "d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416bacda846beb70436033b5847019b718a53", null ]
      ] ],
      [ "checkForSensibleGeometry", "d5/d27/namespaceODH.html#ad74f1e571448227999d26cfbc9811819", null ],
      [ "getCrossType", "d5/d27/namespaceODH.html#ab02b8859bb194aedf72b856b37cd09fa", null ],
      [ "getCurrentAngle", "d5/d27/namespaceODH.html#a196cf9ea52d736bdf15707bc0b449ce6", null ]
    ] ],
    [ "OpNovice", "dd/d0a/namespaceOpNovice.html", [
      [ "run", "dd/d0a/namespaceOpNovice.html#ae7bd61fa983677495485877e6c0eee40", null ]
    ] ],
    [ "OpNovice_GDML", "d7/db6/namespaceOpNovice__GDML.html", [
      [ "run", "d7/db6/namespaceOpNovice__GDML.html#a579d6468d5e111026a7008ee795def02", null ]
    ] ],
    [ "readHEPMC", "d9/dd2/namespacereadHEPMC.html", [
      [ "run", "d9/dd2/namespacereadHEPMC.html#a7b0069c5d756e43cd6fe5004dd79c83c", null ],
      [ "format", "d9/dd2/namespacereadHEPMC.html#acaec78abc71c3e8ad7a1ab62acecb70b", null ],
      [ "input_file", "d9/dd2/namespacereadHEPMC.html#a90a10481e9982b31e9c5271ee1087ce3", null ],
      [ "level", "d9/dd2/namespacereadHEPMC.html#a31e5cd195a5d30b971c21184588a8db1", null ],
      [ "logger", "d9/dd2/namespacereadHEPMC.html#a35ccf02cad5822403395142b79ef7f7a", null ]
    ] ],
    [ "ReadMaterialProperties", "d2/de9/namespaceReadMaterialProperties.html", [
      [ "run", "d2/de9/namespaceReadMaterialProperties.html#a3bfdced8f2930ac00bb3441ce2b2e86c", null ]
    ] ],
    [ "ROOT", "d0/d10/namespaceROOT.html", "d0/d10/namespaceROOT" ],
    [ "Shelf", "d3/d01/namespaceShelf.html", [
      [ "detector_Shelf", "d3/d01/namespaceShelf.html#a4d1a28da6211e1014adbcde4acbbbc3a", null ]
    ] ],
    [ "SiD_ECAL_Parallel_Readout", "d3/d4c/namespaceSiD__ECAL__Parallel__Readout.html", [
      [ "run", "d3/d4c/namespaceSiD__ECAL__Parallel__Readout.html#afd368c22fe94153669891dc6e002972c", null ],
      [ "format", "d3/d4c/namespaceSiD__ECAL__Parallel__Readout.html#ab28885ab57f3d850715456022b033c25", null ],
      [ "level", "d3/d4c/namespaceSiD__ECAL__Parallel__Readout.html#a95876052f57495048593baab30ec88ac", null ],
      [ "logger", "d3/d4c/namespaceSiD__ECAL__Parallel__Readout.html#abbd67301983ec82d5ed9cc6fd64a3173", null ]
    ] ],
    [ "SiD_Markus", "d1/d6c/namespaceSiD__Markus.html", [
      [ "dummy_geom", "d1/d6c/namespaceSiD__Markus.html#a92dc0eb17774d75f70ba45715613d5bc", null ],
      [ "dummy_sd", "d1/d6c/namespaceSiD__Markus.html#a75de70d81f6202338f9ff366865400b2", null ],
      [ "run", "d1/d6c/namespaceSiD__Markus.html#a8fb0d30e93c8be8d024878b475ee987c", null ],
      [ "setupMaster", "d1/d6c/namespaceSiD__Markus.html#ac33a27311e1f618a61147923ea520576", null ],
      [ "setupSensitives", "d1/d6c/namespaceSiD__Markus.html#a9748dff7df29ef7e3993b6c3bbd47f0e", null ],
      [ "setupWorker", "d1/d6c/namespaceSiD__Markus.html#a1798b0463ce28538bb171addbf1ebc1b", null ],
      [ "format", "d1/d6c/namespaceSiD__Markus.html#a0eb5ddef0e1a9f96f44f3fa5e2e118c8", null ],
      [ "level", "d1/d6c/namespaceSiD__Markus.html#ad422cbd5a3eaef1fbb3c432cb418a929", null ],
      [ "logger", "d1/d6c/namespaceSiD__Markus.html#ae4ee7a8dc2c282f409f19fef0ccc973d", null ]
    ] ],
    [ "SiDSim", "dc/df0/namespaceSiDSim.html", [
      [ "help", "dc/df0/namespaceSiDSim.html#a4980f1b038c92a678d0cda3e10168346", null ],
      [ "run", "dc/df0/namespaceSiDSim.html#aef4078b86bdebedad8932d3ed2c2b628", null ],
      [ "format", "dc/df0/namespaceSiDSim.html#a78f8990933b10e352b90c50804329be6", null ],
      [ "level", "dc/df0/namespaceSiDSim.html#a00fe57379a25807cb21988551fe1b995", null ],
      [ "logger", "dc/df0/namespaceSiDSim.html#ae86e427e5e2cb20c2261a476f4a3d71d", null ]
    ] ],
    [ "SiDSim_MT", "d0/d3d/namespaceSiDSim__MT.html", [
      [ "run", "d0/d3d/namespaceSiDSim__MT.html#aa09e4d5d6e3786828fc62b0f28cd6538", null ],
      [ "setupMaster", "d0/d3d/namespaceSiDSim__MT.html#abc9b7abce177484875fd81924150142a", null ],
      [ "setupSensitives", "d0/d3d/namespaceSiDSim__MT.html#a482a55d64c64b326f149a2af1234c854", null ],
      [ "setupWorker", "d0/d3d/namespaceSiDSim__MT.html#a37121fd2393db048beb8bb23365bdc9f", null ],
      [ "format", "d0/d3d/namespaceSiDSim__MT.html#ac0f9f818d62961342aee115855fd68be", null ],
      [ "level", "d0/d3d/namespaceSiDSim__MT.html#acb0d565230511f2e12c320db5b72b071", null ],
      [ "logger", "d0/d3d/namespaceSiDSim__MT.html#a5301cb717799d92cfb82061342651af5", null ]
    ] ],
    [ "SiliconBlock", "da/de7/namespaceSiliconBlock.html", [
      [ "run", "da/de7/namespaceSiliconBlock.html#a69aede706de5b1b831e7c622e20135a2", null ]
    ] ],
    [ "SomeExperiment", "de/dc8/namespaceSomeExperiment.html", "de/dc8/namespaceSomeExperiment" ],
    [ "std", "d8/dcc/namespacestd.html", [
      [ "any_has_value", "d8/dcc/namespacestd.html#ace844ee3df1ce82cddde7c011eb3996a", null ]
    ] ],
    [ "SurfaceManager", "d0/dcf/namespaceSurfaceManager.html", null ],
    [ "test_import", "da/d45/namespacetest__import.html", [
      [ "test_module", "da/d45/namespacetest__import.html#afb79f24476d9bfc176b8f613667f240c", null ],
      [ "ALLOWED_TO_FAIL", "da/d45/namespacetest__import.html#a57f241ee4a98122718685e875c3d99b1", null ],
      [ "GRAPHIC_MODULES", "da/d45/namespacetest__import.html#a7210a621f7ec6db12b63500f9f7eb665", null ],
      [ "moduleNames", "da/d45/namespacetest__import.html#ae749e067e7e8870922a2ce652124f2a2", null ],
      [ "parametrize", "da/d45/namespacetest__import.html#a52c97a8d4f0bd77793f116f23e3f8eb1", null ]
    ] ],
    [ "test_import_ddg4", "da/d58/namespacetest__import__ddg4.html", [
      [ "test_module_ddg4", "da/d58/namespacetest__import__ddg4.html#a65a28a5a09eb5b6eae4d236a3cb46f34", null ]
    ] ],
    [ "testDDPython", "dc/d7e/namespacetestDDPython.html", "dc/d7e/namespacetestDDPython" ],
    [ "TestFramework", "db/da3/namespaceTestFramework.html", [
      [ "make_input", "db/da3/namespaceTestFramework.html#acdd89be37ee53935d7586df4d8301eb7", null ],
      [ "make_subdetector", "db/da3/namespaceTestFramework.html#a18192ad6c2232fb9510e63e62d71763d", null ],
      [ "run", "db/da3/namespaceTestFramework.html#a8b3279c25ca9958e2914903986acc653", null ]
    ] ],
    [ "Tests", "df/da7/namespaceTests.html", "df/da7/namespaceTests" ],
    [ "TrackingRegion", "d6/d03/namespaceTrackingRegion.html", [
      [ "run", "d6/d03/namespaceTrackingRegion.html#a8525abe437d7b398d718cd211ef58b73", null ]
    ] ],
    [ "UTIL", "d8/d46/namespaceUTIL.html", null ],
    [ "VP", "d3/d8b/namespaceVP.html", [
      [ "UserFlags", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890de", [
        [ "LEFT", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890dea5801193c51c6a121a41d9cc1688ffd6e", null ],
        [ "RIGHT", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890deaba8bc508697ad07cebc14088a500ece6", null ],
        [ "MAIN", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890deaf8aebbf83620d31199d1b555fb779d65", null ],
        [ "SIDE", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890dea7e1963a70977b5007750f18119f8f7e7", null ],
        [ "SUPPORT", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890dea37856d5ced2a34b4fc472a490ecf876a", null ],
        [ "MODULE", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890deae877cc6f99d9832ff822683ee3d407ca", null ],
        [ "LADDER", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890deaf40727bc1d9a05487352d0cd04e0b139", null ],
        [ "SENSOR", "d3/d8b/namespaceVP.html#acbd4a68ae11add201ec57a3e656890dea41391a9e991337d61e52d03a5d847797", null ]
      ] ],
      [ "NChipsPerSensor", "d3/d8b/namespaceVP.html#af1ac425e29f4c638cde3f60f01b91748", null ],
      [ "NColumns", "d3/d8b/namespaceVP.html#a557e76fd380d977fe4d727396eebf6ba", null ],
      [ "NModules", "d3/d8b/namespaceVP.html#a93c052d82fbc4f970d702b2059c43e7c", null ],
      [ "NPixelsPerSensor", "d3/d8b/namespaceVP.html#ad06c0f28a70ac51e5e6d6c62ad59fcc4", null ],
      [ "NRows", "d3/d8b/namespaceVP.html#a6c01374f77d6541b6acc3906a83bd5ea", null ],
      [ "NSensorColumns", "d3/d8b/namespaceVP.html#aa29db755b1353944acee6e5b2ed53244", null ],
      [ "NSensors", "d3/d8b/namespaceVP.html#ac13973c2ad246c538a9323c70ee7f0d5", null ],
      [ "NSensorsPerModule", "d3/d8b/namespaceVP.html#ac2d5a0939788e944b2569b01f57244c8", null ],
      [ "Pitch", "d3/d8b/namespaceVP.html#a657261b60fdc567170ddaf044e0147df", null ]
    ] ]
];
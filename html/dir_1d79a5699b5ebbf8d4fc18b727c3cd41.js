var dir_1d79a5699b5ebbf8d4fc18b727c3cd41 =
[
    [ "CLICSiD", "dir_dadb460444e974dcc4629ec5f0ce1821.html", "dir_dadb460444e974dcc4629ec5f0ce1821" ],
    [ "ClientTests", "dir_9c94c72efbdb1f61cb62b97302e315f3.html", "dir_9c94c72efbdb1f61cb62b97302e315f3" ],
    [ "DDCMS", "dir_e76659c476054ac8d7da327b5dd780ee.html", "dir_e76659c476054ac8d7da327b5dd780ee" ],
    [ "DDCodex", "dir_113584c6a4ee5392d51db38a96b4c0ad.html", "dir_113584c6a4ee5392d51db38a96b4c0ad" ],
    [ "DDDigi", "dir_f0d4ab2fbbe6071765fd188ee739ac6c.html", "dir_f0d4ab2fbbe6071765fd188ee739ac6c" ],
    [ "DDG4_MySensDet", "dir_f49cb1bef02437f9e6bc98f96c3d5456.html", "dir_f49cb1bef02437f9e6bc98f96c3d5456" ],
    [ "LHeD", "dir_7a58ba273bd7b831d188ffc2ba2bfe29.html", "dir_7a58ba273bd7b831d188ffc2ba2bfe29" ],
    [ "OpticalSurfaces", "dir_759fa272b0112bc21644912124b7b7fe.html", "dir_759fa272b0112bc21644912124b7b7fe" ]
];
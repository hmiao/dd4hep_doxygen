/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "DD4hep", "index.html", [
    [ "README", "d2/dc2/md__home_miaomiao_software_DD4hep_DD4hep_doc_reference_README.html", null ],
    [ "v01-20-02", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html", [
      [ "v01-20-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md7", null ],
      [ "v01-20", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md8", null ],
      [ "v01-19", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md9", null ],
      [ "v01-18", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md10", null ],
      [ "v01-17-00", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md11", null ],
      [ "v01-16-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md12", null ],
      [ "v01-16", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md13", null ],
      [ "v01-15", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md14", null ],
      [ "v01-14-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md15", null ],
      [ "v01-14", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md16", null ],
      [ "v01-13-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md17", null ],
      [ "v01-13", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md18", null ],
      [ "v01-12-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md19", null ],
      [ "v01-12", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md20", null ],
      [ "v01-11-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md21", null ],
      [ "v01-11", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md22", null ],
      [ "v01-10", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md23", [
        [ "Provide support for Volume divisions.", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md24", null ]
      ] ],
      [ "v01-09", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md25", null ],
      [ "v01-08", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md26", null ],
      [ "Implementation of non-cylindrical tracking region (resolves #371)", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md27", null ],
      [ "Enhancement of assemblies, regions and production cuts (resolves #373)", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md28", null ],
      [ "SensitiveDetector types not changed by Geant4SensDetActionSequence (resolves #378)", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md29", null ],
      [ "v01-07-02", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md30", null ],
      [ "v01-07-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md31", null ],
      [ "v01-07", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md32", null ],
      [ "v01-06", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md33", null ],
      [ "v01-05", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md35", null ],
      [ "v01-04", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md36", null ],
      [ "v01-03", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md41", null ],
      [ "v01-02", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md42", [
        [ "Implement ROOT persistency mechanism for detector descriptions (continuation of AIDASoft/DD4hep#202).", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md43", null ],
        [ "Implement ROOT persistency mechanism for detector descriptions", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md44", null ],
        [ "Integrate <tt>DDSegmentatation</tt> into <tt>DDCore</tt>", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md45", null ],
        [ "Enhance ROOT Detector Description Persistency", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md46", null ],
        [ "Fix Handle Problem when Accessing Materials from Volumes", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md47", null ],
        [ "Implement ROOT persistency mechanism for the conditions", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md48", null ],
        [ "Split of dictionary files", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md49", null ]
      ] ],
      [ "v01-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md50", [
        [ "DDCore: Changes to the VolumeManager interface", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md51", null ]
      ] ],
      [ "v01-00-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md52", null ],
      [ "v01-00", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md53", [
        [ "DD4hep namespace reorganization", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md54", null ],
        [ "DDParsers", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md55", null ]
      ] ],
      [ "v00-24", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md56", null ],
      [ "Implementation of the decisions made at the Conditions mini-workshop", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md57", [
        [ "Access mechanisms of DD4hep conditions for utilities", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md58", null ],
        [ "Naming conventions for detector conditions", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md59", null ],
        [ "Important Notice", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md60", null ],
        [ "Updates to DDCond", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md61", null ],
        [ "Alignment handling/computations", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md62", null ],
        [ "Update of the existing examples", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md63", null ]
      ] ],
      [ "v00-23", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md64", null ],
      [ "v00-22", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md65", null ],
      [ "Improvements to the compact xml processing", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md66", null ],
      [ "Debugging the DDG4 geometry conversion mechanism", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md67", null ],
      [ "v00-21", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md68", null ],
      [ "v00-20", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md69", null ],
      [ "v00-19", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md70", null ],
      [ "v00-18", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md71", null ],
      [ "v00-17", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md72", null ],
      [ "v00-16", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md73", null ],
      [ "v00-15", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md74", null ],
      [ "v00-14", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md75", null ],
      [ "v00-13", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md76", null ],
      [ "v00-12", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md77", null ],
      [ "v00-11", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md78", null ],
      [ "v00-10", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md79", null ],
      [ "v00-09", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md80", null ],
      [ "v00-08", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md81", null ],
      [ "v00-07", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md82", null ],
      [ "v00-06", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md83", null ],
      [ "v00-04", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md84", null ],
      [ "v00-03", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md85", null ],
      [ "v00-02", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md86", null ],
      [ "v00-01", "d2/d3a/md__home_miaomiao_software_DD4hep_DD4hep_doc_ReleaseNotes.html#autotoc_md87", null ]
    ] ],
    [ "DD4hep Examples", "d5/d26/md__home_miaomiao_software_DD4hep_DD4hep_examples_README.html", null ],
    [ "Gaudi::PluginService", "d2/d26/GaudiPluginService-readme.html", [
      [ "Introduction", "d2/d26/GaudiPluginService-readme.html#autotoc_md90", null ],
      [ "Usage", "d2/d26/GaudiPluginService-readme.html#autotoc_md91", null ],
      [ "Special cases", "d2/d26/GaudiPluginService-readme.html#autotoc_md92", [
        [ "Factory aliases", "d2/d26/GaudiPluginService-readme.html#autotoc_md93", null ],
        [ "Namespaces", "d2/d26/GaudiPluginService-readme.html#autotoc_md94", null ],
        [ "Custom Factories", "d2/d26/GaudiPluginService-readme.html#autotoc_md95", null ]
      ] ]
    ] ],
    [ "DD4hep (Detector Description for High Energy Physics)", "d9/d27/md__home_miaomiao_software_DD4hep_DD4hep_README.html", [
      [ "Pre-requisites and installation", "d9/d27/md__home_miaomiao_software_DD4hep_DD4hep_README.html#autotoc_md97", null ],
      [ "Documentation", "d9/d27/md__home_miaomiao_software_DD4hep_DD4hep_README.html#autotoc_md98", null ],
      [ "Release notes", "d9/d27/md__home_miaomiao_software_DD4hep_DD4hep_README.html#autotoc_md99", null ],
      [ "License and Copyright", "d9/d27/md__home_miaomiao_software_DD4hep_DD4hep_README.html#autotoc_md100", null ]
    ] ],
    [ "Deprecated List", "da/d58/deprecated.html", null ],
    [ "Todo List", "dd/da0/todo.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", "namespacemembers_vars" ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", "functions_type" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/d10/structdd4hep_1_1cond_1_1ClientData.html",
"d0/d34/test__surface_8cc.html",
"d0/d66/classdd4hep_1_1cond_1_1ConditionsIOVPool.html#a01220b9c79cbd6a5a818ed8a5bdc14c5",
"d0/d87/readHEPMC_8py.html#acaec78abc71c3e8ad7a1ab62acecb70b",
"d0/d8f/structdd4hep_1_1Converter.html#af0f67187a4b6c7eaa4974aaea73902ee",
"d0/da6/namespacedd4hep_1_1DDAlign.html#a2bc1e0ba284e3702e24b1ea2ed111899",
"d0/dc4/UnicodeValues_8h.html#a1773f03a8f6e4a8572ef81f1e7badf56",
"d0/dc4/UnicodeValues_8h.html#a898d11b5e905bce926d7bd380af515de",
"d0/dc4/UnicodeValues_8h.html#af4674bc2feedbb195d8ad54064bc2a97",
"d1/d11/namespacedd4hep_1_1Errors.html#adc16fda5cf9135a174eea29f5e4b7de0",
"d1/d37/classdd4hep_1_1sim_1_1Geant4Calorimeter_1_1Hit.html#a11dca18ae6d3f26e3fc3635e5eb19b8e",
"d1/d63/structdd4hep_1_1DDCMSDetElementCreator_1_1Data.html#a508fc8484d8eccebf2eed7f0f21ff673",
"d1/d8e/namespacedd4hep_1_1xml_1_1tools.html",
"d1/db2/classdd4hep_1_1align_1_1GlobalAlignmentCache.html#acfe6788078037508fe9bab9656dd999a",
"d1/df7/AlignmentExampleObjects_8cpp.html",
"d2/d14/classdd4hep_1_1Limit.html#ac61fd84a7eab843b10b642e7840d97c0",
"d2/d39/namespaceg4GeometryScan.html#a463b85cd4c8cd8a3de3fedaf9c72b5d2",
"d2/d6f/classdd4hep_1_1OpticalSurface.html#a7386b94259343b0cb86a7d438f162782",
"d2/d79/classdd4hep_1_1digi_1_1DigiKernel.html#ab6c1040e41079c96e42130b744e6c961",
"d2/da0/classdd4hep_1_1Hyperboloid.html#a841ebcd235787e4b3ff35d5f026f5e7c",
"d2/dba/classdd4hep_1_1sim_1_1Geant4HitWrapper_1_1InvalidHit.html",
"d2/de5/classdd4hep_1_1align_1_1AlignmentsCalib.html#a9feaa49a2782024c4c34b56e91ca1164",
"d3/d0b/namespacedd4hep.html#a01ac7ba2a6f22b26a188ea5879869368",
"d3/d0b/namespacedd4hep.html#a4a352b919c6bb60efe2b4120fb55ddf6",
"d3/d0b/namespacedd4hep.html#ab3389d5e62804ede9a32da868b599545",
"d3/d10/classdd4hep_1_1DDSegmentation_1_1GridPhiEta.html#a7136b54092144259405cb34fd83d1e6f",
"d3/d2c/classdd4hep_1_1rec_1_1Vector2D.html#a19063963374d3d81c4c6550757a5f77e",
"d3/d44/DDCMS_2include_2DDCMS_2DDCMSTags_8h.html#a51d4fc2f2a18c89408c9f99a1c8d735d",
"d3/d5d/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1Dimension.html#a02101ee9d3f2fea98bcb01a1fd71e1c0",
"d3/d5d/structdd4hep_1_1DD4HEP__DIMENSION__NS_1_1Dimension.html#acdd31ef37eb2852e406d218e2fd3b074",
"d3/d7b/classdd4hep_1_1align_1_1AlignmentsCollector.html#a9c3f4b21c80551b1f71071b0cc1c6f9b",
"d3/d93/classdd4hep_1_1AlignmentCondition.html#a821f0e6afe93fbf8debb375b4c77f366",
"d3/d9d/namespaceddsix.html#af0a79b0ac06da474bbb8def6560919ae",
"d3/dbf/Handle_8cpp.html#a5dc1579125d2aea8dfce89cb2b316462",
"d3/dcb/classdd4hep_1_1Display.html#ade2c0fff0fb152ea4605c30c5516c319",
"d3/ddf/classCLICSid_1_1CLICSid.html#acc4b1565d039295f86419a2d7e4b66ab",
"d3/df6/g4units_8py.html#af195ce0332a0688ef67eba0a261d977e",
"d4/d19/classdd4hep_1_1digi_1_1EnergyDeposit.html#ae4444721b450756e844d0c977bbfb6dc",
"d4/d47/classdd4hep_1_1rec_1_1GearHandle.html#a4718a3182d5f35d2ed027ffbffd0e979",
"d4/d72/classdd4hep_1_1tools_1_1Evaluator.html#a96931e2d3d752ee42d8ab92ad1fbfded",
"d4/d8f/structdd4hep_1_1Parsers_1_1Grammar___3_01Iterator_00_01ROOT_1_1Math_1_1LorentzVector_3_01T1_01_4_00_01Skipper_01_4.html#a1df7e681416425f573512dcbf68a1a48",
"d4/dbc/classdd4hep_1_1DDSegmentation_1_1TiledLayerGridXY.html#a2fb0aec9d449be372738af41d67f5fd8",
"d4/ddf/classdd4hep_1_1dd4hep__file.html#a7ced1f7476bb40b9d2851cdbca7b7518",
"d5/d10/classdd4hep_1_1DDSegmentation_1_1BitFieldCoder.html#a940f0f42229477a026c86c8c2647700a",
"d5/d27/namespaceODH.html#ac2735d11b3092d73acd80890b275416ba2b50e94be106d209a1ec7e1b924746b5",
"d5/d37/classdd4hep_1_1STD__Conditions.html#af507f1a29058af8d40ff75c678c6646f",
"d5/d4e/classTiXmlHandle__t.html#a454496f0bc896bbd12b9acccc477a8fe",
"d5/d57/classdd4hep_1_1DDSegmentation_1_1BitFieldElement.html#af5106df2efdc439e635096bcc50deb54",
"d5/d77/classdd4hep_1_1SurfaceInstaller.html#a469db6aa7c8f2d442ece8c5cac41c870",
"d5/d89/classdd4hep_1_1DetElement.html#a47782470bbe890c880a2824de6d1b547",
"d5/dbc/classdd4hep_1_1DisplayConfiguration_1_1Config.html",
"d5/dc7/classdd4hep_1_1DDDB_1_1DDDBParamPhysVol3D.html#aa8f205ea780a4a6395f9127f33b4a400",
"d5/dd7/DDParsers_2include_2Evaluator_2DD4hepUnits_8h.html#ae0e1ad0bcded96ff438ed4251094c5b2",
"d5/ded/classdd4hep_1_1OverlayedField.html#a84d77cc61091e78a7bddb5f10af2d97b",
"d6/d08/namespacedd4hep_1_1dd.html#a9b987c5c6d47a3b3540c4e0b67faa464",
"d6/d1e/classGaudiPluginService_1_1cpluginsvc_1_1Property.html#af46e7210e2288436c9b8a4f87318504d",
"d6/d59/classDDG4_1_1Geant4.html#a4bab7a22ff225265a515f950c95d8db0",
"d6/d6e/classgaudi_1_1DeVeloSensorElement.html#a4744e029ae4c9b45ac5257b1cc62efb7",
"d6/d85/classdd4hep_1_1json_1_1Collection__t.html#a6573e92bcf6fa8242b14d0a2de115136",
"d6/d8f/classgaudi_1_1detail_1_1DeVeloSensorStaticObject.html#a24994a3fb6666c257a30e7923ea84bef",
"d6/dc1/classdd4hep_1_1DDSegmentation_1_1PolarGridRPhi2.html#ab04d5b58a743c293296a761b8ab07dd7",
"d7/d07/classgaudi_1_1DeVeloSensorElement_1_1StripInfo.html#a042b4cc9a07d7dcca0195b5e80a7cc8e",
"d7/d2a/classdd4hep_1_1cond_1_1ConditionsDependencyHandler.html#af7067f1ccc9230e1f2ca04cebf161244",
"d7/d48/classdd4hep_1_1DDTest.html#a88b70727ccf1a8c2c4e824309cea599e",
"d7/d6d/classdd4hep_1_1digi_1_1DigiRandomGenerator.html#a7f3344802fd05b1e06d02e96ed922fc4",
"d7/d88/LCIO_8py.html",
"d7/da6/classdd4hep_1_1cms_1_1ParsingContext.html#a74a50f1b021228075c0faa67729de873",
"d7/de0/structPyDDG4.html#a0d6bd662687386a35fc2aae49fa959ec",
"d8/d0e/structgaudi_1_1DeHelpers.html#a4534c36cd66b16b53bb88232c53a800e",
"d8/d3d/classdd4hep_1_1DDDB_1_1DDDBBooleanUnion.html#a14abbe961bd0af6c20aa1b4dfef611fc",
"d8/d7b/classdd4hep_1_1Condition.html#a5ddf10642f3fd351161dbf63a50746a3",
"d8/da2/classdd4hep_1_1IOV.html#a71b7a22eec4336ae3d850bbba0c50a92",
"d8/dcc/classdd4hep_1_1Header.html#a42b50866ceb0bb0f9ae6657db96c6ce4",
"d8/df7/structdd4hep_1_1SpecParRegistry.html#a0de97e4cb6b5318e43df83537ed63ed7",
"d9/d1d/classdd4hep_1_1sim_1_1Geant4UIMessenger.html#a2702dc799129bdd21a745bbdfa90ec75",
"d9/d3b/classDDG4TestSetup_1_1Setup.html#aaa4116c39a9871cc975da4ad6b523246",
"d9/d65/classdd4hep_1_1sim_1_1SequenceHdl.html#a08a21191bda930c6921242a5690a581e",
"d9/d8b/CLICSiD_2scripts_2CLICRandom_8py.html#a49cb4d37676e49c9f572273011f6d9d4",
"d9/dae/classdd4hep_1_1cond_1_1ConditionsTreePersistency.html#a1441123d2d97f27bc49133b652c80bcb",
"d9/dc6/classdd4hep_1_1DDSegmentation_1_1TiledLayerSegmentation.html#ae0ad0612bf170a4f37336380dea757fd",
"d9/de6/classdd4hep_1_1CutTube.html#a64c7fdd56a8c702b140e7a3481dd5f56",
"da/d01/ShapeTags_8h.html#a8fbb4a008f5d69776301778a40725574",
"da/d33/classdd4hep_1_1cms_1_1Namespace.html#a997e136d727a75dc1bae5b5bf0d37383",
"da/d55/namespaceGaudi_1_1PluginService_1_1v2.html#ae37ac78012eb0de62148ae8a1b88e430",
"da/d70/classdd4hep_1_1sim_1_1Geant4Sensitive.html#a88c872b79e49e399c8ee282960c2d77d",
"da/da1/Parsers_2detail_2ChildValue_8inl.html#a35afe475e3f9f2eef988cfee7136d206",
"da/dc3/classgaudi_1_1detail_1_1DeVPStaticObject.html#ae25bcc48f125ebdf3f28be5ced0bca5e",
"da/df2/install_2examples_2ClientTests_2scripts_2FCC__Hcal_8py.html",
"db/d17/classgaudi_1_1detail_1_1DeVPObject.html#ae013266358518756fc9043afd3bc5548",
"db/d50/classdd4hep_1_1detail_1_1VolumeManager__Populator.html#a85ea3306674deb9b2d82f80621f242a2",
"db/d89/classdd4hep_1_1Callback.html#a425f63b197f34e9be4704026c209ad2b",
"db/da9/structdd4hep_1_1detail_1_1ApplyMemFuncConst.html#ad7727f14b8206c1dca05baa983f5693d",
"db/dd1/classdd4hep_1_1DDSegmentation_1_1BitFieldValue.html",
"dc/d0a/namespaceg4units.html#a1243dcfbf7a3138f0283e7af65149f40",
"dc/d26/OtherDetectorHelpers_8h.html#ab02b8859bb194aedf72b856b37cd09fa",
"dc/d52/src_2Spirit_2ToStream_8cpp.html#a7b3739f6425f9072e1ac4597a2b5fa0e",
"dc/d77/namespacecheckGeometry.html#a541d049dcc68dc879aa97516dc35be0a",
"dc/d85/ShapeUtilities_8cpp.html#a5d6e00eea746e7f063db49e3bfdc30ab",
"dc/db5/classdd4hep_1_1DetType.html",
"dc/dde/CreateParsers_8py.html#a4351b7915a711ab1b1863cbb8a2dad78",
"dc/ded/classDDSim_1_1DD4hepSimulation_1_1DD4hepSimulation.html#ae8a041c1ad9b1961b95d6433cd153769",
"dd/d27/classdd4hep_1_1dd4hep__ptr.html",
"dd/d5b/World_8h.html",
"dd/d7c/classdd4hep_1_1sim_1_1Geant4Action.html#a401a8dceb3d540c069fc24f66d41bf64",
"dd/d9d/classtestDDPython_1_1a__class.html",
"dd/dc7/classSimpleCylinderImpl.html#acb062c2b151659d196df3948f60567b1",
"dd/def/structdd4hep_1_1detail_1_1FalphaNoise_1_1random__engine__wrapper.html#a0ea78224b2815085b2fc6d4ded274503",
"de/d25/ShapePlugins_8cpp.html#ad52d615a11976f14d9c9842b6866ea91",
"de/d3d/classTests_1_1Geant4SensitiveAction.html#a739fa2551e0217a591c5e99ae09d883a",
"de/d5d/classdd4hep_1_1DDSegmentation_1_1ProjectiveCylinder.html#a80c2aa700d2baf35837b24ae651da718",
"de/d8a/classdd4hep_1_1sim_1_1Geant4PrimaryConfig.html#a77e955e846604566735cb71f31d329e8",
"de/dda/classdd4hep_1_1detail_1_1ReleaseObjects.html",
"de/dfa/classdd4hep_1_1sim_1_1HitCompare.html",
"df/d37/AlignmentsCalculator_8cpp.html#a570cc507b95fffb34314fe0508080026",
"df/d50/classdd4hep_1_1sim_1_1Geant4VolumeManager.html#abaa478c17ecef61127f3b677d0d1c6bf",
"df/d70/classdd4hep_1_1Detector.html#aedb787d31bf929a8b00c891352ab4d7b",
"df/da4/Readout_8cpp.html",
"df/dda/classTiXmlAttributeSet.html#acb244bc616c28b1c4b8e8417f28e5f9e",
"dir_18f232859a2eda22e1538492b8a334ae.html",
"functions_type.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
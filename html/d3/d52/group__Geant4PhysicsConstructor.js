var group__Geant4PhysicsConstructor =
[
    [ "Geant4CerenkovPhysics", "d9/d45/namespaceGeant4CerenkovPhysics.html", null ],
    [ "Geant4RangeCut", "d1/d44/namespaceGeant4RangeCut.html", null ],
    [ "Geant4ExtraParticles", "d8/d73/namespaceGeant4ExtraParticles.html", null ],
    [ "Geant4Optical", "db/da1/namespaceGeant4Optical.html", null ],
    [ "Geant4ScintillationPhysics", "d5/dfe/namespaceGeant4ScintillationPhysics.html", null ]
];
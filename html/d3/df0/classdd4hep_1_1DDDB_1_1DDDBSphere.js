var classdd4hep_1_1DDDB_1_1DDDBSphere =
[
    [ "type", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#a1d46286f755f97169bc6c8365066ca27", null ],
    [ "delta_phi", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#a270e2492469e91af4b86f928257f0076", null ],
    [ "delta_theta", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#adb9bc6980b8d282a396a9de413cef9d5", null ],
    [ "phi", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#ad9b3ba1343238b719443e247fd767db2", null ],
    [ "rmax", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#a28d458fda0ce8a0f7a965c1be59f67c2", null ],
    [ "rmin", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#aa025bf4ac8cfdcb883abbb9f175d1869", null ],
    [ "theta", "d3/df0/classdd4hep_1_1DDDB_1_1DDDBSphere.html#a8d7b2d58211699c03ecdaddb72c78454", null ]
];